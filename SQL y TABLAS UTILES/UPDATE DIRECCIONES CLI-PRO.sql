-- Clientes
UPDATE    clienteproveedor cp,
          clientes_nippon_direcciones cnd
SET       cp.localidad = cnd.clocali,
          cp.provincia = cnd.cprovin,
          cp.codigopostal = cnd.ccodpos
WHERE     cp.idanterior = cnd.nnuclie


-- Proveedores
UPDATE    clienteproveedor cp,
          proveedores_nippon_direcciones cnd
SET       cp.localidad = cnd.clocali,
          cp.provincia = cnd.cprovin,
          cp.codigopostal = cnd.ccodpos
WHERE     cp.idanterior = cnd.nnuprov


-- Sucursales
UPDATE    sucursal s,
		  clienteproveedor cp,
          clientes_nippon_direcciones cnd
SET       s.localidad = cnd.clocali,
          s.provincia = cnd.cprovin,
          s.codigopostal = cnd.ccodpos
WHERE     s.clienteproveedor_id = cp.id AND cp.idanterior = cnd.nnuclie

UPDATE    sucursal s,
		  clienteproveedor cp,
          proveedores_nippon_direcciones cnd
SET       s.localidad = cnd.clocali,
          s.provincia = cnd.cprovin,
          s.codigopostal = cnd.ccodpos
WHERE     s.clienteproveedor_id = cp.id AND cp.idanterior = cnd.nnuprov