-- INSERT CLIENTES
INSERT INTO clienteproveedor
(idanterior, 
condicioniva_id, 
razonSocial, cuit,dni, denominacion, domiciliocomercial, 
telefono, celular, saldo, 
coeficientePrecio, mail, fax, clienteProveedor, estado) 
SELECT DISTINCT cn.Nro_Cliente, 
(SELECT ci.id FROM condicioniva ci WHERE ci.descripcion = cn.Condicion_IVA), 
cn.Razon_Social, CUIT, DNI, Denominacion, Domicilio_Comercial, 
Telefono, Celular, Saldo_Inicial, 
Coeficiente_Precio, Mail, Fax, 1, 'A' 
FROM clientes_nippon cn 

-- INSERT PROVEEDORES
INSERT INTO clienteproveedor
(
idanterior, 
tipoproveedor_id, 
condicioniva_id, 
razonSocial, denominacion, cuit, dni, domiciliocomercial, telefono, 
celular, saldo, coeficientePrecio, mail,fax, clienteProveedor, estado)

SELECT DISTINCT 
pn.Nro_Proveedor, 
(SELECT tp.id FROM tipoproveedor tp WHERE tp.descripcion = pn.Tipo_Proveedor) tipoproveedor_id, 
(SELECT ci.id FROM condicioniva ci WHERE ci.descripcion = pn.Condicion_IVA) condicioniva_id, 
pn.Razon_Social, pn.Denominacion, pn.CUIT, pn.DNI, pn.Domicilio_Comercial, 
pn.Telefono, pn.Celular, pn.Saldo_Inicial, pn.Coeficiente_Precio, pn.Mail, pn.Fax, 2, 'A'
FROM proveedores_nippon pn
