<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FacturaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('cliente', 'text', array(
                    'mapped' => false,
                    'label' => 'Cliente'
                ))
                ->add('idsucursal', 'text', array(
                    'mapped' => false,
                    'label' => 'ID - Sucursal'
                ))
                ->add('observacion', 'textarea', array(
                    'label' => 'Descripción',
                    'required' => false,
                    'attr' => array('class' => 'form-control', 'attr' => 'height: 200px;')
                ))
                ->add('bonificacion', null, array('required' => false))
                ->add('descuento', null, array('required' => false))
                ->add('percepcionib', null, array('required' => false, 'label' => 'Percepcion IIBB'))
                ->add('percepcioniva', null, array('required' => false, 'label' => 'Percepcion IVA'))
                ->add('tipofactura', 'choice', array(
                    'label' => ' Tipo de Factura',
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'A' => 'A',
                        'B' => 'B',
                        'C' => 'C',
                        'NC' => 'NC',
                        'ND' => 'ND',
                        'N/A' => 'N/A'
            )))
                ->add('fecha', 'date', array(
                    'label' => 'Fecha Factura',
                    'attr' => array('value' => date('Y-m-d')),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                ->add('unidadNegocio', 'entity', array(
                    'class' => 'JOYASJoyasBundle:UnidadNegocio',
                    'label' => 'Unidad Negocio',
                    'query_builder' => function (\JOYAS\JoyasBundle\Entity\UnidadNegocioRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = :ACTIVO AND u.punto > 0')->orderBy('u.descripcion', 'asc')->setParameter(':ACTIVO', 'A');
                    }
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Factura'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_factura';
    }

}
