<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $subcategoria = $builder->getData()->getSubcategoria();
        $id_categoria = $subcategoria ? $subcategoria->getCategoria()->getId() : 0;
        $categoria = $subcategoria ? $subcategoria->getCategoria() : null;

        $builder
//            ->add('rubro')
            ->add('descripcion')
            ->add('marca')
            ->add('modelo')
            ->add('categoria', 'entity', array (
                'class' => 'JOYASJoyasBundle:Categoria',
                'label' => 'Categoria',
                'mapped'=>false,
                'required'=>false,
                'empty_value'=>'Seleccionar categoría',
                'data'=> $categoria,
                'query_builder' => function (\JOYAS\JoyasBundle\Entity\CategoriaRepository $repository)
                {
                    return $repository->createQueryBuilder('u')
                        ->where('u.estado = :ACTIVO')
                        ->orderBy('u.descripcion', 'asc')
                        ->setParameter(':ACTIVO', 'A');
                }
            ))
            ->add('subcategoria')
            ->add('caracteristica')
            ->add('anio', 'text', array('label'=>'Años de Fabricación', 'required'=>false))
            ->add('marcarepuesto', 'text', array('label'=>'Marca del Respuesto', 'required'=>false))
            ->add('origen')
            ->add('precio')
            ->add('coeficientePrecio')
            ->add('procedencia')
            ->add('codigoOriginal')
            ->add('nroFabrica', 'text', array('label'=> 'Código Proveedor'))
            ->add('nroFabricacion', 'text', array('label'=> 'Nro. de Fabricación', 'required'=>false))
            ->add('tipoiva', 'choice', array (
				'label' => 'Tipo Iva',
				'attr'  => array('class'=>'form-control'),
				'choices' => array(
                    '21' 	=> '21',
                    '10.5' 	=> '10.5',
                    '0' 	=> '0'
		   		)))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Producto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'joyas_joyasbundle_producto';
    }
}
