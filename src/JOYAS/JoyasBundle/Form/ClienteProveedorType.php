<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClienteProveedorType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('razonSocial', 'text', array('label' => $options['razonSocialLabel']))
            ->add('cuit', 'text', array('required'=>true))
            ->add('dni','text', array('required'=>false))
            ->add('denominacion','text', array('required'=>true))
    		->add('condicioniva', 'entity', array (
    			'class' => 'JOYASJoyasBundle:CondicionIva',
    			'label' => $options['condicioniva'],
    			'query_builder' => function (\JOYAS\JoyasBundle\Entity\CondicionIvaRepository $repository)
    				 {
    					 return $repository->createQueryBuilder('u')
                                    ->where('u.estado = ?1')->setParameter(1, 'A')
                                    ->orderBy('u.descripcion');
    				 }
    				))
            ->add('provincia')
            ->add('localidad')
            ->add('domiciliocomercial', 'text', array(
                                'label' => $options['direccionLabel'],
                                'required'=>true
                                ))
            ->add('codigopostal', 'text', array(
                                'label' => 'Codigo Postal',
                                'required'=>true
                                ))
			->add('clienteProveedor', 'choice', array (
				'label' => ' Cliente o Proveedor',
				'attr'=> array('class'=>'form-control'),
				'choices' => array(
                    1 => 'Cliente',
					2 => 'Proveedor'
		   		)))
            ->add('coeficientePrecio')
			->add('saldo', 'text', array('label'=>'Saldo Inicial', 'required'=>false))
            ->add('telefono', 'text', array('label' => $options['telefonoLabel'], 'required'=>false))
            ->add('celular', 'text', array('required'=>false))
            ->add('mail', 'email', array('required'=>false))
            ->add('fax', 'text', array('required'=>false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\ClienteProveedor',
			'razonSocialLabel' =>  'Razón Social',
			'condicioniva' =>  'Condición IVA',
			'direccionLabel' =>  'Domicilio Comercial',
			'telefonoLabel' =>  'Teléfono',
			'numremito' =>  'Número de Remito'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'joyas_joyasbundle_clienteproveedor';
    }
}
