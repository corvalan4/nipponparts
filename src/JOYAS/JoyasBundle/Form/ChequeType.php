<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChequeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaemision')
            ->add('fechacobro')
            ->add('importe')
            ->add('nrocheque')
            ->add('cuit')
            ->add('firmantes')
            ->add('conciliacion')
            ->add('estado')
            ->add('movimiento')
            ->add('cuentaorigen')
            ->add('cuentadestino')
            ->add('unidadNegocio')
            ->add('cobranza')
            ->add('pago')
            ->add('banco')
            ->add('tipocheque')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Cheque'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'joyas_joyasbundle_cheque';
    }
}
