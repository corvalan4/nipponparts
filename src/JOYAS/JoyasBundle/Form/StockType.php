<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StockType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stock')
            ->add('costo')
            ->add('producto')
			->add('unidadnegocio', 'entity', array (
				'class' => 'JOYASJoyasBundle:UnidadNegocio',
				'empty_value' => false,
				'label' => 'Unidad Negocio',
				'query_builder' => function (\JOYAS\JoyasBundle\Entity\UnidadNegocioRepository $repository)
					 {
						 return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.descripcion');											 					 
					 }
					))
				;
	}
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Stock'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'joyas_joyasbundle_stock';
    }
}
