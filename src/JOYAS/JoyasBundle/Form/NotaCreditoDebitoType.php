<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NotaCreditoDebitoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $clienteproveedor = $builder->getData()->getClienteProveedor();
        if($clienteproveedor){
            $builder
                ->add('clienteProveedor', 'entity', array(
                        'class' => 'JOYASJoyasBundle:ClienteProveedor',
                        'label' => 'Cliente / Proveedor',
                        'query_builder' => function (\JOYAS\JoyasBundle\Entity\ClienteProveedorRepository $repository) use ($clienteproveedor)
                        {
                            return $repository->createQueryBuilder('u')->where('u.id = :clienteproveedor')->setParameter(':clienteproveedor', $clienteproveedor);
                        }
                    )
                );
        }else{
            $builder
                ->add('clienteProveedor', 'entity', array(
                        'class' => 'JOYASJoyasBundle:ClienteProveedor',
                        'label' => 'Cliente / Proveedor',
                        'query_builder' => function (\JOYAS\JoyasBundle\Entity\ClienteProveedorRepository $repository) use ($clienteproveedor)
                        {
                            return $repository->createQueryBuilder('u')->setMaxResults(1);
                        }
                    )
                );
        }

        $builder
            ->add('idsucursal', 'text', array(
                'mapped'=>false,
                'label'=>'ID - Sucursal',
                'required'=>false
            ))
            ->add('descripcion', 'textarea', array(
                'label'=>'Descripción',
                'required'=>false,
                'attr'=>array('class'=>'form-control', 'attr'=>'height: 200px;')
            ))
            ->add('tiponota', 'hidden', array ())
            ->add('fecha', 'date', array(
                'label'=>'Fecha Factura',
                'attr'=>array('value'=>date('Y-m-d')),
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ))
            ->add('unidadNegocio', 'entity', array (
                'class' => 'JOYASJoyasBundle:UnidadNegocio',
                'label' => 'Unidad Negocio',
                'query_builder' => function (\JOYAS\JoyasBundle\Entity\UnidadNegocioRepository $repository)
                {
                    return $repository->createQueryBuilder('u')->where('u.estado = :ACTIVO AND u.punto > 0')->orderBy('u.descripcion', 'asc')->setParameter(':ACTIVO', 'A');											 					 	 }
                )
            )

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\NotaCreditoDebito'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'joyas_joyasbundle_notacreditodebito';
    }
}
