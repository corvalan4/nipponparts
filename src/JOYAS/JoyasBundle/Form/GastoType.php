<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GastoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('importe')
            ->add('nrocomprobante')
            ->add('descripcion', 'textarea', array('attr'=>array('style'=>'height: 200px;', 'class'=>'form-control')))
            ->add('descuento')
            ->add('bonificacion')
            ->add('estado')
            ->add('fecha', 'date', array(
                'label'=>'Fecha',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ))
            ->add('puntoventa')
            ->add('unidadnegocio')
			->add('tipoGasto', 'entity', array (
				'class' => 'JOYASJoyasBundle:TipoGasto',
				'label' => 'Tipo Gasto',
				'query_builder' => function (\JOYAS\JoyasBundle\Entity\TipoGastoRepository $repository)
					 {
						 return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.nombre');
					 }
					))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Gasto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'joyas_joyasbundle_gasto';
    }
}
