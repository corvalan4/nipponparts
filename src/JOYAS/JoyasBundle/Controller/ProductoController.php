<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;

use JOYAS\JoyasBundle\Entity\Producto;
use JOYAS\JoyasBundle\Form\ProductoType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

// IMPORTACION NECESARIA PARA ARCHIVOS
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Producto controller.
 *
 */
class ProductoController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;
    /**
     * Lists all Producto entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

		$filter = array(
			'codigo'=>$request->get('codigo'),
			'descripcion'=>$request->get('descripcion'),
			'marcarepuesto'=>$request->get('marcarepuesto'),
			'marcaauto'=>$request->get('marcaauto'),
			'modelo'=>$request->get('modelo'),
			'categoria'=>$request->get('categoria'),
			'subcategoria'=>$request->get('subcategoria'),
			'rubro'=>$request->get('rubro')
		);
        $entities = $em->getRepository('JOYASJoyasBundle:Producto')->findByFilter($filter);
        $rubros = $em->getRepository('JOYASJoyasBundle:Rubro')->findBy(array('estado'=>'A'), array('descripcion'=>'ASC'));
        $categorias = $em->getRepository('JOYASJoyasBundle:Categoria')->findBy(array('estado'=>'A'), array('descripcion'=>'ASC'));
        $marcas = $em->getRepository('JOYASJoyasBundle:Marca')->findBy(array('estado'=>'A'), array('descripcion'=>'ASC'));

        return $this->render('JOYASJoyasBundle:Producto:index.html.twig', array(
            'entities' => $entities,
            'marcas' => $marcas,
            'rubros' => $rubros,
            'categorias' => $categorias,
        ));
    }

    public function predictivaAction(Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		$search = $request->get('data');
	    $string = '<ul>';
		if(strlen($search) >= 2){
			$productos = $em->getRepository('JOYASJoyasBundle:Producto')->predictiva($search);
			foreach($productos as $prod){
				$string = $string.'<li class=suggest-element codigo='.$prod->getCodigo().' id='.$prod->getId().'><a href="#" data='.$prod->getId().' id=producto'.$prod->getId().'>'.$prod->getDescripcion().' ( '.$prod->getCodigo().' )';
			}
		}
		$response = $string;
		return new Response($response);
    }


    public function predictivaUnidadAction(Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		$search = $request->get('data');
		$idUnidad = $request->get('idunidad');
	    $string = '<ul>';
		if(strlen($search) >= 2){
			$stocks = $em->getRepository('JOYASJoyasBundle:Stock')->predictiva($search, $idUnidad);
			foreach($stocks as $prod){
				$string = $string.'<li class=suggest-element codigo='.$prod->getProducto()->getCodigo().' id='.$prod->getId().'><a href="#" data='.$prod->getId().' id=producto'.$prod->getId().'>'.$prod->getProducto()->getDescripcion().' ( '.$prod->getProducto()->getCodigo().' )';
			}
		}
		$response = $string;
		return new Response($response);
    }

    public function listadoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Producto')->findAll();

        return $this->render('JOYASJoyasBundle:Producto:listado.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function listadocategoriaAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Producto')->listadocategoria($this->sessionManager->getSession('iddependencia'));

		$listado = new ArrayCollection();

		foreach($entities as $entity){
			$categoria= $em->getRepository('JOYASJoyasBundle:Categoria')->find($entity['id']);
			$entidad = array('stock'=>$entity['stock'], "nombrecategoria"=>$categoria->getNombre());
			$listado->add($entidad);
		}

        return $this->render('JOYASJoyasBundle:Producto:listadocategoria.html.twig', array(
            'entities' => $listado,
        ));
    }
    /**
     * Creates a new Producto entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Producto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
		$em = $this->getDoctrine()->getManager();

        $usuarioSession = $this->sessionManager->getSession('usuario');

        $usuario = $em->getRepository('JOYASJoyasBundle:Usuario')->find($usuarioSession->getId());
		$entity->setUsuario($usuario);
        if ($form->isValid()) {
			$em->persist($entity);
            $em->flush();
			$entity->setCodigo("NP".str_pad($entity->getId(), 6, "0", STR_PAD_LEFT));
            $em->flush();

            $this->sessionManager->addFlash('msgOk', 'Producto creado con exito.');
            return $this->redirect($this->generateUrl('producto_show', array('id'=>$entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:Producto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Producto entity.
     *
     * @param Producto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Producto $entity)
    {
        $form = $this->createForm(new ProductoType($this->sessionManager->getSession('iddependencia')), $entity, array(
            'action' => $this->generateUrl('producto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Producto entity.
     *
     */
    public function newAction()
    {
        $entity = new Producto();
        $form   = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:Producto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Producto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);

//		$image = stream_get_contents($entity->getImagen());

        return $this->render('JOYASJoyasBundle:Producto:show.html.twig', array(
            'entity'      => $entity,
//			'imagen'      => $image,
        ));
    }

    /**
     * Displays a form to edit an existing Producto entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Producto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Producto entity.
    *
    * @param Producto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Producto $entity)
    {
        $form = $this->createForm(new ProductoType($this->sessionManager->getSession('iddependencia')), $entity, array(
            'action' => $this->generateUrl('producto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }
    /**
     * Edits an existing Producto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

		if ($editForm->isValid()) {
            $this->sessionManager->addFlash('msgOk', 'Producto editado con exito.');
            $em->flush();

			return $this->redirect($this->generateUrl('producto'));
       }

        return $this->render('JOYASJoyasBundle:Producto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Producto entity.
     *
     */
	public function deleteAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);
		$entity->setEstado("E");
		$em->flush();

        return $this->redirect($this->generateUrl('producto'));
    }

    public function activarAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);
		$entity->setEstado("A");
		$em->flush();

        return $this->redirect($this->generateUrl('producto'));
    }

    /**
     * Creates a form to delete a Producto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('producto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
