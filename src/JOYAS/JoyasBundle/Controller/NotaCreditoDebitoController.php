<?php

namespace JOYAS\JoyasBundle\Controller;

use JOYAS\JoyasBundle\Entity\ItemNota;
use JOYAS\JoyasBundle\Entity\Iva;
use JOYAS\JoyasBundle\Entity\PagoAsignacion;
use Symfony\Bridge\Propel1\Tests\Fixtures\Item;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\NotaCreditoDebito;
use JOYAS\JoyasBundle\Form\NotaCreditoDebitoType;

use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * NotaCreditoDebito controller.
 *
 */
class NotaCreditoDebitoController extends Controller
{

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all NotaCreditoDebito entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:NotaCreditoDebito')->findAll();

        return $this->render('JOYASJoyasBundle:NotaCreditoDebito:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new NotaCreditoDebito entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new NotaCreditoDebito();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $hayProductos = false;
        $total = 0;
        if ($form->isValid()) {
            for($i = 1; $i <=30; $i++ ){
                $elementonombre = $request->get("elemento$i");
                if(!empty($elementonombre)){
                    $cantidad = $request->get("cantidad$i");
                    $precio = $request->get("precio$i");
                    $total+=$cantidad*$precio;

                    $item = new ItemNota();
                    $item->setPrecio($precio);
                    $item->setCantidad($cantidad);
                    $item->setDescripcion($elementonombre);
                    $item->setNotadebitocredito($entity);
                    $em->persist($item);

                    $hayProductos = true;
                    if($entity->getClienteProveedor()->getCondicioniva() == 'Responsable Inscripto'){
                        $entity->setCodigonota("A");
                    }
                }
            }
            if(!$hayProductos){
                $this->sessionManager->addFlash('msgError','Debe agregar items.');
                return $this->render('JOYASJoyasBundle:NotaCreditoDebito:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ));
            }

            $entity->setPtovta($entity->getUnidadNegocio()->getPunto());
            $entity->setImporte($total);

            // Consultar
            if($entity->getCodigonota()=='A'){
                $tipoIva = $em->getRepository('JOYASJoyasBundle:TipoIva')->findOneBy(array('porcentaje'=>21));
                $iva = new Iva();
                if($tipoIva){$iva->setTipoIva($tipoIva);}
                $iva->setNotaCreditoDebito($entity);
                $montoIva = $total-($total/1.21);
                $iva->setValor(round($montoIva, 2));
                $em->persist($iva);

                $entity->addIva($iva);
            }

            // Se asigna Pago.
            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setNotaCreditoDebito($entity);
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setImporte($total);

            $em->persist($pagoAsignacion);
            $em->persist($entity);
            $em->flush();

            $this->sessionManager->facturarNota($entity);

            $em->flush();

            return $this->redirect($this->generateUrl('notacreditodebito_show', array('id' => $entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:NotaCreditoDebito:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a NotaCreditoDebito entity.
     *
     * @param NotaCreditoDebito $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(NotaCreditoDebito $entity)
    {
        $form = $this->createForm(new NotaCreditoDebitoType(), $entity, array(
            'action' => $this->generateUrl('notacreditodebito_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear Nota', 'attr'=>array('class'=>'btn btn-success', 'style'=>'float:right')));

        return $form;
    }

    /**
     * Displays a form to create a new NotaCreditoDebito entity.
     *
     */
    public function newAction($id_clienteproveedor, $tipo)
    {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        if(empty($tipo) or empty($id_clienteproveedor)){
            return $this->redirect($this->generateUrl("joyas_joyas_homepage"));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = new NotaCreditoDebito();
        $clienteproveedor = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($id_clienteproveedor);
        $entity->setClienteProveedor($clienteproveedor);
        if($this->sessionManager->getSession('usuario')->getPerfil() != 'ADMINISTRADOR'){
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionManager->getUnidad()->getId());
            $entity->setUnidadNegocio($unidad);
        }

        $entity->setTiponota($tipo);
        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:NotaCreditoDebito:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a NotaCreditoDebito entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:NotaCreditoDebito')->find($id);
        switch ($entity->getCodigonota()) {
            case 'A':
                $nrocomprobante = strtoupper($entity->getTiponota())=='CREDITO' ? '03' : '02';
                break;
            case 'B':
                $nrocomprobante = strtoupper($entity->getTiponota())=='CREDITO' ? '08' : '07';
                break;
        }
        $codigobarras = $this->sessionManager->obtenerTextoCodigoBarras(
            str_replace("-", "", $entity->getClienteProveedor()->getCuit()),
            $nrocomprobante,
            $entity->getPtovta(),
            $entity->getCae(),
            $entity->getFechavtocae()->format('Ymd')
        );
        return $this->render('JOYASJoyasBundle:NotaCreditoDebito:show'.$entity->getCodigonota().'.html.twig', array(
            'entity' => $entity,
            'tipofactura' => $entity->getCodigonota(),
            'nrocomprobante' => $nrocomprobante,
            'codigobarras' => $codigobarras
        ));

    }

    /**
     * Displays a form to edit an existing NotaCreditoDebito entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:NotaCreditoDebito')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaCreditoDebito entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:NotaCreditoDebito:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a NotaCreditoDebito entity.
     *
     * @param NotaCreditoDebito $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(NotaCreditoDebito $entity)
    {
        $form = $this->createForm(new NotaCreditoDebitoType(), $entity, array(
            'action' => $this->generateUrl('notacreditodebito_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing NotaCreditoDebito entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:NotaCreditoDebito')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NotaCreditoDebito entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('notacreditodebito_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:NotaCreditoDebito:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a NotaCreditoDebito entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:NotaCreditoDebito')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find NotaCreditoDebito entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('notacreditodebito'));
    }

    /**
     * Creates a form to delete a NotaCreditoDebito entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('notacreditodebito_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}
