<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\Cheque;
use JOYAS\JoyasBundle\Entity\MovimientoBancario;
use JOYAS\JoyasBundle\Form\ChequeType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Cheque controller.
 *
 */
class ChequeController extends Controller
{

  /**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

    /**
     * Lists all Cheque entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findBy(array('estado'=>'A'));
        $cuentas = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->findBy(array('estado'=>'A'));
        if($this->sessionManager->getPerfil()!='ADMINISTRADOR'){
            $entities = $em->getRepository('JOYASJoyasBundle:Cheque')
                           ->findBy(array('unidadNegocio'=>$this->sessionManager->getUnidad()->getId()));
        }else{
            $entities = $em->getRepository('JOYASJoyasBundle:Cheque')->findAll();
        }

        return $this->render('JOYASJoyasBundle:Cheque:index.html.twig', array(
            'entities' => $entities,
            'unidades' => $unidades,
            'cuentas' => $cuentas,
        ));
    }
    /**
     * Creates a new Cheque entity.
     *
     */
    public function createAction(Request $request)
    {
		$em = $this->getDoctrine()->getManager();

		$unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidad'));

        $banco = $em->getRepository('JOYASJoyasBundle:Banco')->find($request->get('banco'));
        $cuenta = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->find($request->get('cuenta'));
        $tipocheque = $em->getRepository('JOYASJoyasBundle:TipoCheque')->find($request->get('tipocheque'));
        $cheque = new Cheque();
        $cheque->setImporte($request->get('importe'));
        $cheque->setFirmantes($request->get('firmantes'));
        $cheque->setCuit($request->get('cuit'));
        $cheque->setFechacobro(new \DateTime($request->get('fechacobro')));
        $cheque->setFechaemision(new \DateTime($request->get('fechaemision')));
        $cheque->setNrocheque($request->get('nrocheque'));
        $cheque->setBanco($banco);
        $cheque->setTipocheque($tipocheque);
        $cheque->setUnidadNegocio($unidad);
        $cheque->setCuentaorigen($cuenta);
        $em->persist($cheque);
        $em->flush();

		$this->sessionManager->addFlash('msgOk','Cheque registrado correctamente.');
		return $this->redirect($this->generateUrl('cheque'));
    }

    /**
     * Creates a form to create a Cheque entity.
     *
     * @param Cheque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cheque $entity)
    {
        $form = $this->createForm(new ChequeType(), $entity, array(
            'action' => $this->generateUrl('cheque_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Cheque entity.
     *
     */
    public function newAction(Request $request)
    {
		if(!$this->sessionManager->isLogged()){
			return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
		}

        $em = $this->getDoctrine()->getManager();
		if($this->sessionManager->getPerfil()!='ADMINISTRADOR'){
    		$unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionManager->getUnidad()->getId());
		}else{
			if(empty($request->get('unidad'))){
				return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
			}
	   	    $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidad'));
		}
		$tipocheques = $em->getRepository('JOYASJoyasBundle:TipoCheque')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
		$bancos = $em->getRepository('JOYASJoyasBundle:Banco')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
		$cuentas = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));

		return $this->render('JOYASJoyasBundle:Cheque:new.html.twig', array(
			'tipocheques'=>$tipocheques,
			'cuentas'=>$cuentas,
			'bancos'=>$bancos,
			'unidadnegocio'=>$unidad
		));
    }

    /**
     * Finds and displays a Cheque entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cheque entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Cheque:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Cheque entity.
     *
     */
    public function editAction($id)
    {
		if(!$this->sessionManager->isLogged()){
			return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
		}

        $em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);
		$tipocheques = $em->getRepository('JOYASJoyasBundle:TipoCheque')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
		$bancos = $em->getRepository('JOYASJoyasBundle:Banco')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
		$cuentas = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));

		return $this->render('JOYASJoyasBundle:Cheque:edit.html.twig', array(
			'tipocheques'=>$tipocheques,
			'entity'=>$entity,
			'bancos'=>$bancos,
            'cuentas'=>$cuentas,
		));
    }
	public function depositarAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();

        $cheque = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);
        if($request->get('idcuenta') != 0){
            $cuentabancaria = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->find($request->get('idcuenta'));

            $cheque->setEstado('D');
            $movimiento = new MovimientoBancario();
            $movimiento->setCheque($cheque);
            $movimiento->setCuentabancaria($cuentabancaria);
            $movimiento->setTipoMovimiento('CH');
            $movimiento->setValor($cheque->getImporte());
            $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
            $em->persist($movimiento);
			$cheque->setMovimiento($movimiento);
			$cheque->setCuentadestino($cuentabancaria);
            $cuentabancaria->setMovimiento($movimiento);
            $em->flush();

    		$this->sessionManager->addFlash('msgOk','Cheque Depositado correctamente.');
        }else{
    		$this->sessionManager->addFlash('msgWarn','Debe seleccionar Cuenta Bancaria.');
        }
		return $this->redirect($this->generateUrl('cheque'));
    }

    public function conciliarAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
        $cheque = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);
        $cheque->setConciliacion('SI');
        $cheque->setEstado('C');
        if($cheque->getCuentaorigen()){
            // $movimiento = new MovimientoBancario();
            // $movimiento->setCheque($cheque);
            // $movimiento->setCuentabancaria($cheque->getCuentaorigen());
            // $movimiento->setTipoMovimiento('CH');
            // $movimiento->setValor($cheque->getImporte());
            // $movimiento->setUnidadNegocio($cheque->getUnidadNegocio());
            // $em->persist($movimiento);
            // $cheque->setMovimiento($movimiento);
            // $cheque->getCuentaorigen()->setMovimiento($movimiento);
        }
        $em->flush();

		$this->sessionManager->addFlash('msgOk','Cheque Conciliado correctamente.');
		return $this->redirect($this->generateUrl('cheque'));
    }

    /**
    * Creates a form to edit a Cheque entity.
    *
    * @param Cheque $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Cheque $entity)
    {
        $form = $this->createForm(new ChequeType(), $entity, array(
            'action' => $this->generateUrl('cheque_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Cheque entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();

        $cheque = $em->getRepository('JOYASJoyasBundle:Cheque')->find($request->get('idcheque'));

        $banco = $em->getRepository('JOYASJoyasBundle:Banco')->find($request->get('banco'));
        $cuenta = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->find($request->get('cuenta'));
        $tipocheque = $em->getRepository('JOYASJoyasBundle:TipoCheque')->find($request->get('tipocheque'));

        $cheque->setImporte($request->get('importe'));
        $cheque->setFirmantes($request->get('firmantes'));
        $cheque->setCuit($request->get('cuit'));
        $cheque->setFechacobro(new \DateTime($request->get('fechacobro')));
        $cheque->setFechaemision(new \DateTime($request->get('fechaemision')));
        $cheque->setNrocheque($request->get('nrocheque'));
        $cheque->setBanco($banco);
        $cheque->setTipocheque($tipocheque);
        $cheque->setCuentaorigen($cuenta);

        $em->flush();

		$this->sessionManager->addFlash('msgOk','Cheque editado correctamente.');
		return $this->redirect($this->generateUrl('cheque'));
    }
    /**
     * Deletes a Cheque entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cheque entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cheque'));
    }

    /**
     * Creates a form to delete a Cheque entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cheque_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
	public function cambiarestadoAction(Request $request){
        $estado = $request->get('estado');
        $id = $request->get('id');
        if(isset($estado)){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);
            $entity->setEstado($estado);
            $em->flush();
    		$this->sessionManager->addFlash('msgOk','Accion realizada.');
        }
		return $this->redirect($this->generateUrl('cheque'));
    }
}
