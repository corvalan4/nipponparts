<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ps\PdfBundle\Annotation\Pdf;
use Doctrine\Common\Collections\ArrayCollection;
use JOYAS\JoyasBundle\Entity\Factura;
use JOYAS\JoyasBundle\Entity\PagoAsignacion;
use JOYAS\JoyasBundle\Entity\ProductoFactura;
use JOYAS\JoyasBundle\Entity\Iva;
use JOYAS\JoyasBundle\Entity\Stock;
use JOYAS\JoyasBundle\Entity\TipoIva;
use JOYAS\JoyasBundle\Entity\Producto;
use JOYAS\JoyasBundle\Form\FacturaType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;

/**
 * Factura controller.
 *
 */
class FacturaProveedorController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all FacturaProveedor entities.
     *
     */
    public function indexAction($page = 1) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $this->sessionManager->setSession('fechaDesde', '');
        $this->sessionManager->setSession('fechaHasta', '');
        $this->sessionManager->setSession('listado', '');
        $em = $this->getDoctrine()->getManager();

        if ($this->sessionManager->getPerfil() != 'ADMINISTRADOR') {
            $entities = $em->getRepository('JOYASJoyasBundle:Factura')->findBy(array('unidadNegocio' => $this->sessionManager->getUnidad()->getId(), 'tipo' => 'FP'));
        } else {
            $entities = $em->getRepository('JOYASJoyasBundle:Factura')->findBy(array('estado' => 'A', 'tipo' => 'FP'));
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findBy(array('estado' => 'A'));
        }
        $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('clienteProveedor' => 2));

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(80);
        $paginador->setCurrentPage($page);

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        return $this->render('JOYASJoyasBundle:FacturaProveedor:index.html.twig', array(
                    'entities' => $paginador,
                    'clientesProveedores' => $clientesProveedores,
                    'unidades' => $unidades,
        ));
    }

    public function filtroAction(Request $request, $page = 1) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        if ($request->get('fechaDesde') == '' and $request->get('fechaHasta') == '' and $request->get('listado') == '') {
            $desde = new \DateTime($this->sessionManager->getSession('fechaDesde'));
            $hasta = new \DateTime($this->sessionManager->getSession('fechaHasta'));
            $listado = $this->sessionManager->getSession('listado');
        } else {
            if ($request->get('fechaDesde') == '' and $request->get('fechaHasta') == '') {
                $desde = new \DateTime('1/1/1996');
                $hasta = new \DateTime('NOW');
            } else {
                $desde = new \DateTime($request->get('fechaDesde'));
                $hasta = new \DateTime();
            }
            $listado = $request->get('listado');
            $this->sessionManager->setSession('fechaDesde', $desde->format('Y-m-d'));
            $this->sessionManager->setSession('fechaHasta', $hasta->format('Y-m-d'));
            $this->sessionManager->setSession('getAllActivas', $request->get('listado'));
        }

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        if ($this->sessionManager->getPerfil() != 'ADMINISTRADOR') {
            $facturas = $em->getRepository('JOYASJoyasBundle:Factura')->getPorFechas($this->sessionManager->getUnidad(), $desde, $hasta, $listado, 'FP');
        } else {
            $facturas = $em->getRepository('JOYASJoyasBundle:Factura')->getPorFechas($request->get('unidadnegocio'), $desde, $hasta, $listado, 'FP');
        }

        $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('clienteProveedor' => 2));

        $adapter = new ArrayAdapter($facturas);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(80);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:FacturaProveedor:index.html.twig', array(
                    'entities' => $paginador,
                    'clientesProveedores' => $clientesProveedores,
                    'unidades' => $unidades,
        ));
    }

    /**
     * Creates a new FacturaProveedorentity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
       
        $em = $this->getDoctrine()->getManager();
        $entity = new Factura();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $entity->setTipo("FP");
        $mes = $form->get("mes")->getData();
        $anio = $form->get("anio")->getData();
        $entity->setMesanio($mes . "/" . $anio);

        $sucursalstr = $form->get("idsucursal")->getData();
        if (!empty($sucursalstr)) {
            $sucursal = $em->getRepository('JOYASJoyasBundle:Sucursal')->find($sucursalstr);
            $entity->setSucursal($sucursal);
            $entity->setClienteProveedor($sucursal->getClienteproveedor());
        }

        $importetotal = $request->get("importetotal");
        $entity->setImporte($importetotal);

        $total = 0;

        if ($form->isValid()) {
            $bandera=0;
            for ($i = 1; $i <= 30; $i++) {
                
                $idelemento = $request->get("idelemento$i");
                if (!empty($idelemento)) {
                    $producto = $em->getRepository('JOYASJoyasBundle:Producto')->find($idelemento);
                    if ($producto) {
                        $cantidad = $request->get("cantidad$i");
                        $precio = $request->get("precioReal$i");
                       
                        $alicuota = $request->get("alicuota$i"); // Ver que hacer con esto.

                        $elementostock = new Stock();
                        $elementostock->setFecha($entity->getFecha());
                        $elementostock->setCosto($precio);
                        $elementostock->setProducto($producto);
                        $elementostock->setStock($cantidad);
                        $elementostock->setUnidadnegocio($entity->getUnidadnegocio());

                        $em->persist($elementostock);

                        $total += $cantidad * $precio;

                        $productofactura = new ProductoFactura();
                        $productofactura->setStock($elementostock);
                        $productofactura->setFactura($entity);
                        $productofactura->setCantidad($cantidad);
                        $productofactura->setPrecio($precio);
                        $elementostock->setStock($cantidad);
                        $em->persist($productofactura);
                    }
                }
            }
 
            $tipoIva21 = $em->getRepository('JOYASJoyasBundle:TipoIva')->findOneBy(array('porcentaje' => 21));
            $tipoIva105 = $em->getRepository('JOYASJoyasBundle:TipoIva')->findOneBy(array('porcentaje' => 10.5));

            $importeiva21 = $request->get("importeiva21");
            $importeiva105 = $request->get("importeiva105");
            if (!empty($importeiva21) and $importeiva21 > 0) {
                $iva = new Iva();
                if ($tipoIva21) {
                    $iva->setTipoIva($tipoIva21);
                }
                $iva->setFactura($entity);
                $iva->setValor(round($importeiva21, 2));
                $em->persist($iva);
            }
            if (!empty($importeiva105) and $importeiva105 > 0) {
                $iva = new Iva();
                if ($importeiva105) {
                    $iva->setTipoIva($tipoIva105);
                }
                $iva->setFactura($entity);
                $iva->setValor(round($importeiva105, 2));
                $em->persist($iva);
                $entity->addIva($iva);
            }
            $puntoVta= $request->get("puntoventa");
            if (!$puntoVta){
                $puntoVta=0;
            }
          
            $entity->setPtovta($puntoVta);
            $entity->setNrofactura($request->get("nrofactura"));

            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setFactura($entity);
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setImporte($entity->getImporte());
            $em->persist($pagoAsignacion);

            $em->persist($entity);

            $em->flush();
            $this->sessionManager->addFlash("msgOk", "Factura creada exitosamente.");
            return $this->redirect($this->generateUrl('facturaproveedor'));
        }

        $this->sessionManager->addFlash("msgError", "Error al dar de alta la factura.");
        return $this->render('JOYASJoyasBundle:FacturaProveedor:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function refacturarAction($id) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);
        $this->sessionManager->facturar($entity);

        return $this->redirect($this->generateUrl('facturaproveedor'));
    }

    /**
     * Creates a form to create a Factura entity.
     *
     * @param Factura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Factura $entity) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new FacturaType(), $entity, array(
            'action' => $this->generateUrl('facturaproveedor_create'),
            'method' => 'POST',
        ));

        $form->add('cae', 'text', array('label' => 'CAE', 'required' => true, 'attr' => array('minlength' => '14', 'maxlength' => '14')));
        $form->add('fechavtocae', 'date', array(
            'label' => 'Fecha Vto. CAE',
            'attr' => array('value' => date('Y-m-d')),
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd'
        ));

        $form->add('mes', 'integer', array(
            'label' => 'Mes',
            'required' => true,
            'mapped' => false,
            'attr' => array('min' => 1, 'max' => 12)));
        $form->add('anio', 'integer', array(
            'label' => 'Año',
            'required' => true,
            'mapped' => false,
            'attr' => array('min' => 2017, 'max' => date('Y'))));
        $form->add('subtotal', 'text', array(
            'mapped' => false,
            'label' => 'Subtotal',
            'attr' => array('readonly' => 'readonly')
        ));
        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-success', 'style' => 'float:right;')));

        return $form;
    }

    /**
     * Displays a form to create a new Factura entity.
     *
     */
    public function newAction(Request $request) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = new Factura();
        if ($this->sessionManager->getSession('usuario')->getPerfil() != 'ADMINISTRADOR') {
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionManager->getUnidad()->getId());
            $entity->setUnidadNegocio($unidad);
        }

        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:FacturaProveedor:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function remitofacturaAction($id) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        if ($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion() == 'Responsable Inscripto') {
            return $this->render('JOYASJoyasBundle:FacturaProveedor:remitofacturaA.html.twig', array(
                        'entity' => $entity,
            ));
        } else {
            return $this->render('JOYASJoyasBundle:FacturaProveedor:remitofacturaB.html.twig', array(
                        'entity' => $entity,
            ));
        }
    }

    public function facturaremitoAction($id) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        return $this->render('JOYASJoyasBundle:FacturaProveedor:facturaremito.html.twig', array(
                    'entity' => $entity,
        ));
    }

    public function crearfacturaremitoAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($request->get('id'));
        $entity->setNroremito($request->get('nroremito'));
        $contador = count($entity->getProductosFactura());
        for ($x = 1; $x <= $contador; $x++) {
            $id = $request->get('id' . $x);
            $this->sessionManager->setSession('id' . $id, $id);
            if (!is_null($id) and $id != '') {
                $productoFactura = $em->getRepository('JOYASJoyasBundle:ProductoFactura')->find($id);
                $productoFactura->setPertenece('RF');
            }
        }
        $em->flush();

        $this->sessionManager->addFlash('msgOk', 'Remito registrado.');

        return $this->redirect($this->generateUrl('factura_show', array('id' => $entity->getId(), 'tipo' => 'R')));
    }

    public function showAction(Request $request, $id) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);
        return $this->render('JOYASJoyasBundle:FacturaProveedor:show.html.twig', array(
                    'entity' => $entity,
                    'nrocomprobante' => '01'
        ));
    }

    public function imprimirAction($id) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }
        return $this->render('JOYASJoyasBundle:FacturaProveedor:imprimir.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Displays a form to edit an existing Factura entity.
     *
     */
    public function editAction($id) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('JOYASJoyasBundle:FacturaProveedor:edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $editForm->createView()
        ));
    }

    /**
     * Creates a form to edit a Factura entity.
     *
     * @param Factura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Factura $entity) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new FacturaType(), $entity, array(
            'action' => $this->generateUrl('facturaproveedor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('cae', 'text', array('label' => 'CAE', 'read_only' => true, 'required' => true, 'attr' => array('minlength' => '14', 'maxlength' => '14')));
        $form->add('fechavtocae', 'date', array(
            'label' => 'Fecha Vto. CAE',
            'read_only' => true,
            'attr' => array('value' => date('Y-m-d')),
            'widget' => 'single_text',
            'data' => $entity->getFechavtocae(),
            'format' => 'yyyy-MM-dd'
        ));
        $mes = $entity->getMesanio() ? explode('/', $entity->getMesanio())[0] : '';
        $anio = $entity->getMesanio() ? explode('/', $entity->getMesanio())[1] : '';
        $form->add('mes', 'integer', array(
            'label' => 'Mes',
            'read_only' => true,
            'required' => true,
            'mapped' => false,
            'data' => $mes,
            'attr' => array('min' => 1, 'max' => 12)));
        $form->add('anio', 'integer', array(
            'label' => 'Año',
            'required' => true,
            'read_only' => true,
            'mapped' => false,
            'data' => $anio,
            'attr' => array('min' => 2017, 'max' => date('Y'))));
        $form->add('subtotal', 'text', array(
            'mapped' => false,
            'label' => 'Subtotal',
            'attr' => array('readonly' => 'readonly')
        ));

        $form->add('submit', 'submit', array('label' => 'Completar Factura', 'attr' => array('class' => 'btn btn-success', 'style' => 'float:right;')));

        return $form;
    }

    /**
     * Edits an existing Factura entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Factura entity.');
        }
       
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $importetotal = $request->get("importetotal");
        $entity->setImporte($importetotal);

        if ($editForm->isValid()) {
            $bandera=0;
            for ($i = 1; $i <= 30; $i++) {
                $idelemento = $request->get("idelemento$i");
                if (!empty($idelemento)) {
                    $bandera=1;
                    $producto = $em->getRepository('JOYASJoyasBundle:Producto')->find($idelemento);
                    if ($producto) {
                        $cantidad = $request->get("cantidad$i");
                        $precio = $request->get("precioReal$i");

                        $alicuota = $request->get("alicuota$i"); // Ver que hacer con esto.

                        $elementostock = new Stock();
                        $elementostock->setFecha(new \DateTime('NOW'));
                        $elementostock->setCosto($precio);
                        $elementostock->setProducto($producto);
                        $elementostock->setStock($cantidad);
                        $elementostock->setUnidadnegocio($entity->getUnidadnegocio());

                        $em->persist($elementostock);

                        $productofactura = new ProductoFactura();
                        $productofactura->setStock($elementostock);
                        $productofactura->setFactura($entity);
                        $productofactura->setCantidad($cantidad);
                        $productofactura->setPrecio($precio);
                        $elementostock->setStock($cantidad);
                        $em->persist($productofactura);
                    }
                }
            }

            $tipoIva21 = $em->getRepository('JOYASJoyasBundle:TipoIva')->findOneBy(array('porcentaje' => 21));
            $tipoIva105 = $em->getRepository('JOYASJoyasBundle:TipoIva')->findOneBy(array('porcentaje' => 10.5));

            $importeiva21 = $request->get("importeiva21");
            $importeiva105 = $request->get("importeiva105");

            if (!empty($importeiva21) and $importeiva21 > 0) {
                $iva21 = $em->getRepository('JOYASJoyasBundle:Iva')->findOneBy(array('tipoIva' => $tipoIva21, 'factura' => $entity));
                if ($iva21) {
                    $iva21->setValor(round($importeiva21, 2));
                } else {
                    $iva21 = new Iva();
                    if ($tipoIva21) {
                        $iva21->setTipoIva($tipoIva21);
                    }
                    $iva21->setFactura($entity);
                    $iva21->setValor(round($importeiva21, 2));
                    $em->persist($iva21);
                    $entity->addIva($iva21);
                }
            }
            if (!empty($importeiva105) and $importeiva105 > 0) {
                $iva105 = $em->getRepository('JOYASJoyasBundle:Iva')->findOneBy(array('tipoIva' => $tipoIva105, 'factura' => $entity));
                if ($iva105) {
                    $iva105->setValor(round($importeiva21, 2));
                } else {
                    $iva105 = new Iva();
                    if ($importeiva105) {
                        $iva105->setTipoIva($tipoIva105);
                    }
                    $iva105->setFactura($entity);
                    $iva105->setValor(round($importeiva105, 2));
                    $em->persist($iva105);
                    $entity->addIva($iva105);
                }
            }

            $em->persist($entity);

            $em->flush();
            if ($bandera==1){
            $this->sessionManager->addFlash("msgOk", "Factura Completa.");
            }else {
                $this->sessionManager->addFlash("msgOk", "Factura editada.");
            }
            return $this->redirect($this->generateUrl('facturaproveedor'));
        }

        $this->sessionManager->addFlash("msgError", "Error al editar la factura.");

        return $this->render('JOYASJoyasBundle:FacturaProveedor:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Factura entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

        foreach ($entity->getProductosFactura() as $productofactura) {
            $stock = $productofactura->getStock()->getStock() + $productofactura->getCantidad();
            $productofactura->getStock()->setStock($stock);
        }

        $entity->setEstado("E");
        $em->flush();

        $this->sessionManager->addFlash("msgOk", "Entrega eliminada con éxito.");

        return $this->redirect($this->generateUrl('facturaproveedor'));
    }

    /**
     * Creates a form to delete a Factura entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('factura_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function verclienteAction(Request $request) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $search = $request->get('data');
        $string = '';
        $cliente = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findOneBy(array('cuit' => $search));
        if (empty($cliente)) {
            $cliente = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findOneBy(array('dni' => $search));
        }
        if (empty($cliente)) {
            $response = '0///0';
        } else {
            $response = $cliente->getId() . '///' . $cliente->getRazonSocial();
        }
        return new Response($response);
    }

    public function altaclienteAction(Request $request) {
        if (!$this->sessionManager->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $condicion = $em->getRepository('JOYASJoyasBundle:CondicionIva')->find(1);
        $tipo = $request->get('data');
        $nrodoc = $request->get('data1');
        $direc = $request->get('data2');
        $razon = $request->get('data3');
        $telefono = $request->get('data4');
        $email = $request->get('data5');

        $respuesta = '0';

        $cliente = new ClienteProveedor();
        $cliente->setRazonSocial($razon);
        if ($tipo == 'CUIT') {
            $cliente->setCuit(str_replace('-', '', $nrodoc));
        } else {
            $cliente->setDni($nrodoc);
        }
        $cliente->setCondicioniva($condicion);
        $cliente->setDomiciliocomercial($direc);
        $cliente->setTelefono($telefono);
        $cliente->setMail($email);

        $em->persist($cliente);
        $em->flush();

        $respuesta = $nrodoc;

        return new Response($respuesta);
    }

}

?>
