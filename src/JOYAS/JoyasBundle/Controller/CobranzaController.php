<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\Cobranza;
use JOYAS\JoyasBundle\Entity\CobranzaFactura;
use JOYAS\JoyasBundle\Entity\Retencion;
use JOYAS\JoyasBundle\Entity\PagoAsignacion;
use JOYAS\JoyasBundle\Entity\MedioDocumento;
use JOYAS\JoyasBundle\Entity\Cheque;
use JOYAS\JoyasBundle\Form\CobranzaType;

use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Cobranza controller.
 *
 */
class CobranzaController extends Controller
{
    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;
    /**
     * Lists all Cobranza entities.
     *
     */
    public function indexAction(Request $request)
    {
        if(!$this->sessionManager->isLogged()){
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        if(!isset($page)){
            $page = 1;
        }
        $desde = $request->get('desde');
        if(empty($desde)){
            $desde = new \DateTime('NOW -30 days');
        }else{
            $desde = new \DateTime($request->get('desde'));
        }

        $hasta = $request->get('hasta');
        if(empty($hasta)){
            $hasta = new \DateTime('NOW +1 days');
        }else{
            $hasta = new \DateTime($request->get('hasta'));
        }

        $em = $this->getDoctrine()->getManager();

        if($this->sessionManager->getPerfil()!='ADMINISTRADOR'){
            $entities = $em->getRepository('JOYASJoyasBundle:Cobranza')->filtro($desde, $hasta, $this->sessionManager->getUnidad()->getId());
        }else{
            $entities = $em->getRepository('JOYASJoyasBundle:Cobranza')->filtro($desde, $hasta, $request->get('unidad'));
        }

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(300);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Cobranza:index.html.twig', array(
            'entities' => $paginador,
            'unidades' => $unidades,
        ));

    }
    /**
     * Creates a new Cobranza entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Cobranza();
        $form = $this->createCreateForm($entity);

        $form
            ->add('facturas', 'entity', array (
                'class' => 'JOYASJoyasBundle:Factura',
                'label' => 'Facturas',
                'mapped'=>false,
                'multiple'=>true
            ));
        $form->handleRequest($request);

        $medios = $em->getRepository('JOYASJoyasBundle:Medio')->findBy(array(), array('descripcion' => 'ASC'));

        $facturasAsociadas = $form['facturas']->getData();

        $idfactura = $request->get('idfactura');
        $factura = null;
        if(!empty($idfactura)){
            $factura = $em->getRepository('JOYASJoyasBundle:Factura')->find($idfactura);
        }

        if ($this->sessionManager->getPerfil() == 'ADMINISTRADOR') {
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidad'));
            $entity->setUnidadnegocio($unidad);
        } else {
            $entity->setUnidadnegocio($this->sessionManager->getUnidad());
        }


        $importemedios = 0;
        foreach ($medios as $medio) {
            $importemedio = $request->get('importemedio' . $medio->getId());
            $numerocupon = $request->get('numerocupon' . $medio->getId());
            $numerotransaccion = $request->get('numerotransaccion' . $medio->getId());
            $cuotas = $request->get('cuotas' . $medio->getId());
            if (!empty($importemedio)) {
                $mediodocumento = new MedioDocumento();
                $mediodocumento->setMedio($medio);
                $mediodocumento->setCobranza($entity);
                $mediodocumento->setImporte($importemedio);
                $mediodocumento->setNumerolote((int)$numerotransaccion);
                $mediodocumento->setNumerocupon((int)$numerocupon);
                $mediodocumento->setCuotas((int)$cuotas);
                $em->persist($mediodocumento);
                $importemedios += $importemedio;
            }
        }

        $importecheques = 0;
        for($i = 0; $i<6; $i++){
            if($request->get('banco'.$i) != 0 and $request->get('tipocheque'.$i)
                and !empty($request->get('importe'.$i)) and $request->get('importe'.$i)>0){

                $banco = $em->getRepository('JOYASJoyasBundle:Banco')->find($request->get('banco'.$i));
                $tipocheque = $em->getRepository('JOYASJoyasBundle:TipoCheque')->find($request->get('tipocheque'.$i));
                $importecheque = $request->get('importe'.$i);
                $cheque = new Cheque();
                $cheque->setImporte($importecheque);
                $cheque->setFirmantes($request->get('firmantes'.$i));
                $cheque->setCuit($request->get('cuit'.$i));
                $cheque->setFechacobro(new \DateTime($request->get('fechacobro'.$i)));
                $cheque->setFechaemision(new \DateTime($request->get('fechaemision'.$i)));
                $cheque->setNrocheque($request->get('nrocheque'.$i));
                $cheque->setBanco($banco);
                $cheque->setTipocheque($tipocheque);
                $cheque->setCobranza($entity);
                $cheque->setUnidadNegocio($unidad);
                $em->persist($cheque);
                $importecheques+=$importecheque;
            }
        }

        $importeretenciones = 0;

        $tiporetenciones = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findBy(array(),array('descripcion'=>'ASC'));
        for($i = 1; $i<=count($tiporetenciones); $i++){
            if($request->get('retencionimporte'.$i)!='' and $request->get('retencionimporte'.$i)>0){
                $retencion = new Retencion();
                $tiporetencion = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findOneBy(array('descripcion'=>$request->get('retencion'.$i)));
                $improteretencion = $request->get('retencionimporte'.$i);
                $retencion->setImporte($improteretencion);
                $retencion->setTiporetencion($tiporetencion);
                $retencion->setCobranza($entity);
                $retencion->setUnidadNegocio($unidad);
                $em->persist($retencion);
                $importeretenciones+=$improteretencion;
            }
        }
        $importe = $importeretenciones + $importemedios + $importecheques;
        $entity->setImporte($importe);

        if ($form->isValid()) {
            $em->persist($entity);
            // Se asigna Pago.
            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setCobranza($entity);
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setImporte($importe);
            $em->persist($pagoAsignacion);
            $em->flush();

            if(count($facturasAsociadas)>0){
                foreach ($facturasAsociadas as $facturasAsociada){
                    $cobranzafactura = new CobranzaFactura();
                    $cobranzafactura->setCobranza($entity);
                    $cobranzafactura->setImporte($importe);
                    $cobranzafactura->setFactura($facturasAsociada);

                    $em->persist($cobranzafactura);
                }
                $em->flush();
                return $this->redirect($this->generateUrl('cobranza_show', array('id' => $entity->getId())));
            }

            if($factura){
                $cobranzafactura = new CobranzaFactura();
                $cobranzafactura->setCobranza($entity);
                $cobranzafactura->setImporte($importe);
                $cobranzafactura->setFactura($factura);

                $em->persist($cobranzafactura);
                $em->flush();
                return $this->redirect($this->generateUrl('cobranza_show', array('id' => $entity->getId())));
            }

            return $this->redirect($this->generateUrl('cobranza'));
        }

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        $bancos = $em->getRepository('JOYASJoyasBundle:Banco')->findBy(array(),array('descripcion'=>'ASC'));
        $tipocheques = $em->getRepository('JOYASJoyasBundle:TipoCheque')->findBy(array(),array('descripcion'=>'ASC'));

        return $this->render('JOYASJoyasBundle:Cobranza:new.html.twig', array(
                'entity'           => $entity,
                'unidades'         => $unidades,
                'medios'           => $medios,
                'bancos'           => $bancos,
                'tipocheques'      => $tipocheques,
                'factura'          => $factura,
                'tiporetenciones'  => $tiporetenciones,
                'form'             => $form->createView())
        );
    }
    /**
     * Creates a form to create a Cobranza entity.
     *
     * @param Cobranza $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cobranza $entity)
    {
        $form = $this->createForm(new CobranzaType(), $entity, array(
            'action' => $this->generateUrl('cobranza_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr'=> array('class'=>'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Cobranza entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Cobranza();
        $entity->setFecha(new \DateTime("NOW"));

        $idfactura = $request->get('idfactura');
        $idfacturas = $request->get('idfacturas');

        $factura = null;
        if(!empty($idfactura)){
            $factura = $em->getRepository('JOYASJoyasBundle:Factura')->find($idfactura);

            if($factura){
                $entity->setFecha($factura->getFecha());
                $entity->setClienteProveedor($factura->getClienteProveedor());
                $entity->setUnidadnegocio($factura->getUnidadnegocio());
            }
        }

        $totalFacturas = 0;
        $arrFacturas = array();
        if(!empty($idfacturas)){
            foreach($idfacturas as $idfactura){
                $facturas = $em->getRepository('JOYASJoyasBundle:Factura')->find($idfactura);
                if($facturas){
                    $arrFacturas[] = $facturas;
                    $entity->setClienteProveedor($facturas->getClienteProveedor());
                    $totalFacturas+=$facturas->getImporte();
                }
            }
            $entity->setClienteProveedor($facturas->getClienteProveedor());
            $entity->setFecha(new \DateTime("NOW"));
        }

        $form   = $this->createCreateForm($entity);
        if(count($arrFacturas)>0){
            $form
                ->add('facturas', 'entity', array (
                    'class' => 'JOYASJoyasBundle:Factura',
                    'label' => 'Facturas',
                    'mapped'=>false,
                    'data'=> $arrFacturas,
                    'multiple'=>true,
                    'query_builder' => function (\JOYAS\JoyasBundle\Entity\FacturaRepository $repository) use ($idfacturas)
                    {
                        return $repository->createQueryBuilder('c')
                            ->where('c.id IN (?1)')
                            ->setParameter(1, $idfacturas);
                    }
                ));
        }else{
            $form
                ->add('facturas', 'choice', array(
                    'choices'   => array(),
                    'mapped'    => false,
                    'required'  => false)
                );
        }

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        $medios = $em->getRepository('JOYASJoyasBundle:Medio')->findBy(array(),array('descripcion'=>'ASC'));
        $bancos = $em->getRepository('JOYASJoyasBundle:Banco')->findBy(array(),array('descripcion'=>'ASC'));
        $tipocheques = $em->getRepository('JOYASJoyasBundle:TipoCheque')->findBy(array(),array('descripcion'=>'ASC'));
        $tiporetenciones = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findBy(array(),array('descripcion'=>'ASC'));

        return $this->render('JOYASJoyasBundle:Cobranza:new.html.twig', array(
                'entity'           => $entity,
                'unidades'         => $unidades,
                'medios'           => $medios,
                'bancos'           => $bancos,
                'tipocheques'      => $tipocheques,
                'tiporetenciones'  => $tiporetenciones,
                'factura'          => $factura,
                'facturas'         => $arrFacturas,
                'totalFacturas'    => $totalFacturas,
                'form'             => $form->createView())
        );
    }

    /**
     * Finds and displays a Cobranza entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cobranza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cobranza entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Cobranza:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Cobranza entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cobranza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cobranza entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Cobranza:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Cobranza entity.
    *
    * @param Cobranza $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Cobranza $entity)
    {
        $form = $this->createForm(new CobranzaType(), $entity, array(
            'action' => $this->generateUrl('cobranza_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-success')));

        return $form;
    }
    /**
     * Edits an existing Cobranza entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cobranza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cobranza entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cobranza_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:Cobranza:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Cobranza entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Cobranza')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cobranza entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cobranza'));
    }

    /**
     * Creates a form to delete a Cobranza entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cobranza_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
