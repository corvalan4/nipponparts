<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\ImagenProducto;
use JOYAS\JoyasBundle\Form\ImagenProductoType;

/**
 * ImagenProducto controller.
 *
 */
class ImagenProductoController extends Controller
{

    /**
     * Lists all ImagenProducto entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:ImagenProducto')->findAll();

        return $this->render('JOYASJoyasBundle:ImagenProducto:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ImagenProducto entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ImagenProducto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('imagenproducto_show', array('id' => $entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:ImagenProducto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ImagenProducto entity.
     *
     * @param ImagenProducto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ImagenProducto $entity)
    {
        $form = $this->createForm(new ImagenProductoType(), $entity, array(
            'action' => $this->generateUrl('imagenproducto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ImagenProducto entity.
     *
     */
    public function newAction()
    {
        $entity = new ImagenProducto();
        $form   = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:ImagenProducto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ImagenProducto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ImagenProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ImagenProducto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:ImagenProducto:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ImagenProducto entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ImagenProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ImagenProducto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:ImagenProducto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ImagenProducto entity.
    *
    * @param ImagenProducto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ImagenProducto $entity)
    {
        $form = $this->createForm(new ImagenProductoType(), $entity, array(
            'action' => $this->generateUrl('imagenproducto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ImagenProducto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ImagenProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ImagenProducto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('imagenproducto_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:ImagenProducto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ImagenProducto entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:ImagenProducto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ImagenProducto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('imagenproducto'));
    }

    /**
     * Creates a form to delete a ImagenProducto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('imagenproducto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
