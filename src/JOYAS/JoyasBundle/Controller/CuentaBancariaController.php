<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\MovimientoBancario;
use JOYAS\JoyasBundle\Entity\CuentaBancaria;
use JOYAS\JoyasBundle\Form\CuentaBancariaType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * CuentaBancaria controller.
 *
 */
class CuentaBancariaController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

    /**
     * Lists all CuentaBancaria entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->findAll();

        return $this->render('JOYASJoyasBundle:CuentaBancaria:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CuentaBancaria entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CuentaBancaria();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cuentabancaria'));
        }

        return $this->render('JOYASJoyasBundle:CuentaBancaria:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a CuentaBancaria entity.
     *
     * @param CuentaBancaria $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CuentaBancaria $entity)
    {
        $form = $this->createForm(new CuentaBancariaType(), $entity, array(
            'action' => $this->generateUrl('cuentabancaria_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn middle-first')));

        return $form;
    }

    /**
     * Displays a form to create a new CuentaBancaria entity.
     *
     */
    public function newAction()
    {
        $entity = new CuentaBancaria();
        $form   = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:CuentaBancaria:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CuentaBancaria entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CuentaBancaria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:CuentaBancaria:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function ajusteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->find($id);

        $movimiento = new MovimientoBancario();
        $movimiento->setTipoMovimiento('AJ');
        $movimiento->setValor($request->get('valor'));
        $movimiento->setObservacion($request->get('observacion'));


    	if($this->sessionManager->getPerfil()!='ADMINISTRADOR'){
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionManager->getSession('unidad'));
    	}else{
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
    	}
#        $movimiento->setCuentabancaria($entity);
#        $movimiento->setUnidadNegocio($unidad);

        $em->persist($movimiento);

        $entity->setMovimiento($movimiento);
        $em->flush();

        return $this->redirect($this->generateUrl('cuentabancaria_movimientos', array('id'=>$entity->getId())));
    }

    /**
     * Displays a form to edit an existing CuentaBancaria entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CuentaBancaria entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:CuentaBancaria:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CuentaBancaria entity.
    *
    * @param CuentaBancaria $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CuentaBancaria $entity)
    {
        $form = $this->createForm(new CuentaBancariaType(), $entity, array(
            'action' => $this->generateUrl('cuentabancaria_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn middle-first')));

        return $form;
    }
    /**
     * Edits an existing CuentaBancaria entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CuentaBancaria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cuentabancaria'));
        }

        return $this->render('JOYASJoyasBundle:CuentaBancaria:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CuentaBancaria entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CuentaBancaria entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cuentabancaria'));
    }

    /**
     * Creates a form to delete a CuentaBancaria entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cuentabancaria_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function movimientosAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:CuentaBancaria')->find($id);
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findBy(array('estado'=>'A'), array('descripcion'=>'DESC'));
        if($this->sessionManager->getPerfil()=='ADMINISTRADOR'){
            $movimientos = $em->getRepository('JOYASJoyasBundle:MovimientoBancario')->findBy(array('estado'=>'A'), array('id'=>'DESC'));
        }else{
            $movimientos = $em->getRepository('JOYASJoyasBundle:MovimientoBancario')->findBy(array('unidadNegocio'=>$this->sessionManager->getSession('unidad')), array('id'=>'DESC'));
        }

        return $this->render('JOYASJoyasBundle:CuentaBancaria:movimientos.html.twig', array(
            'entity'      => $entity,
            'movimientos' => $movimientos,
            'unidades'    => $unidades
        ));
    }
}
