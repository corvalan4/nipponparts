<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ps\PdfBundle\Annotation\Pdf;
use Doctrine\Common\Collections\ArrayCollection;

use JOYAS\JoyasBundle\Entity\Factura;
use JOYAS\JoyasBundle\Entity\PagoAsignacion;
use JOYAS\JoyasBundle\Entity\ProductoFactura;
use JOYAS\JoyasBundle\Entity\Iva;
use JOYAS\JoyasBundle\Entity\TipoIva;
use JOYAS\JoyasBundle\Entity\Producto;
use JOYAS\JoyasBundle\Form\FacturaType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;

/**
 * Factura controller.
 *
 */
class FacturaController extends Controller
{

	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

	/**
	 * Lists all Factura entities.
	 *
	 */
	public function indexAction($page = 1)
	{
		$this->sessionManager->setSession('fechaDesde', '');
		$this->sessionManager->setSession('fechaHasta', '');
		$this->sessionManager->setSession('listado', '');
		$em = $this->getDoctrine()->getManager();

		if($this->sessionManager->getPerfil()!='ADMINISTRADOR'){
			$entities = $em->getRepository('JOYASJoyasBundle:Factura')->findBy(array('unidadNegocio'=>$this->sessionManager->getUnidad()->getId(), 'tipo'=>'F'), array('fecha'=>'DESC'));
		}else{
			$entities = $em->getRepository('JOYASJoyasBundle:Factura')->findBy(array('estado'=>'A', 'tipo'=>'F'), array('fecha'=>'DESC'));
			$unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findBy(array('estado'=>'A'));
		}
		$clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('clienteProveedor'=>1));

		$adapter = new ArrayAdapter($entities);
		$paginador = new Pagerfanta($adapter);
		$paginador->setMaxPerPage(50);
		$paginador->setCurrentPage($page);

		$unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

		return $this->render('JOYASJoyasBundle:Factura:index.html.twig', array(
			'entities' => $paginador,
			'clientesProveedores' => $clientesProveedores,
			'unidades' => $unidades,
		));
	}

	public function filtroAction(Request $request, $page = 1)
	{
		$em = $this->getDoctrine()->getManager();

		if($request->get('fechaDesde')=='' and $request->get('fechaHasta')=='' and $request->get('listado')==''){
			$desde = new \DateTime($this->sessionManager->getSession('fechaDesde'));
			$hasta = new \DateTime($this->sessionManager->getSession('fechaHasta'));
			$listado = $this->sessionManager->getSession('listado');
		}else{
			if($request->get('fechaDesde')=='' and $request->get('fechaHasta')==''){
				$desde = new \DateTime('1/1/1996');
				$hasta = new \DateTime('NOW');
			}else{
				$desde = new \DateTime($request->get('fechaDesde'));
				$hasta = new \DateTime();
			}
			$listado = $request->get('listado');
			$this->sessionManager->setSession('fechaDesde', $desde->format('Y-m-d'));
			$this->sessionManager->setSession('fechaHasta', $hasta->format('Y-m-d'));
			$this->sessionManager->setSession('getAllActivas', $request->get('listado'));
		}

		$unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

		if($this->sessionManager->getPerfil()!='ADMINISTRADOR'){
			$facturas = $em->getRepository('JOYASJoyasBundle:Factura')->getPorFechas($this->sessionManager->getUnidad(), $desde, $hasta, $listado, 'F');
		}else{
			$facturas = $em->getRepository('JOYASJoyasBundle:Factura')->getPorFechas($request->get('unidadnegocio'), $desde, $hasta, $listado, 'F');
		}

		$clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('clienteProveedor'=>1));

		$adapter = new ArrayAdapter($facturas);
		$paginador = new Pagerfanta($adapter);
		$paginador->setMaxPerPage(50);
		$paginador->setCurrentPage($page);

		return $this->render('JOYASJoyasBundle:Factura:index.html.twig', array(
			'entities' => $paginador,
			'clientesProveedores' => $clientesProveedores,
			'unidades' => $unidades,
		));
	}

	/**
	 * Creates a new Factura entity.
	 *
	 */
	public function createAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$entity = new Factura();
		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		$sucursalstr = $form->get("idsucursal")->getData();
		if(!empty($sucursalstr)){
			$sucursal = $em->getRepository('JOYASJoyasBundle:Sucursal')->find($sucursalstr);
			$entity->setSucursal($sucursal);
			$entity->setClienteProveedor($sucursal->getClienteproveedor());
		}

		$hayProductos = false;
		$total = 0;
		if ($form->isValid()) {
			for($i = 1; $i <=30; $i++ ){
				$idelemento = $request->get("idelemento$i");
				if(!empty($idelemento)){
					$elementostocks = $em->getRepository('JOYASJoyasBundle:Stock')->findBy(array('producto'=>$idelemento), array('fecha'=>'ASC'));
					$cantidad = $request->get("cantidad$i");
					$precio = $request->get("precio$i");

					$total+= $cantidad*$precio;

					foreach($elementostocks as $elementostock){
						if($cantidad > 0 and $cantidad<=$elementostock->getStock()){
							$productofactura = new ProductoFactura();
							$productofactura->setStock($elementostock);
							$productofactura->setFactura($entity);
							$productofactura->setCantidad($cantidad);
							$productofactura->setPrecio($precio);
							$stock = $elementostock->getStock() - $cantidad;
							$elementostock->setStock($stock);
							$em->persist($productofactura);
							$cantidad = 0;
						}else{
							if($cantidad > 0 and $elementostock->getStock()>0){
								$productofactura = new ProductoFactura();
								$productofactura->setStock($elementostock);
								$productofactura->setFactura($entity);
								$productofactura->setPrecio($precio);

								$productofactura->setCantidad($elementostock->getStock());
								$cantidad = $cantidad - $elementostock->getStock();
								$elementostock->setStock(0);
								$em->persist($productofactura);
							}
						}
					}
					$hayProductos = true;
				}
			}

			if(!$hayProductos){
				$this->sessionManager->addFlash('msgError','Debe agregar productos.');
				return $this->render('JOYASJoyasBundle:Factura:new.html.twig', array(
					'entity' => $entity,
					'form'   => $form->createView(),
				));
			}

			$entity->setPtovta($entity->getUnidadNegocio()->getPunto());
			$entity->setImporte($total);
			if($entity->getTipofactura()=='A'){
				$tipoIva = $em->getRepository('JOYASJoyasBundle:TipoIva')->findOneBy(array('porcentaje'=>21));
				$iva = new Iva();
				if($tipoIva){$iva->setTipoIva($tipoIva);}
				$iva->setFactura($entity);
				$montoIva = $total-($total/1.21);
				$iva->setValor(round($montoIva, 2));
				$em->persist($iva);

				$entity->addIva($iva);
			}

			// Se asigna Pago.
			$pagoAsignacion = new PagoAsignacion();
			$pagoAsignacion->setFactura($entity);
			$pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
			$pagoAsignacion->setFecha($entity->getFecha());
			$pagoAsignacion->setImporte($entity->getImporte());
			$em->persist($pagoAsignacion);

			$em->persist($entity);

			$em->flush();

			$this->sessionManager->facturar($entity);
			$cae = $entity->getCae();
			if(!empty($cae)){
				return $this->redirect($this->generateUrl('cobranza_new', array('idfactura'=>$entity->getId())));
			}else{
				return $this->redirect($this->generateUrl('factura'));
			}
		}
	}

	public function refacturarAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);
		$this->sessionManager->facturar($entity);

		return $this->redirect($this->generateUrl('factura'));
	}

	public function crearremitofacturaAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($request->get('id'));
		$tipofactura = $request->get('tipofactura');

		$entity->setTipofactura($tipofactura);
		$entity->setFecha( new \DateTime() );
		$entity->setImporte($request->get('resultadoFinal'));
		$entity->getMovimientocc()->setTipoDocumento('FC');

		$idsucursal = $request->get('sucursal');
		if(isset($idsucursal)){
			$sucursal = $em->getRepository('JOYASJoyasBundle:Sucursal')->find($idsucursal);
			if(!is_null($sucursal)){
				$entity->setSucursal($sucursal);
			}
		}

		foreach($entity->getProductosFactura() as $prodFac){
			$prodFac->setPertenece('F');
		}

		$contador = $request->get('contador');
		for($x = 0; $x <= $contador; $x++){
			$predictiva = $request->get('predictiva'.$x);

			if( !is_null($predictiva) and $predictiva!='' ){
				$subdiv = explode(" ( ", $predictiva);
				if(count($subdiv)>1){
					$codigo = explode(" )", $subdiv[1]);
					$productoFactura = new ProductoFactura();
					$producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo[0], 'unidadNegocio'=>$entity->getListaPrecio()->getUnidadNegocio()));

					if(!is_null($producto)){
						$stock = $producto->getStock() - $request->get('cantidad'.$x);
						if($stock<0){$stock=0;}
						$producto->setStock($stock);
					}

					if(!is_null($producto)){
						$productoFactura->setProducto($producto);
						$productoFactura->setFactura($entity);
						$productoFactura->setDescuento($request->get('bonificacion'.$x));
						$productoFactura->setPrecio($request->get('precio'.$x));
						$productoFactura->setPertenece('F');
						$productoFactura->setCantidad($request->get('cantidad'.$x));
						$entity->addProductosFactura($productoFactura);
					}
				}
			}
		}

		$wsaa = new WSAA('./');

		// Si la fecha de expiracion es menor a hoy, obtengo un nuevo Ticket de Acceso.
		if($wsaa->get_expiration() < date("Y-m-d h:m:i")) {
			if ($wsaa->generar_TA()) {
			} else {
				echo 'msgError','error al obtener el TA.';
			}
		} else {
		};

		$wsfe = new WSFEV1('./');

		// Carga el archivo TA.xml
		$wsfe->openTA();

		// devuelve el cae
		$ptovta = 3;

		if($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
			$tipocbte = 01;
			$tipofactura = 'A';
		}else{
			$tipocbte = 06;
			$tipofactura = 'B';
		}

		/*
		- 01, 02, 03, 04, 05,34,39,60, 63 para los clase A
		- 06, 07, 08, 09, 10, 35, 40,64, 61 para los clase B.
		- 11, 12, 13, 15 para los clase C.
		- 51, 52, 53, 54 para los clase M.
		- 49 para los Bienes Usados
		Consultar m�todo FEParamGetTiposCbte.
		*/

		$nc='';
		// Ultimo comprobante autorizado, a este le sumo uno para procesar el siguiente.
		$cmp = $wsfe->FECompUltimoAutorizado($tipocbte, $ptovta);

		//Armo array con valores hardcodeados de la factura.
		$regfac['concepto'] = 1; 					        # 1: productos, 2: servicios, 3: ambos

		if($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Consumidor Final'){
			$regfac['tipodocumento'] = 99;		                # 80: CUIT, 96: DNI, 99: Consumidor Final
		}else{
			$regfac['tipodocumento'] = 80;		                # 80: CUIT, 96: DNI, 99: Consumidor Final
		}
		$regfac['numerodocumento'] = $entity->getMovimientocc()->getClienteProveedor()->getDni();	    # 0 para Consumidor Final (<$1000)
		$regfac['cuit'] = str_replace("-", "", $entity->getMovimientocc()->getClienteProveedor()->getCuit());
		$regfac['capitalafinanciar'] = 0;			        # subtotal de conceptos no gravados
		if($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
			$regfac['importetotal'] = $entity->getImporte();	# total del comprobante
			$regfac['importeiva'] = ($entity->getImporte()-($entity->getImporte()/1.21));			# subtotal neto sujeto a IVA
			$regfac['importeneto'] = ($entity->getImporte()/1.21);
		}else{
			$regfac['importetotal'] = $entity->getImporte();	# total del comprobante
			$regfac['importeiva'] = 0;
			$regfac['importeneto'] = 0;			# subtotal neto sujeto a IVA
			$regfac['capitalafinanciar'] = $entity->getImporte();
		}

		$regfac['imp_trib'] = 1.0;
		$regfac['imp_op_ex'] = 0.0;
		$regfac['nrofactura'] = $cmp->FECompUltimoAutorizadoResult->CbteNro + 1;
		$regfac['fecha_venc_pago'] = date('Ymd');

		// Armo con la factura los parametros de entrada para el pedido
		$params = $wsfe->armadoFacturaUnica(
			$tipofactura,
			$ptovta,    // el punto de venta
			$nc,
			$regfac     // los datos a facturar
		);

		//Solicito el CAE
		$cae = $wsfe->solicitarCAE($params);
		$this->sessionManager->setSession('CAE',json_encode($cae) );

		if($cae->FECAESolicitarResult->FeCabResp->Resultado=='A' or $cae->FECAESolicitarResult->FeCabResp->Resultado=='P'){
			$em->flush();
			$entity->setNrofactura($regfac['nrofactura']);
			$entity->setCae($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAE);
			$entity->setFechavtocae(new \DateTime($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAEFchVto));
			$em->flush();

			$cae = $entity->getCae();
			if(!empty($cae)){
				return $this->redirect($this->generateUrl('cobranza_new', array('idfactura'=>$entity->getId())));
			}else{
				return $this->redirect($this->generateUrl('factura_show', array('id'=>$entity->getId(), 'tipo'=>'F')));
			}
		}else{
			if($cae->FECAESolicitarResult->FeCabResp->Resultado=='R'){
				$this->sessionManager->addFlash('msgError','Error al dar de alta factura: '. json_encode($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones) );
			}
			return $this->redirect($this->generateUrl('factura'));
		}

		$em->flush();

		$this->sessionManager->addFlash('msgOk','Venta registrada.');

		$cae = $entity->getCae();
		if(!empty($cae)){
			return $this->redirect($this->generateUrl('cobranza_new', array('idfactura'=>$entity->getId())));
		}else{
			return $this->redirect($this->generateUrl('factura_show', array('id'=>$entity->getId(), 'tipo'=>'F')));
		}
	}

	/**
	 * Creates a form to create a Factura entity.
	 *
	 * @param Factura $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(Factura $entity)
	{
		$form = $this->createForm(new FacturaType(), $entity, array(
			'action' => $this->generateUrl('factura_create'),
			'method' => 'POST',
		));

		$form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn btn-success', 'style'=>'float:right;')));

		return $form;
	}

	/**
	 * Displays a form to create a new Factura entity.
	 *
	 */
	public function newAction(Request $request) {
		if (!$this->sessionManager->isLogged()) {
			return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
		}

		$em = $this->getDoctrine()->getManager();
		$entity = new Factura();
		if($this->sessionManager->getSession('usuario')->getPerfil() != 'ADMINISTRADOR'){
			$unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionManager->getUnidad()->getId());
			$entity->setUnidadNegocio($unidad);
		}
		$entity->setTipofactura('B');
		$form   = $this->createCreateForm($entity);

		return $this->render('JOYASJoyasBundle:Factura:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
		));
	}

	public function remitofacturaAction($id)
	{
		if(!$this->sessionManager->isLogged()){
			return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
		}

		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

		if($entity->getMovimientocc()->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
			return $this->render('JOYASJoyasBundle:Factura:remitofacturaA.html.twig', array(
				'entity' => $entity,
			));
		}else{
			return $this->render('JOYASJoyasBundle:Factura:remitofacturaB.html.twig', array(
				'entity' => $entity,
			));
		}
	}

	public function facturaremitoAction($id)
	{
		if(!$this->sessionManager->isLogged()){
			return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
		}

		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

		return $this->render('JOYASJoyasBundle:Factura:facturaremito.html.twig', array(
			'entity' => $entity,
		));
	}


	public function crearfacturaremitoAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($request->get('id'));
		$entity->setNroremito($request->get('nroremito'));
		$contador = count($entity->getProductosFactura());
		for($x = 1; $x <= $contador; $x++){
			$id = $request->get('id'.$x);
			$this->sessionManager->setSession('id'.$id,$id);
			if(!is_null($id) and $id!=''){
				$productoFactura = $em->getRepository('JOYASJoyasBundle:ProductoFactura')->find($id);
				$productoFactura->setPertenece('RF');
			}
		}
		$em->flush();

		$this->sessionManager->addFlash('msgOk','Remito registrado.');

		return $this->redirect($this->generateUrl('factura_show', array('id'=>$entity->getId(), 'tipo'=>'R')));
	}

	public function showAction(Request $request, $id) {
		if (!$this->sessionManager->isLogged()) {
			return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
		}
		$tipodoc = $request->get('tipo');
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);
		if ($tipodoc == 'F' or $entity->getTipo()=='F') {
			if ($entity->getTipofactura() == 'A') {
				$nrocomprobante = '01';
				$codigobarras = $this->sessionManager->obtenerTextoCodigoBarras(
					str_replace("-", "", $entity->getClienteProveedor()->getCuit()),
					$nrocomprobante,
					$entity->getPtovta(),
					$entity->getCae(),
					$entity->getFechavtocae()->format('Ymd')
				);
				return $this->render('JOYASJoyasBundle:Factura:showA.html.twig', array(
					'entity' => $entity,
					'nrocomprobante' => $nrocomprobante,
					'codigobarras' => $codigobarras
				));
			}
			if ($entity->getTipofactura() == 'B' or $entity->getTipofactura() == 'C') {
				$nrocomprobante = ($entity->getTipofactura()=='C') ? '11'  : '06';
				$codigobarras = $this->sessionManager->obtenerTextoCodigoBarras(
					str_replace("-", "", $entity->getClienteProveedor()->getCuit()),
					$nrocomprobante,
					$entity->getPtovta(),
					$entity->getCae(),
					$entity->getFechavtocae()->format('Ymd')
				);
				return $this->render('JOYASJoyasBundle:Factura:showB.html.twig', array(
					'entity' => $entity,
					'tipofactura' => $entity->getTipofactura(),
					'nrocomprobante' => $nrocomprobante,
					'codigobarras' => $codigobarras
				));
			}
		} else {
			if ($entity->getTipofactura() == 'A') {
				return $this->render('JOYASJoyasBundle:Factura:creditoA.html.twig', array(
					'entity' => $entity));
			}
			if ($entity->getTipofactura() == 'B' or $entity->getTipofactura() == 'C') {
				$nrocomprobante = ($entity->getTipofactura()=='C') ? '13'  : '08';
				$codigobarras = $this->sessionManager->obtenerTextoCodigoBarras(
					str_replace("-", "", $entity->getClienteProveedor()->getCuit()),
					$nrocomprobante,
					$entity->getPtovta(),
					$entity->getCae(),
					$entity->getFechavtocae()->format('Ymd')
				);
				return $this->render('JOYASJoyasBundle:Factura:creditoB.html.twig', array(
					'entity' => $entity,
					'tipofactura' => $entity->getTipofactura(),
					'nrocomprobante' => $nrocomprobante,
					'codigobarras' => $codigobarras
				));
			}
		}
	}

	public function imprimirAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Factura entity.');
		}


		return $this->render('JOYASJoyasBundle:Factura:imprimir.html.twig', array(
			'entity' => $entity
		));
	}

	/**
	 * Displays a form to edit an existing Factura entity.
	 *
	 */
	public function editAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Factura entity.');
		}

		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		return $this->render('JOYASJoyasBundle:Factura:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		));
	}

	/**
	 * Creates a form to edit a Factura entity.
	 *
	 * @param Factura $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(Factura $entity)
	{
		$form = $this->createForm(new FacturaType(), $entity, array(
			'action' => $this->generateUrl('factura_update', array('id' => $entity->getId())),
			'method' => 'PUT',
		));

		$form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn middle-first')));

		return $form;
	}
	/**
	 * Edits an existing Factura entity.
	 *
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Factura entity.');
		}

		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {
			$em->flush();

			return $this->redirect($this->generateUrl('factura_edit', array('id' => $id)));
		}

		return $this->render('JOYASJoyasBundle:Factura:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		));
	}
	/**
	 * Deletes a Factura entity.
	 *
	 */
	public function deleteAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);

		foreach($entity->getProductosFactura() as $productofactura){
			$stock = $productofactura->getStock()->getStock() + $productofactura->getCantidad();
			$productofactura->getStock()->setStock($stock);
		}

		$entity->setEstado("E");
		$em->flush();

		$this->sessionManager->addFlash("msgOk", "Entrega eliminada con éxito.");

		return $this->redirect($this->generateUrl('factura'));
	}

	public function borraremitoAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('JOYASJoyasBundle:Factura')->find($id);
		$entity->setEstado('B');
		$entity->getMovimientocc()->setEstado('B');

		$em->flush();

		return $this->redirect($this->generateUrl('factura'));
	}

	/**
	 * Creates a form to delete a Factura entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('factura_delete', array('id' => $id)))
			->setMethod('DELETE')
			->add('submit', 'submit', array('label' => 'Delete'))
			->getForm()
			;
	}

	public function verclienteAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$search = $request->get('data');
		$string = '';
		$cliente = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findOneBy(array('cuit' => $search));
		if (empty($cliente)) {
			$cliente = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findOneBy(array('dni' => $search));
		}
		if (empty($cliente)) {
			$response = '0///0';
		} else {
			$response = $cliente->getId() . '///' . $cliente->getRazonSocial();
		}
		return new Response($response);
	}

	public function altaclienteAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$condicion = $em->getRepository('JOYASJoyasBundle:CondicionIva')->find(1);
		$tipo = $request->get('data');
		$nrodoc = $request->get('data1');
		$direc = $request->get('data2');
		$razon = $request->get('data3');
		$telefono = $request->get('data4');
		$email = $request->get('data5');

		$respuesta = '0';

		$cliente = new ClienteProveedor();
		$cliente->setRazonSocial($razon);
		if ($tipo == 'CUIT') {
			$cliente->setCuit(str_replace('-', '', $nrodoc));
		} else {
			$cliente->setDni($nrodoc);
		}
		$cliente->setCondicioniva($condicion);
		$cliente->setDomiciliocomercial($direc);
		$cliente->setTelefono($telefono);
		$cliente->setMail($email);

		$em->persist($cliente);
		$em->flush();

		$respuesta = $nrodoc;

		return new Response($respuesta);
	}

}
?>
