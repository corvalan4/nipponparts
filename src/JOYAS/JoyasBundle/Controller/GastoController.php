<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\PagoAsignacion;
use JOYAS\JoyasBundle\Entity\Gasto;
use JOYAS\JoyasBundle\Entity\MedioDocumento;
use JOYAS\JoyasBundle\Form\GastoType;

use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Gasto controller.
 *
 */
class GastoController extends Controller
{

	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

    /**
     * Lists all Gasto entities.
     *
     */
    public function indexAction(Request $request)
    {
		if(!$this->sessionManager->isLogged()){
			return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
		}

        if(!isset($page)){
            $page = 1;
        }

		if(empty($request->get('desde'))){
			$desde = new \DateTime('NOW -30 days');
		}else{
			$desde = new \DateTime($request->get('desde'));
		}

		if(empty($request->get('hasta'))){
			$hasta = new \DateTime('NOW +1 days');
		}else{
			$hasta = new \DateTime($request->get('hasta'));
		}



		$em = $this->getDoctrine()->getManager();

		if($this->sessionManager->getPerfil()!='ADMINISTRADOR'){
			$entities = $em->getRepository('JOYASJoyasBundle:Gasto')->filtro($desde, $hasta, $this->sessionManager->getUnidad()->getId());
		}else{
			$entities = $em->getRepository('JOYASJoyasBundle:Gasto')->filtro($desde, $hasta, $request->get('unidad'));
		}

		$unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

		$tiposGasto = $em->getRepository('JOYASJoyasBundle:TipoGasto')->getAllActivas();

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
		$paginador->setMaxPerPage(300);
		$paginador->setCurrentPage($page);

		return $this->render('JOYASJoyasBundle:Gasto:index.html.twig', array(
			'entities' => $paginador,
			'unidades' => $unidades,
			'tiposGasto' => $tiposGasto,
		));
    }
    /**
     * Creates a new Gasto entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Gasto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

		$em = $this->getDoctrine()->getManager();

		$proveedor = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($request->get('clienteProveedor'));
        $medios = $em->getRepository('JOYASJoyasBundle:Medio')->findBy(array(),array('descripcion'=>'ASC'));
		$entity->setClienteProveedor($proveedor);

		if($this->sessionManager->getPerfil()=='ADMINISTRADOR'){
			$unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidad'));
			$entity->setUnidadnegocio($unidad);
		}else{
			$entity->setUnidadnegocio($this->sessionManager->getUnidad());
		}

        $importemedios = 0;
        foreach ($medios as $medio){
            $importemedio = $request->get('importemedio' . $medio->getId());
            $numerocupon = $request->get('numerocupon' . $medio->getId());
            $numerotransaccion = $request->get('numerotransaccion' . $medio->getId());
            $cuotas = $request->get('cuotas' . $medio->getId());
            if (!empty($importemedio)) {
                $mediodocumento = new MedioDocumento();
                $mediodocumento->setMedio($medio);
                $mediodocumento->setGasto($entity);
                $mediodocumento->setImporte($importemedio);
                $mediodocumento->setNumerolote((int)$numerotransaccion);
                $mediodocumento->setNumerocupon((int)$numerocupon);
                $mediodocumento->setCuotas((int)$cuotas);
                $em->persist($mediodocumento);
                $importemedios += $importemedio;
            }
        }

		$importeRetenciones = 0;

		$tiporetenciones = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
		for($i = 1; $i<=count($tiporetenciones); $i++){
			if($request->get('retencionimporte'.$i)!='' and $request->get('retencionimporte'.$i)>0){
				$retencion = new Retencion();
				$tiporetencion = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findOneBy(array('descripcion'=>$request->get('retencion'.$i)));
				$retencion->setImporte($request->get('retencionimporte'.$i));
				$retencion->setTiporetencion($tiporetencion);
				$retencion->setGasto($entity);
				$em->persist($retencion);
				$importeRetenciones+=$request->get('retencionimporte'.$i);
			}
		}
		$importe = $importeRetenciones + $importemedios;
		$entity->setImporte($importe);

    	if ($form->isValid()) {
			$em->persist($entity);

			// Se asigna Pago.
			$pagoAsignacion = new PagoAsignacion();
			$pagoAsignacion->setGasto($entity);
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setImporte($importe);
			$em->persist($pagoAsignacion);

            $em->flush();

//            return $this->redirect($this->generateUrl('gasto'));
             return $this->redirect($this->generateUrl('gasto_show', array('id' => $entity->getId())));
        }

		$cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('clienteProveedor'=>2));
		$unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        $medios = $em->getRepository('JOYASJoyasBundle:Medio')->findBy(array(),array('descripcion'=>'ASC'));
		$tiporetenciones = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
		$cheques = $em->getRepository('JOYASJoyasBundle:Cheque')->findBy(array('estado'=>'A'),array('fechacobro'=>'ASC'));

		return $this->render('JOYASJoyasBundle:Gasto:new.html.twig', array(
			'entity'           => $entity,
			'unidades'         => $unidades,
			'medios'           => $medios,
			'tiporetenciones'  => $tiporetenciones,
			'cheques'          => $cheques,
			'form'             => $form->createView(),
			'clientesProveedores' => $cliProvs)
		);
    }

    /**
     * Creates a form to create a Gasto entity.
     *
     * @param Gasto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Gasto $entity)
    {
        $form = $this->createForm(new GastoType(), $entity, array(
            'action' => $this->generateUrl('gasto_create'),
            'method' => 'POST',
        ));

		$form->add('submit', 'submit', array('label' => 'Crear', 'attr'=> array('class'=>'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Gasto entity.
     *
     */
    public function newAction()
    {
		$em = $this->getDoctrine()->getManager();
		$entity = new Gasto();
        $entity->setFecha(new \DateTime("NOW"));
        $form   = $this->createCreateForm($entity);

		$cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('clienteProveedor'=>2));
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        $medios = $em->getRepository('JOYASJoyasBundle:Medio')->findBy(array(),array('descripcion'=>'ASC'));
		$tiporetenciones = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
		$cheques = $em->getRepository('JOYASJoyasBundle:Cheque')->findBy(array('estado'=>'A'),array('fechacobro'=>'ASC'));

		return $this->render('JOYASJoyasBundle:Gasto:new.html.twig', array(
			'entity'           => $entity,
			'unidades'         => $unidades,
			'medios'           => $medios,
			'tiporetenciones'  => $tiporetenciones,
			'cheques'          => $cheques,
			'form'             => $form->createView(),
			'clientesProveedores' => $cliProvs)
		);
    }

    /**
     * Finds and displays a Gasto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Gasto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gasto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Gasto:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Gasto entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Gasto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gasto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Gasto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Gasto entity.
    *
    * @param Gasto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Gasto $entity)
    {
        $form = $this->createForm(new GastoType(), $entity, array(
            'action' => $this->generateUrl('gasto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

		$form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-success')));

        return $form;
    }
    /**
     * Edits an existing Gasto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Gasto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gasto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('gasto_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:Gasto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Gasto entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Gasto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Gasto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('gasto'));
    }

    /**
     * Creates a form to delete a Gasto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gasto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
