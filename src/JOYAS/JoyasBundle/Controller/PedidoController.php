<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\Pedido;
use JOYAS\JoyasBundle\Entity\Stock;
use JOYAS\JoyasBundle\Entity\StockPedido;
use JOYAS\JoyasBundle\Entity\StockEntregado;
use JOYAS\JoyasBundle\Form\PedidoType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Pedido controller.
 *
 */
class PedidoController extends Controller
{
    /**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

    /**
     * Lists all Pedido entities.
     *
     */
	public function indexAction(Request $request)
    {
         $em = $this->getDoctrine()->getManager();

 		if(empty($request->get('desde'))){
 			$desde = new \DateTime('NOW -30 days');
 		}else{
 			$desde = new \DateTime($request->get('desde'));
 		}

 		if(empty($request->get('hasta'))){
 			$hasta = new \DateTime('NOW +23 hours');
 		}else{
 			$hasta = new \DateTime($request->get('hasta')." +23 hours");
 		}

		$entities = $em->getRepository('JOYASJoyasBundle:Pedido')->filtro($desde, $hasta, $this->sessionManager->getSession('usuario'));

		return $this->render('JOYASJoyasBundle:Pedido:index.html.twig', array(
		 'entities' => $entities,
		 'desde' => $desde->format('Y-m-d'),
		 'hasta' => $hasta->format('Y-m-d'),
		));
    }

    /**
     * Creates a new Pedido entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Pedido();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $fechaStr = $request->get('fecha');
        $entity->setFecha(new \DateTime($fechaStr));
        if($entity->getUnidadOrigen() == $entity->getUnidadDestino()){
            $this->sessionManager->addFlash('msgError','Las Unidades deben ser distintas.');
            return $this->render('JOYASJoyasBundle:Pedido:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
        }
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            for($i = 1; $i <=30; $i++ ){
				$idelemento = $request->get("idelemento$i");
				if(!empty($idelemento)){
					$stock = $em->getRepository('JOYASJoyasBundle:Stock')->find($idelemento);
					$cantidad = $request->get("cantidad$i");
                    $stockPedido = new StockPedido();
                    $stockPedido->setCantidad($cantidad);
                    $stockPedido->setPedido($entity);
                    $stockPedido->setStock($stock);
                    $em->persist($stockPedido);
                    $em->flush();
				}
			}

            return $this->redirect($this->generateUrl('pedido_show', array('id' => $entity->getId())));
        }
        return $this->render('JOYASJoyasBundle:Pedido:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Pedido entity.
     *
     * @param Pedido $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pedido $entity)
    {
        $form = $this->createForm(new PedidoType(), $entity, array(
            'action' => $this->generateUrl('pedido_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr'=>array('class'=>'btn btn-success', 'style'=>'float:right')));

        return $form;
    }

    /**
     * Displays a form to create a new Pedido entity.
     *
     */
    public function newAction()
    {
        $entity = new Pedido();

        $usuarioSession = $this->sessionManager->getSession('usuario');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('JOYASJoyasBundle:Usuario')->find($usuarioSession->getId());
		$entity->setUsuario($usuario);

        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:Pedido:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Pedido entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pedido entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Pedido:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pedido entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pedido entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Pedido:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Pedido entity.
    *
    * @param Pedido $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Pedido $entity)
    {
        $form = $this->createForm(new PedidoType(), $entity, array(
            'action' => $this->generateUrl('pedido_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Aprobar', 'attr'=>array('class'=>'btn btn-success', 'style'=>'float:right')));

        return $form;
    }
    /**
     * Edits an existing Pedido entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pedido entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
		$totalEntregado = true;
        if ($editForm->isValid()) {
			foreach($entity->getStocksPedidos() as $stockpedido){
				// Cantidad a Entregar
				$cantidad = $request->get("entrega".$stockpedido->getId());
				$cantidadParaEntregar = $cantidad - $stockpedido->getCantidadEntregada();

				if($cantidadParaEntregar>0){
					// Cantidad Entregada
					$elementostocks = $em->getRepository('JOYASJoyasBundle:Stock')->findBy(array('producto'=>$stockpedido->getStock()->getProducto()->getId(), 'unidadnegocio'=>$stockpedido->getStock()->getUnidadnegocio()->getId()), array('fecha'=>'ASC'));
					if(count($elementostocks)==0){
						$this->sessionManager->addFlash("msgError", "El producto ".$stockpedido->getStock()->getProducto()->getDescripcion()." no tiene stock para entregar.");
					}
					foreach($elementostocks as $elementostock){
						if($cantidadParaEntregar > 0 and $cantidadParaEntregar<=$elementostock->getStock()){
							$stocksEntregado = new StockEntregado();
							$stocksEntregado->setPedido($entity);
							$stocksEntregado->setStockpedido($stockpedido);
							$stocksEntregado->setCantidad($cantidadParaEntregar);
							$stock = $elementostock->getStock() - $cantidadParaEntregar;
							$elementostock->setStock($stock);

							// Busco registro de stock para la unidad que solicita.
							$stockTraspasado = $em->getRepository('JOYASJoyasBundle:Stock')->findOneBy(array('producto'=>$stockpedido->getStock()->getProducto()->getId(), 'unidadnegocio'=>$entity->getUnidadDestino()->getId(), 'costo'=>$elementostock->getCosto()));
							if(!$stockTraspasado){
								// Genero nuevo registro de stock para la unidad que solicita.
								$stockTraspasado = new Stock();
								$stockTraspasado->setProducto($stockpedido->getStock()->getProducto());
								$stockTraspasado->setUnidadnegocio($entity->getUnidadDestino());
								$stockTraspasado->setStock($cantidadParaEntregar);
								$stockTraspasado->setCosto($elementostock->getCosto());
								$em->persist($stockTraspasado);
							}else{
								$nuevoStockTraspaso = $stockTraspasado->getStock() + $cantidadParaEntregar;
								$stockTraspasado->setStock($nuevoStockTraspaso);
							}

							$cantidadParaEntregar = 0;
							$em->persist($stocksEntregado);
						}else{
							if($cantidadParaEntregar > 0 and $elementostock->getStock()>0){
								$stocksEntregado = new StockEntregado();
								$stocksEntregado->setPedido($entity);
								$stocksEntregado->setStockpedido($stockpedido);

								$stocksEntregado->setCantidad($elementostock->getStock());
								$cantidadParaEntregar = $cantidadParaEntregar - $elementostock->getStock();

								// Busco registro de stock para la unidad que solicita.
								$stockTraspasado = $em->getRepository('JOYASJoyasBundle:Stock')->findOneBy(array('producto'=>$stockpedido->getStock()->getProducto()->getId(), 'unidadnegocio'=>$entity->getUnidadDestino()->getId(), 'costo'=>$elementostock->getCosto()));
								if(!$stockTraspasado){
									// Genero nuevo registro de stock para la unidad que solicita.
									$stockTraspasado = new Stock();
									$stockTraspasado->setProducto($stockpedido->getStock()->getProducto());
									$stockTraspasado->setUnidadnegocio($entity->getUnidadDestino());
									$stockTraspasado->setStock($cantidadParaEntregar);
									$stockTraspasado->setCosto($elementostock->getCosto());
									$em->persist($stockTraspasado);
								}else{
									$nuevoStockTraspaso = $stockTraspasado->getStock() + $cantidadParaEntregar;
									$stockTraspasado->setStock($nuevoStockTraspaso);
								}

								$elementostock->setStock(0);
								$em->persist($stocksEntregado);
							}
						}

					}
				}
				if($request->get("entrega".$stockpedido->getId()) < $stockpedido->getCantidad()){
					$totalEntregado = false;
				}
			}
			if($totalEntregado){
				$entity->setEstado('E');
			}else{
				$entity->setEstado('P');
			}
			$em->flush();

            return $this->redirect($this->generateUrl('pedido_show', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:Pedido:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a Pedido entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Pedido entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pedido'));
    }

	public function cancelarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);
		$entity->setEstado('C');
        $em->flush();

        return $this->redirect($this->generateUrl('pedido'));
    }


	public function terminarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);
		$entity->setEstado('E');
        $em->flush();

        return $this->redirect($this->generateUrl('pedido'));
    }

    /**
     * Creates a form to delete a Pedido entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pedido_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
