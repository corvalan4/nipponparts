<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\Stock;
use JOYAS\JoyasBundle\Form\StockType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Stock controller.
 *
 */
class StockController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

    /**
     * Lists all Stock entities.
     *
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Stock')->findBy(array('producto'=>$id));
        $producto = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);

        return $this->render('JOYASJoyasBundle:Stock:index.html.twig', array(
            'entities' => $entities,
            'producto' => $producto,
        ));
    }
    /**
     * Creates a new Stock entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Stock();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
		$em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('stock', array('id' => $entity->getProducto()->getId())));
        }

        return $this->render('JOYASJoyasBundle:Stock:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Stock entity.
     *
     * @param Stock $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Stock $entity)
    {
        $form = $this->createForm(new StockType(), $entity, array(
            'action' => $this->generateUrl('stock_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Stock entity.
     *
     */
    public function newAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $producto = $em->getRepository('JOYASJoyasBundle:Producto')->find($id);
        $entity = new Stock();
        $entity->setProducto($producto);
		$form   = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:Stock:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Stock entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Stock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stock entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Stock:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Stock entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Stock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stock entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Stock:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Stock entity.
    *
    * @param Stock $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Stock $entity)
    {
        $form = $this->createForm(new StockType(), $entity, array(
            'action' => $this->generateUrl('stock_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }
    /**
     * Edits an existing Stock entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Stock')->find($id);

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('stock', array('id' => $entity->getProducto()->getId())));
        }

        return $this->render('JOYASJoyasBundle:Stock:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

	public function predictivaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $string = '<ul>';
		if(strlen($request->get('valor'))>2){
		    if(!empty($request->get('idunidad'))){
                $entities = $em->getRepository('JOYASJoyasBundle:Stock')->predictiva($request->get('valor'), $request->get('idunidad'));
                foreach ($entities as $entity) {
                    $string = $string . '<li class=suggest-element id=' . $entity->getProducto()->getId() . ' stock=' . $entity->getProducto()->getStock() . ' precio=' . $entity->getProducto()->getPrecio() . '><a href="#" id=predictivaelemento' . $entity->getProducto()->getId() . ' data=' . $entity->getProducto()->getId(). ' >' . $entity->getProducto()->getCodigo() . ' - '. $entity->getProducto()->getDescripcion();
                }
            }else{
                $entities = $em->getRepository('JOYASJoyasBundle:Producto')->findByFilter(array("codigoLike"=>$request->get('valor'), "descripcion"=>$request->get('valor')));
                foreach ($entities as $entity) {
                    $string = $string . '<li class=suggest-element id=' . $entity->getId() . ' precio=' . $entity->getPrecio() . '><a href="#" id=predictivaelemento' . $entity->getId() . ' data=' . $entity->getId(). ' >' . $entity->getCodigo() . ' - '. $entity->getDescripcion();
                }
            }
		}
        $string = $string.'</ul>';
		return new Response($string);
	}

    /**
     * Deletes a Stock entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Stock')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Stock entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('stock'));
    }

    /**
     * Creates a form to delete a Stock entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('stock_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
