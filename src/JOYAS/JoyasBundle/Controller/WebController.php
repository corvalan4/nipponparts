<?php

namespace JOYAS\JoyasBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use JOYAS\JoyasBundle\Entity\ListaPrecio;
use JOYAS\JoyasBundle\Entity\MP;
use JOYAS\JoyasBundle\Entity\Precio;
use JOYAS\JoyasBundle\Entity\Producto;
use JOYAS\JoyasBundle\Entity\DetallePedido;
use JOYAS\JoyasBundle\Entity\Pedido;
use JOYAS\JoyasBundle\Entity\UsuarioWeb;

class WebController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;	
	
    public function indexAction($page)
    {
		if(session_status()=='PHP_SESSION_DISABLED'){
			$this->sessionManager->startSession();
		}
 		if(!isset($page))
		{
			$page = 1;
		}
   
	    $em = $this->getDoctrine()->getManager();

		$lista = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findOneBy(array('descripcion' => 'Lista Web'));
		$query = $em->getRepository('JOYASJoyasBundle:Producto')->precios($lista->getId());

		$novedades = $em->getRepository('JOYASJoyasBundle:Producto')->novedades();
		$cant = 32;
		
		$novedadesDos = NULL;
		
 		if($page == 1){
			$cant = 32 - count($novedades);
	 		if($cant <= 0){
				$cant  = 1;
			}
			$novedadesDos = $novedades;
		}

		$paginador = new Pagerfanta(new DoctrineORMAdapter($query));
		$paginador->setMaxPerPage($cant);
		$paginador->setCurrentPage($page);

		$this->sessionManager->setSession('paginas',$paginador->getNbPages());
		
        return $this->render('JOYASJoyasBundle:Web:index.html.twig', array(
            'precios' => $paginador,
            'novedades' => $novedades,
            'novedadesDos' => $novedadesDos,
        ));
	}
    public function micuentaAction(Request $request)
    {

	    $em = $this->getDoctrine()->getManager();

		$usuarioweb = $em->getRepository('JOYASJoyasBundle:UsuarioWeb')->find($request->get('idusuarioweb'));
		
        return $this->render('JOYASJoyasBundle:Web:micuenta.html.twig', array(
            'usuarioweb' => $usuarioweb,
        ));
	}
	
    public function novedadesAction()
    {
        $em = $this->getDoctrine()->getManager();

		$novedades = $em->getRepository('JOYASJoyasBundle:Producto')->novedades();
		
        return $this->render('JOYASJoyasBundle:Web:novedades.html.twig', array(
            'novedades' => $novedades,
        ));
	}

    public function ofertasAction()
    {
        $em = $this->getDoctrine()->getManager();

		$ofertas = $em->getRepository('JOYASJoyasBundle:Producto')->ofertas();
		
        return $this->render('JOYASJoyasBundle:Web:ofertas.html.twig', array(
            'ofertas' => $ofertas,
        ));
	}
	
    public function checkoutAction()
    {
        $em = $this->getDoctrine()->getManager();
		$precios = new ArrayCollection();
		$lista = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->findOneBy(array('descripcion' => 'Lista Web'));
		$carrito = $this->sessionManager->getSession('carrito');
		$ids = explode(',',$carrito);
		
		for($i=0; $i<count($ids); $i++){
			if( $ids[$i] != '' )
			{
				$precio = $em->getRepository('JOYASJoyasBundle:Precio')->findOneBy(array('producto' => $ids[$i], 'listaPrecio'=>$lista->getId()));
				if(!is_null($precio)){
					$precios->add($precio);
				}
			}	
		}
		
        return $this->render('JOYASJoyasBundle:Web:checkout.html.twig', array(
            'precios' => $precios,
        ));
	}
	
    public function contactoAction()
    {
        return $this->render('JOYASJoyasBundle:Web:contact.html.twig');
	}

    public function mailAction(Request $request)
    {
		$message = \Swift_Message::newInstance()
		->setSubject('Contacto Web')
		->setFrom($request->get('email'))
		->setTo('joyas@sistemastitanio.com')
		->setBody(
					$this->renderView('JOYASJoyasBundle:Web:email.txt.twig', array('nombre' => $request->get('nombre'),'telefono' => $request->get('telefono'),'mensaje' => $request->get('mensaje'),'email' => $request->get('email')))										
				);
		$this->get('mailer')->send($message);

		$this->sessionManager->addFlash('msgOk', 'Mensaje enviado correctamente, gracias por su consulta.');		
		return $this->redirect($this->generateUrl('joyas_joyas_web'));
	}
    public function irregistroAction(Request $request)
    {
        return $this->render('JOYASJoyasBundle:Web:register.html.twig');
	}
    public function registroAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
		$usuario = $em->getRepository('JOYASJoyasBundle:UsuarioWeb')->findOneBy(array('mail' => $request->get('email')));
		if(!is_null($usuario)){
			$this->sessionManager->addFlash('msgWarn', 'Ya existe un usuario registrado con ese mail.');
			return $this->redirect($this->generateUrl('joyas_joyas_irregistro'));
		}else{
			$usuario = new UsuarioWeb();
			$usuario->setNombre($request->get('nombre'));
			$usuario->setMail($request->get('email'));
			$usuario->setTelefono($request->get('telefono'));
			$usuario->setClave($request->get('clave'));
			
			$em->persist($usuario);
			$em->flush();
			
			$message = \Swift_Message::newInstance()
			->setSubject('Registro Web')
			->setFrom('info@sistemastitanio.com.ar')
			->setTo('joyas@dediosjoyas.com')
			->setBody(
						$this->renderView('JOYASJoyasBundle:Web:registro.txt.twig', array('nombre' => $request->get('nombre'),'telefono' => $request->get('telefono'),'login' => $request->get('login'),'email' => $request->get('email')))										
					);
			$this->get('mailer')->send($message);
			
			$this->sessionManager->addFlash('msgOk', 'Gracias por registrarte! Nos pondremos en contacto contigo para aprobar tu registro.');
		}
		
		return $this->redirect($this->generateUrl('joyas_joyas_web'));
	}

    public function cerrarAction()
    {
		$this->sessionManager->closeSession();
		return $this->redirect($this->generateUrl('joyas_joyas_web'));
	}

    public function realizarpedidoAction(Request $request)
    {	
	    $em = $this->getDoctrine()->getManager();

		if($this->sessionManager->getSession('idusuarioweb')==''){
			$this->sessionManager->addFlash('msgWarn', 'Debe loguearse para seleccionar metodo de pago.');
			return $this->redirect($this->generateUrl('joyas_joyas_web'));
		}		

		$usuarioweb = $em->getRepository('JOYASJoyasBundle:UsuarioWeb')->find($this->sessionManager->getSession('idusuarioweb'));
		$pedido = new Pedido();
		$pedido->setObservacion($request->get('comentario'));
		$pedido->setTotal($request->get('resultadofin'));
		$pedido->setFecha(new \DateTime('NOW'));
		$pedido->setUsuarioweb($usuarioweb);
		$em->persist($pedido);
		$em->flush();

		$carrito = $this->sessionManager->getSession('carrito');
		$ids = explode(',',$carrito);
		if(count($ids)>0){
			for($i=0; $i<count($ids); $i++){
				$cant = 'cantidad'.$ids[$i];
				$precio = 'valor'.$ids[$i];
				$cantidad = $request->get($cant);
				$preciodesc = 'valordescuento'.$ids[$i];
				$preciodescuento = $request->get($preciodesc);
				
				$valor = $request->get($precio);
				
				if(isset($cantidad) and $cantidad>0){				
					$producto = $em->getRepository('JOYASJoyasBundle:Producto')->find($ids[$i]);
					$detallepedido = new DetallePedido();
					$detallepedido->setPedido($pedido);
					$detallepedido->setProducto($producto);
					
					if(isset($preciodescuento) && $preciodescuento!=''){
						$detallepedido->setPrecio($preciodescuento);
					}else{
						$detallepedido->setPrecio($valor);
					}

					$detallepedido->setCantidad($request->get($cant));
					$detallepedido->setProductodesc($producto->getDescripcion().' ( COD: '.$producto->getCodigo().' )');					

					$em->persist($detallepedido);
					$em->flush();
				}
			}		

			$message = \Swift_Message::newInstance()
			->setSubject('Nuevo Pedido Web')
			->setFrom('info@dediosjoyas.com.ar')
			->setTo('joyas@dediosjoyas.com')
			->setBody(
						$this->renderView('JOYASJoyasBundle:Default:nuevopedido.txt.twig', array('pedido'=>$pedido, 'usuarioweb'=>$usuarioweb))										
					);
			$this->get('mailer')->send($message);

			$this->sessionManager->addFlash('msgOk', 'Pedido realizado, solo falta acordar el medio de pago.');		
			$this->sessionManager->vaciarcarrito();
			return $this->redirect($this->generateUrl('joyas_joyas_formapago', array('id'=>$pedido->getId())));
		}else{
			$this->sessionManager->addFlash('msgWarn', 'Algo salio mal.');		
			return $this->redirect($this->generateUrl('joyas_joyas_checkout'));
		}
    }

    public function formapagoAction($id)
    {
	    $em = $this->getDoctrine()->getManager();
		$pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);
		$mp = new MP ("313886360384963", "pfJXYTmlA5sgxJxtXasMptlDOlb6ynKx");
		$mp->sandbox_mode(FALSE);
		$items = array(); 
	
		foreach($pedido->getDetallespedido() as $detalle){
			$titulo = $detalle->getProducto()->getDescripcion().'(COD: '.$detalle->getProducto()->getCodigo().')';
			$cantidad = $detalle->getCantidad();
			$precio = $detalle->getPrecio();

			$item = array("title" => $titulo,"quantity" => $cantidad,"currency_id" => "ARS","unit_price" => $precio);

			array_push($items, $item); 
		}
		
		$back = array(
			"success" => 'http://www.dediosjoyas.com.ar/titanio/dedios/app.php/'.$pedido->getId().'/success', 
			"failure" => 'http://www.dediosjoyas.com.ar/titanio/dedios/app.php/'.$pedido->getId().'/failure', 
			"pending" => 'http://www.dediosjoyas.com.ar/titanio/dedios/app.php/'.$pedido->getId().'/pending');

		$preference_data = array(
			"items" => $items,
			"back_urls"=>$back,
			"external_reference"=>$pedido->getId());

		$preference = $mp->create_preference ($preference_data);
		$this->sessionManager->setSession('preference',$preference);
		return $this->render('JOYASJoyasBundle:Web:elegirmedio.html.twig', array(
			'pedido' => $pedido,
//			'pagar' => $preference['response']['sandbox_init_point'],
			'pagar' => $preference['response']['init_point'],
		));
	}
	
    public function acordarAction($id)
    {
	    $em = $this->getDoctrine()->getManager();
		$pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);
		$pedido->setFormapago('O');
		$em->flush();
		
		$this->sessionManager->addFlash('msgOk', 'Medio de pago acordado.');		
		return $this->redirect($this->generateUrl('joyas_joyas_micuenta', array('idusuarioweb'=>$this->sessionManager->getSession('idusuarioweb'))));
	}

    public function pedidoAction($id)
    {
	    $em = $this->getDoctrine()->getManager();
		$pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);

        return $this->render('JOYASJoyasBundle:Web:pedido.html.twig', array(
            'pedido' => $pedido,
        ));
	}

    public function successAction($idpedido)
    {
	    $em = $this->getDoctrine()->getManager();
		$pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($idpedido);
		$pedido->setFormapago('S');
		$em->flush();		
		$this->sessionManager->addFlash('msgOk', 'Pago realizado.');		
		return $this->redirect($this->generateUrl('joyas_joyas_micuenta', array('idusuarioweb'=>$this->sessionManager->getSession('idusuarioweb'))));
	}
	
    public function failureAction($idpedido)
    {
	    $em = $this->getDoctrine()->getManager();
		$pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($idpedido);
		$pedido->setFormapago('F');
		$em->flush();
		$this->sessionManager->addFlash('msgOk', 'El pago ha fallado.');		
		return $this->redirect($this->generateUrl('joyas_joyas_micuenta', array('idusuarioweb'=>$this->sessionManager->getSession('idusuarioweb'))));
	}
	
    public function pendingAction($idpedido)
    {
	    $em = $this->getDoctrine()->getManager();
		$pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($idpedido);
		$pedido->setFormapago('P');
		$em->flush();
		$this->sessionManager->addFlash('msgOk', 'Pago pendiente de aprobacion.');		
		return $this->redirect($this->generateUrl('joyas_joyas_micuenta', array('idusuarioweb'=>$this->sessionManager->getSession('idusuarioweb'))));
	}

    public function estadopedidoAction(Request $request)
    {
		$idpago = $request->get('idpago');
		$idpedido = $request->get('idpedido');

	    $em = $this->getDoctrine()->getManager();
		$pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($idpedido);		
		$pedido->setIdpago($idpago);
		$em->flush();
		
		$mp = new MP ("313886360384963", "pfJXYTmlA5sgxJxtXasMptlDOlb6ynKx");
		
		$mp->sandbox_mode(FALSE);
		
		$payment_info = $mp->get_payment_info($idpago);
		$merchant_order_info = $mp->get("/merchant_orders/" . $payment_info["response"]["collection"]["merchant_order_id"]);
		$estado = '';
		foreach ($merchant_order_info["response"]["payments"] as  $payment) {
			if($payment['status']=='approved'){
				$pedido->setFormapago('S');
				$estado = 'APROBADO';
			}
			if($payment['status']=='pending'){
				$pedido->setFormapago('P');
				$estado = 'PENDIENTE DE PAGO';
			}
			if($payment['status']=='rejected'){
				$pedido->setFormapago('R');
				$estado = 'RECHAZADO';
			}
			if($payment['status']=='in_process'){
				$pedido->setFormapago('I');
				$estado = 'SIENDO REVISADO';
			}
			if($payment['status']=='cancelled'){
				$pedido->setFormapago('C');
				$estado = 'SIENDO REVISADO';
			}
		}
	
		$em->flush();
		$this->sessionManager->addFlash('msgWarn', 'El estado del pago es: '.$estado);		

		return $this->redirect($this->generateUrl('joyas_joyas_web'));
	}
    public function pedidogestionAction($id)
    {
	    $em = $this->getDoctrine()->getManager();
		$pedido = $em->getRepository('JOYASJoyasBundle:Pedido')->find($id);

        return $this->render('JOYASJoyasBundle:Web:pedidogestion.html.twig', array(
            'pedido' => $pedido,
        ));
	}
    public function pedidosgestionAction()
    {
	    $em = $this->getDoctrine()->getManager();
		$pedidos = $em->getRepository('JOYASJoyasBundle:Pedido')->findAll();
		
        return $this->render('JOYASJoyasBundle:Web:pedidos.html.twig', array(
            'pedidos' => $pedidos,
        ));
	}
}
?>
