<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\PagoAsignacion;
use JOYAS\JoyasBundle\Form\PagoAsignacionType;

/**
 * PagoAsignacion controller.
 *
 */
class PagoAsignacionController extends Controller
{

    /**
     * Lists all PagoAsignacion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:PagoAsignacion')->findAll();

        return $this->render('JOYASJoyasBundle:PagoAsignacion:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PagoAsignacion entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PagoAsignacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('pagoasignacion_show', array('id' => $entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:PagoAsignacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a PagoAsignacion entity.
     *
     * @param PagoAsignacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PagoAsignacion $entity)
    {
        $form = $this->createForm(new PagoAsignacionType(), $entity, array(
            'action' => $this->generateUrl('pagoasignacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new PagoAsignacion entity.
     *
     */
    public function newAction()
    {
        $entity = new PagoAsignacion();
        $form   = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:PagoAsignacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PagoAsignacion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:PagoAsignacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PagoAsignacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:PagoAsignacion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PagoAsignacion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:PagoAsignacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PagoAsignacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:PagoAsignacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PagoAsignacion entity.
    *
    * @param PagoAsignacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PagoAsignacion $entity)
    {
        $form = $this->createForm(new PagoAsignacionType(), $entity, array(
            'action' => $this->generateUrl('pagoasignacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing PagoAsignacion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:PagoAsignacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PagoAsignacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pagoasignacion_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:PagoAsignacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PagoAsignacion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:PagoAsignacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PagoAsignacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pagoasignacion'));
    }

    /**
     * Creates a form to delete a PagoAsignacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pagoasignacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
