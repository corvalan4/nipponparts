<?php

namespace JOYAS\JoyasBundle\Controller;

use JOYAS\JoyasBundle\Entity\PagoFactura;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JOYAS\JoyasBundle\Entity\PagoAsignacion;
use JOYAS\JoyasBundle\Entity\Pago;
use JOYAS\JoyasBundle\Entity\MedioDocumento;
use JOYAS\JoyasBundle\Form\PagoType;

use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * Pago controller.
 *
 */
class PagoController extends Controller
{

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionManager;

    /**
     * Lists all Pago entities.
     *
     */
    public function indexAction(Request $request)
    {
        if(!$this->sessionManager->isLogged()){
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        if(!isset($page)){
            $page = 1;
        }
        $desde = $request->get('desde');
        if(empty($desde)){
            $desde = new \DateTime('NOW -30 days');
        }else{
            $desde = new \DateTime($request->get('desde'));
        }
        $hasta = $request->get('hasta');
        if(empty($hasta)){
            $hasta = new \DateTime('NOW +1 days');
        }else{
            $hasta = new \DateTime($request->get('hasta'));
        }

        $em = $this->getDoctrine()->getManager();

        if($this->sessionManager->getPerfil()!='ADMINISTRADOR'){
            $entities = $em->getRepository('JOYASJoyasBundle:Pago')->filtro($desde, $hasta, $this->sessionManager->getUnidad()->getId());
        }else{
            $entities = $em->getRepository('JOYASJoyasBundle:Pago')->filtro($desde, $hasta, $request->get('unidad'));
        }

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        $adapter = new ArrayAdapter($entities);
        $paginador = new Pagerfanta($adapter);
        $paginador->setMaxPerPage(300);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:Pago:index.html.twig', array(
            'entities' => $paginador,
            'unidades' => $unidades,
        ));
    }
    /**
     * Creates a new Pago entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Pago();
        $form = $this->createCreateForm($entity);
        $form
            ->add('facturas', 'entity', array (
                'class' => 'JOYASJoyasBundle:Factura',
                'label' => 'Facturas',
                'mapped'=>false,
                'multiple'=>true
            ));

        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $facturasAsociadas = $form['facturas']->getData();

//        $proveedor = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($request->get('clienteProveedor'));
        $medios = $em->getRepository('JOYASJoyasBundle:Medio')->findBy(array(),array('descripcion'=>'ASC'));
//        $entity->setClienteProveedor($proveedor);

        if($this->sessionManager->getPerfil()=='ADMINISTRADOR'){
            $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidad'));
            $entity->setUnidadnegocio($unidad);
        }else{
            $entity->setUnidadnegocio($this->sessionManager->getUnidad());
        }

        $importemedios = 0;
        foreach ($medios as $medio){
            $importemedio = $request->get('importemedio' . $medio->getId());
            $numerocupon = $request->get('numerocupon' . $medio->getId());
            $numerotransaccion = $request->get('numerotransaccion' . $medio->getId());
            $cuotas = $request->get('cuotas' . $medio->getId());
            if (!empty($importemedio)) {
                $mediodocumento = new MedioDocumento();
                $mediodocumento->setMedio($medio);
                $mediodocumento->setPago($entity);
                $mediodocumento->setImporte($importemedio);
                $mediodocumento->setNumerolote((int)$numerotransaccion);
                $mediodocumento->setNumerocupon((int)$numerocupon);
                $mediodocumento->setCuotas((int)$cuotas);

                $em->persist($mediodocumento);
                $importemedios += $importemedio;
            }
        }

        $importeRetenciones = 0;

        $tiporetenciones = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
        for($i = 1; $i<=count($tiporetenciones); $i++){
            if($request->get('retencionimporte'.$i)!='' and $request->get('retencionimporte'.$i)>0){
                $retencion = new Retencion();
                $tiporetencion = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findOneBy(array('descripcion'=>$request->get('retencion'.$i)));
                $retencion->setImporte($request->get('retencionimporte'.$i));
                $retencion->setTiporetencion($tiporetencion);
                $retencion->setPago($entity);
                $em->persist($retencion);
                $importeRetenciones+=$request->get('retencionimporte'.$i);
            }
        }
        $importe = $importeRetenciones + $importemedios;
        $entity->setImporte($importe);

        if ($form->isValid()) {
            $em->persist($entity);

            // Se asigna Pago.
            $pagoAsignacion = new PagoAsignacion();
            $pagoAsignacion->setPago($entity);
            $pagoAsignacion->setFecha($entity->getFecha());
            $pagoAsignacion->setClienteProveedor($entity->getClienteProveedor());
            $pagoAsignacion->setImporte($importe);
            $em->persist($pagoAsignacion);
            $em->flush();
            $entity->setNrocomprobante($entity->getId());

            if(count($facturasAsociadas)>0){
                foreach ($facturasAsociadas as $facturasAsociada){
                    $pagofactura = new PagoFactura();
                    $pagofactura->setPago($entity);
                    $pagofactura->setImporte($importe);
                    $pagofactura->setFactura($facturasAsociada);

                    $em->persist($pagofactura);
                }
                $em->flush();
                return $this->redirect($this->generateUrl('pago_show', array('id' => $entity->getId())));
            }

            $em->flush();
            return $this->redirect($this->generateUrl('pago_show', array('id' => $entity->getId())));
        }

        $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('clienteProveedor'=>2));
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        $medios = $em->getRepository('JOYASJoyasBundle:Medio')->findBy(array(),array('descripcion'=>'ASC'));
        $tiporetenciones = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
        $cheques = $em->getRepository('JOYASJoyasBundle:Cheque')->findBy(array('estado'=>'A'),array('fechacobro'=>'ASC'));

        return $this->render('JOYASJoyasBundle:Pago:new.html.twig', array(
                'entity'           => $entity,
                'unidades'         => $unidades,
                'medios'           => $medios,
                'tiporetenciones'  => $tiporetenciones,
                'cheques'          => $cheques,
                'form'             => $form->createView(),
                'clientesProveedores' => $cliProvs)
        );
    }
    
    /**
     * Creates a form to create a Pago entity.
     *
     * @param Pago $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pago $entity)
    {
        $form = $this->createForm(new PagoType(), $entity, array(
            'action' => $this->generateUrl('pago_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr'=> array('class'=>'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Pago entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Pago();
        $entity->setFecha(new \DateTime("NOW"));

        $cliProvs = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->findBy(array('clienteProveedor'=>2));
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        $medios = $em->getRepository('JOYASJoyasBundle:Medio')->findBy(array(),array('descripcion'=>'ASC'));
        $tiporetenciones = $em->getRepository('JOYASJoyasBundle:TipoRetencion')->findBy(array('estado'=>'A'),array('descripcion'=>'ASC'));
        $cheques = $em->getRepository('JOYASJoyasBundle:Cheque')->findBy(array('estado'=>'A'),array('fechacobro'=>'ASC'));

        $idfacturas = $request->get('idfacturas');
        $totalFacturas = 0;
        $arrFacturas = array();
        if(!empty($idfacturas)){
            foreach($idfacturas as $idfactura){
                $facturas = $em->getRepository('JOYASJoyasBundle:Factura')->find($idfactura);
                if($facturas){
                    $arrFacturas[] = $facturas;
                    $entity->setClienteProveedor($facturas->getClienteProveedor());
                    $totalFacturas+=$facturas->getImporte();
                }
            }
            $entity->setClienteProveedor($facturas->getClienteProveedor());
            $entity->setFecha(new \DateTime("NOW"));
        }
        $form   = $this->createCreateForm($entity);
        if(count($arrFacturas)>0){
            $form
                ->add('facturas', 'entity', array (
                    'class' => 'JOYASJoyasBundle:Factura',
                    'label' => 'Facturas',
                    'mapped'=>false,
                    'data'=> $arrFacturas,
                    'multiple'=>true,
                    'query_builder' => function (\JOYAS\JoyasBundle\Entity\FacturaRepository $repository) use ($idfacturas)
                    {
                        return $repository->createQueryBuilder('c')
                            ->where('c.id IN (?1)')
                            ->setParameter(1, $idfacturas);
                    }
                ));
        }else{
            $form
                ->add('facturas', 'choice', array(
                        'choices'   => array(),
                        'mapped'    => false,
                        'required'  => false)
                );
        }


        return $this->render('JOYASJoyasBundle:Pago:new.html.twig', array(
                'entity'           => $entity,
                'medios'         => $medios,
                'unidades'         => $unidades,
                'tiporetenciones'  => $tiporetenciones,
                'cheques'          => $cheques,
                'form'             => $form->createView(),
                'facturas'         => $arrFacturas,
                'totalFacturas'    => $totalFacturas,
                'clientesProveedores' => $cliProvs)
        );
    }

    /**
     * Finds and displays a Pago entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Pago')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pago entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Pago:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pago entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Pago')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pago entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Pago:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Pago entity.
     *
     * @param Pago $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Pago $entity)
    {
        $form = $this->createForm(new PagoType(), $entity, array(
            'action' => $this->generateUrl('pago_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-success')));

        return $form;
    }
    /**
     * Edits an existing Pago entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Pago')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pago entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pago_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:Pago:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Pago entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Pago')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Pago entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pago'));
    }

    /**
     * Creates a form to delete a Pago entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pago_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}
