<?php

namespace JOYAS\JoyasBundle\Services;
use JOYAS\JoyasBundle\Entity\NotaCreditoDebito;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
// AFIP
use JOYAS\JoyasBundle\Afip\Exceptionhandler;
use JOYAS\JoyasBundle\Afip\WSAA;
use JOYAS\JoyasBundle\Afip\WSFEV1;


class SessionManager
{
    /**
     *
     * @var Container
     */
    public $container;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var Session
     */
    public $session;

    public function __construct(Container $container){
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->session = $container->get('session');
    }

    public function startSession(){
        $this->session->start();
        $this->setSession('login', false);
    }

    public function clearSession(){
        $this->session->clear();
        $this->session->getFlashBag()->clear();
    }

    public function closeSession(){
        $this->clearSession();
        $this->startSession();
    }

    public function agregaralcarrito($id){
        $ids = $this->getSession('carrito');
        if($ids==''){
            $ids = $id;
        }else{
            $ids = $ids.','.$id;
        }
        $this->setSession('carrito', $ids);
    }

    public function quitardelcarrito($id){
        $ids = $this->getSession('carrito');
        $nuevoids = str_replace($id, '', $ids);
        $this->setSession('carrito', $nuevoids);
    }

    public function vaciarcarrito(){
        $this->setSession('carrito', '');
    }

    public function getPerfil(){
        return $this->getSession('usuario')->getPerfil();
    }

    public function getUnidad(){
        return $this->em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->getSession('unidad'));
    }

    public function login($login, $pass){
        $usuario = $this->em->getRepository('JOYASJoyasBundle:Usuario')->findOneBy(array('login' => $login, 'clave'=>$this->encryptIt($pass), 'estado'=>'A'));
        if ($usuario){
            $this->setSession('login', true);
            $this->setSession('usuario', $usuario);
            if($usuario->getUnidadNegocio()){
                $this->setSession('unidad', $usuario->getUnidadNegocio()->getId());
            }
            return true;
        }else{
            $this->addFlash('msgError','Usuario o clave incorrecto. Vuelva a intentar.');
        }

        return false;
    }

    /**
     * Agrega un mensaje de tipo msgType a la siguiente response.
     * msgType validos: msgOk, msgInfo, msgWarn, msgError.
     * @param string $msgType
     * @param string $msg
     */
    public function addFlash($msgType, $msg){
        $this->session->getFlashBag()->add($msgType, $msg);
    }

    /**
     * Setea un parametro en la sesion
     * @param string $attr
     * @param string $value
     * @return mixed
     */
    public function setSession($attr, $value){
        $this->session->set($attr, $value);
    }

    /**
     * Devuelve un valor de la sesion
     * @param string $attr
     * @return mixed
     */
    public function getSession($attr){
        return $this->session->get($attr);
    }

    public function isLogged(){
        $isLogged = $this->getSession('login');
        return !empty($isLogged) ? $this->getSession('login') : false;
    }

    public function decryptIt( $q ) {
        $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
        $qDecoded  = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }

    public function facturar($entity) {
        $cuitEmisor = str_replace("-", "", $entity->getUnidadNegocio()->getCuit());
        $wsaa = new WSAA(false, 'wsfe');

        if($wsaa->get_expiration() < (new \DateTime('NOW'))) {
            $wsaa->generar_TA();
        }

        $wsfe = new WSFEV1(false);
        $wsfe->openTA();
        $ptovta = $entity->getPtovta();
        $tipofactura = $entity->getTipofactura();
        $tipocbte = ($entity->getTipofactura() == 'A') ? 01 : 06;

        $nc = '';
        $cmp = $wsfe->FECompUltimoAutorizado($tipocbte, $ptovta, $cuitEmisor);

        $regfac['concepto'] = 1; 					        # 1: productos, 2: servicios, 3: ambos
        $regfac['tipodocumento'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Consumidor Final' and $entity->getImporte()<10000) ? 99 : 80; // Aca falta el 96 si es nroDoc.
        $regfac['numerodocumento'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Consumidor Final' and $entity->getImporte()<10000) ? 0 : $entity->getClienteProveedor()->getDni(); # 0 para Consumidor Final (<$10000)
        $regfac['cuit'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Consumidor Final' and $entity->getImporte()<10000) ? 0 : str_replace("-", "", $entity->getClienteProveedor()->getCuit()); # 0 para Consumidor Final (<$10000)
        $regfac['capitalafinanciar'] = 0;

        // Trato de IVA
        if(count($entity->getIvas())>0){
            foreach($entity->getIvas() as $iva){
                if($iva->getTipoIva()->getPorcentaje()==21){
                    $importeiva['21'] = $iva->getValor();
                    $baseimp = (100*$importeiva['21']/21);
                }
                if($iva->getTipoIva()->getPorcentaje()==10.5){
                    $importeiva['10-5'] = $iva->getValor();
                    $baseimp = (100*$importeiva['10-5']/10.5);
                }
            }
        }

        if($entity->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Responsable Inscripto'){
            $regfac['coniva'] = "SI";
            $regfac['importetotal'] = $entity->getImporte();	       # total del comprobante
            $regfac['importeiva'] = (!empty($importeiva['21']) ? $importeiva['21'] : 0);	                   # subtotal neto sujeto a IVA
            $regfac['baseimp'] = round($baseimp, 2);
            $regfac['importeneto'] = ($entity->getImporte() - round($importeiva['21'] ,2));
        }else{
            $regfac['importetotal'] = $entity->getImporte();	# total del comprobante
            $regfac['importeiva'] = 0;
            $regfac['importeneto'] = 0;			# subtotal neto sujeto a IVA
            $regfac['coniva'] = "NO";
            $regfac['capitalafinanciar'] = $entity->getImporte();
        }

        $regfac['fechaemision'] = $entity->getFecha()->format('Ymd');
        $regfac['fechadesde'] = '';
        $regfac['fechahasta'] = '';

        $regfac['fecha_'] = '';
        $regfac['imp_trib'] = 1.0;
        $regfac['imp_op_ex'] = 0.0;
        $regfac['nrofactura'] = $cmp->FECompUltimoAutorizadoResult->CbteNro + 1;
        $regfac['fecha_venc_pago'] = date('Ymd');

        // Armo con la factura los parametros de entrada para el pedido
        $params = $wsfe->armadoFacturaUnica(
            $tipofactura,
            $ptovta,    // el punto de venta
            $nc,
            $regfac,     // los datos a facturar
            $cuitEmisor
        );

        //Solicito el CAE
        $cae = $wsfe->solicitarCAE($params, $cuitEmisor);

        if($cae->FECAESolicitarResult->FeCabResp->Resultado=='A' or $cae->FECAESolicitarResult->FeCabResp->Resultado=='P'){
            $entity->setNrofactura($regfac['nrofactura']);
            $entity->setCae($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAE);
            $entity->setFechavtocae(new \DateTime($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAEFchVto));
            $this->em->flush();
            return true;
        }else{
            if($cae->FECAESolicitarResult->FeCabResp->Resultado=='R'){
                $observaciones = "";
                if(isset($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones)){
                    $observaciones = json_encode($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones);
                    $entity->setObservacion($entity->getObservacion()." - ERROR: $observaciones");
                    $this->em->flush();
                }
                if(isset($cae->FECAESolicitarResult->Errors->Err)){
                    $observaciones = json_encode($cae->FECAESolicitarResult->Errors->Err);
                    $entity->setObservacion($entity->getObservacion()." - ERROR: $observaciones");
                    $this->em->flush();
                }
                $this->addFlash('msgError','Error al dar de alta factura: '. json_encode($observaciones) );
            }
            return false;
        }
    }

    public function facturarNota(NotaCreditoDebito $entity) {
        $cuitEmisor = str_replace("-", "", $entity->getUnidadNegocio()->getCuit());
        $wsaa = new WSAA(false, 'wsfe');

        if($wsaa->get_expiration() < (new \DateTime('NOW'))) {
            $wsaa->generar_TA();
        }

        $wsfe = new WSFEV1(false);
        $wsfe->openTA();
        $ptovta = $entity->getPtovta();
        $tipofactura = $entity->getCodigonota();

        if($entity->getTiponota()=='debito'){
            $tipocbte = ($tipofactura == 'A') ? 2 : 7;
        }else{
            $tipocbte = ($tipofactura == 'A') ? 3 : 8;
        }

        $cmp = $wsfe->FECompUltimoAutorizado($tipocbte, $ptovta, $cuitEmisor);

        $regfac['concepto'] = 1; 					        # 1: productos, 2: servicios, 3: ambos
        $regfac['tipodocumento'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Consumidor Final' and $entity->getImporte()<10000) ? 99 : 80; // Aca falta el 96 si es nroDoc.
        $regfac['numerodocumento'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Consumidor Final' and $entity->getImporte()<10000) ? 0 : $entity->getClienteProveedor()->getDni(); # 0 para Consumidor Final (<$10000)
        $regfac['cuit'] = ($entity->getClienteProveedor()->getCondicioniva()->getDescripcion()=='Consumidor Final' and $entity->getImporte()<10000) ? 0 : str_replace("-", "", $entity->getClienteProveedor()->getCuit()); # 0 para Consumidor Final (<$10000)
        $regfac['capitalafinanciar'] = 0;

        if(count($entity->getIvas())>0){
            foreach($entity->getIvas() as $iva){
                if($iva->getTipoIva()->getPorcentaje()==21){
                    $importeiva['21'] = $iva->getValor();
                    $baseimp = (100*$importeiva['21']/21);
                }
                if($iva->getTipoIva()->getPorcentaje()==10.5){
                    $importeiva['10-5'] = $iva->getValor();
                    $baseimp = (100*$importeiva['10-5']/10.5);
                }
            }
        }

        if($entity->getClienteProveedor()->getCondicioniva() == 'Responsable Inscripto'){
            $regfac['coniva'] = "SI";
            $regfac['importetotal'] = $entity->getImporte();	                                # total del comprobante
            $regfac['importeiva'] = (!empty($importeiva['21']) ? $importeiva['21'] : 0);	    # subtotal neto sujeto a IVA
            $regfac['baseimp'] = round($baseimp, 2);
            $regfac['importeneto'] = ($entity->getImporte() - round($importeiva['21'] ,2));
        }else{
            $regfac['importetotal'] = $entity->getImporte();	# total del comprobante
            $regfac['importeiva'] = 0;
            $regfac['importeneto'] = 0;			# subtotal neto sujeto a IVA
            $regfac['coniva'] = "NO";
            $regfac['capitalafinanciar'] = $entity->getImporte();
        }

        $regfac['fechaemision'] = $entity->getFecha()->format('Ymd');
        $regfac['fechadesde'] = '';
        $regfac['fechahasta'] = '';

        $regfac['fecha_'] = '';
        $regfac['imp_trib'] = 1.0;
        $regfac['imp_op_ex'] = 0.0;
        $regfac['nrofactura'] = $cmp->FECompUltimoAutorizadoResult->CbteNro + 1;
        $regfac['fecha_venc_pago'] = date('Ymd');

        $params = $wsfe->armadoFacturaUnica($tipofactura,$ptovta,$entity->getTiponota(),$regfac,$cuitEmisor);
        $cae = $wsfe->solicitarCAE($params, $cuitEmisor);

        if($cae->FECAESolicitarResult->FeCabResp->Resultado=='A' or $cae->FECAESolicitarResult->FeCabResp->Resultado=='P'){
            $entity->setNrocomprobante($regfac['nrofactura']);
            $entity->setCae($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAE);
            $entity->setFechavtocae(new \DateTime($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->CAEFchVto));
            $this->em->flush();
            return true;
        }else{
            if($cae->FECAESolicitarResult->FeCabResp->Resultado=='R'){
                $observaciones = "";
                if(isset($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones)){
                    $observaciones = json_encode($cae->FECAESolicitarResult->FeDetResp->FECAEDetResponse->Observaciones);
                }
                if(isset($cae->FECAESolicitarResult->Errors->Err)){
                    $observaciones = json_encode($cae->FECAESolicitarResult->Errors->Err);
                }
                $entity->setDescripcion($entity->getDescripcion()." - ERROR: $observaciones");
                $this->em->flush();
                $this->addFlash('msgError','Error al dar de alta factura: '. $observaciones );
            }

            return false;
        }
    }

    public static function obtenerTextoCodigoBarras($v_cuit, $v_tipo_comprobante, $v_punto_venta, $v_cae, $v_vencimiento_cae){
        $aux_1 = 0;
        $aux_2 = 0;

        $base_calculo = $v_cuit.$v_tipo_comprobante.$v_punto_venta.$v_cae.$v_vencimiento_cae;

        $largo = strlen(trim($base_calculo));

        for ($i=0; $i<$largo+1; $i = $i + 2)
            $aux_1 = $aux_1 + substr($base_calculo,$i,1);

        for ($i=1; $i<$largo+1; $i = $i + 2)
            $aux_2 = $aux_2 + substr($base_calculo,$i,1);

        $aux_3 = $aux_1 * 3;
        $aux_4 = $aux_3 + $aux_2;

        $largo_final = strlen(trim($aux_4));
        $digito = 10 - substr($aux_4,$largo_final-1,1);

        return $base_calculo.$digito;
    }


    public function encryptIt( $clave ) {
        $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
        $qEncoded = base64_encode( mcrypt_encrypt( MCRYPT_BLOWFISH, $cryptKey, $clave, MCRYPT_MODE_ECB, "\0"));
        return( $qEncoded );
    }
}
