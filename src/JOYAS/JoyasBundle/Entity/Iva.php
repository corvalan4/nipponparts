<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\IvaRepository")
 * @ORM\Table(name="iva")
 */
class Iva{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $valor;

	/**
	* @ORM\ManyToOne(targetEntity="NotaCreditoDebito", inversedBy="ivas")
	* @ORM\JoinColumn(name="notacreditodebito_id", referencedColumnName="id", nullable=true)
	*/
    protected $notaCreditoDebito;

	/**
	* @ORM\ManyToOne(targetEntity="Factura", inversedBy="ivas")
	* @ORM\JoinColumn(name="factura_id", referencedColumnName="id", nullable=true)
	*/
    protected $factura;

	/**
	* @ORM\ManyToOne(targetEntity="TipoIva", inversedBy="ivas")
	* @ORM\JoinColumn(name="tipoiva_id", referencedColumnName="id", nullable=true)
	*/
    protected $tipoIva;

	/**
	* @ORM\ManyToOne(targetEntity="Gasto", inversedBy="ivas")
	* @ORM\JoinColumn(name="gasto_id", referencedColumnName="id", nullable=true)
	*/
    protected $gasto;

	/**
	* @ORM\ManyToOne(targetEntity="FacturaImportacion", inversedBy="ivas")
	* @ORM\JoinColumn(name="facturaimportacion_id", referencedColumnName="id", nullable=true)
	*/
    protected $facturaImportacion;
	
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return $this->getValor();	
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Iva
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    
        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Iva
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set notaCreditoDebito
     *
     * @param \JOYAS\JoyasBundle\Entity\NotaCreditoDebito $notaCreditoDebito
     * @return Iva
     */
    public function setNotaCreditoDebito(\JOYAS\JoyasBundle\Entity\NotaCreditoDebito $notaCreditoDebito = null)
    {
        $this->notaCreditoDebito = $notaCreditoDebito;
    
        return $this;
    }

    /**
     * Get notaCreditoDebito
     *
     * @return \JOYAS\JoyasBundle\Entity\NotaCreditoDebito 
     */
    public function getNotaCreditoDebito()
    {
        return $this->notaCreditoDebito;
    }

    /**
     * Set factura
     *
     * @param \JOYAS\JoyasBundle\Entity\Factura $factura
     * @return Iva
     */
    public function setFactura(\JOYAS\JoyasBundle\Entity\Factura $factura = null)
    {
        $this->factura = $factura;
    
        return $this;
    }

    /**
     * Get factura
     *
     * @return \JOYAS\JoyasBundle\Entity\Factura 
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * Set tipoIva
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoIva $tipoIva
     * @return Iva
     */
    public function setTipoIva(\JOYAS\JoyasBundle\Entity\TipoIva $tipoIva = null)
    {
        $this->tipoIva = $tipoIva;
    
        return $this;
    }

    /**
     * Get tipoIva
     *
     * @return \JOYAS\JoyasBundle\Entity\TipoIva 
     */
    public function getTipoIva()
    {
        return $this->tipoIva;
    }

    /**
     * Set gasto
     *
     * @param \JOYAS\JoyasBundle\Entity\Gasto $gasto
     * @return Iva
     */
    public function setGasto(\JOYAS\JoyasBundle\Entity\Gasto $gasto = null)
    {
        $this->gasto = $gasto;
    
        return $this;
    }

    /**
     * Get gasto
     *
     * @return \JOYAS\JoyasBundle\Entity\Gasto 
     */
    public function getGasto()
    {
        return $this->gasto;
    }

    /**
     * Set facturaImportacion
     *
     * @param \JOYAS\JoyasBundle\Entity\FacturaImportacion $facturaImportacion
     * @return Iva
     */
    public function setFacturaImportacion(\JOYAS\JoyasBundle\Entity\FacturaImportacion $facturaImportacion = null)
    {
        $this->facturaImportacion = $facturaImportacion;
    
        return $this;
    }

    /**
     * Get facturaImportacion
     *
     * @return \JOYAS\JoyasBundle\Entity\FacturaImportacion 
     */
    public function getFacturaImportacion()
    {
        return $this->facturaImportacion;
    }
}