<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\PagoRepository")
 * @ORM\Table(name="pago")
 */
class Pago{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
		
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaRegistracion;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $importe;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="pagos")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteProveedor;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nrocomprobante;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="pagos")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadnegocio;

	/**
	* @ORM\OneToMany(targetEntity="PagoAsignacion", mappedBy="pago", cascade={"persist"})
	*/
	protected $pagosAsignacion;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="pago", cascade={"persist"})
     */
    protected $cheques;

    /**
     * @ORM\OneToMany(targetEntity="PagoFactura", mappedBy="pago", cascade={"persist"})
     */
    protected $pagosfactura;

    /**
     * @ORM\OneToMany(targetEntity="MedioDocumento", mappedBy="pago", cascade={"persist"})
     */
    protected $medios;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $efectivo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $puntoventa;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $bonificacion;

    /**
     * @ORM\OneToMany(targetEntity="Iva", mappedBy="pago", cascade={"persist"})
     */
    protected $ivas;

    /**
     * @ORM\OneToMany(targetEntity="IngresosBrutos", mappedBy="pago", cascade={"persist"})
     */
    protected $ingresosbrutos;

    /**
     * @ORM\OneToMany(targetEntity="Retencion", mappedBy="pago", cascade={"persist"})
     */
    protected $retenciones;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
        $this->medios = new ArrayCollection();
        $this->ivas = new ArrayCollection();
        $this->ingresosbrutos = new ArrayCollection();
        $this->pagosAsignacion = new ArrayCollection();
        $this->pagosfactura = new ArrayCollection();
        $this->retenciones = new ArrayCollection();
        $this->fechaRegistracion = new \DateTime('NOW');
	}
		

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	    $nro = !empty($this->nrocomprobante) ? $this->nrocomprobante : $this->id;
        return "PAGO ".str_pad($nro, 5, "0", STR_PAD_LEFT);
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pago
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set fechaRegistracion
     *
     * @param \DateTime $fechaRegistracion
     * @return Pago
     */
    public function setFechaRegistracion($fechaRegistracion)
    {
        $this->fechaRegistracion = $fechaRegistracion;
    
        return $this;
    }

    /**
     * Get fechaRegistracion
     *
     * @return \DateTime 
     */
    public function getFechaRegistracion()
    {
        return $this->fechaRegistracion;
    }

    /**
     * Set importe
     *
     * @param string $importe
     * @return Pago
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    
        return $this;
    }

    /**
     * Get importe
     *
     * @return string 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set nrocomprobante
     *
     * @param string $nrocomprobante
     * @return Pago
     */
    public function setNrocomprobante($nrocomprobante)
    {
        $this->nrocomprobante = $nrocomprobante;
    
        return $this;
    }

    /**
     * Get nrocomprobante
     *
     * @return string 
     */
    public function getNrocomprobante()
    {
        return $this->nrocomprobante;
    }

    /**
     * Set efectivo
     *
     * @param float $efectivo
     * @return Pago
     */
    public function setEfectivo($efectivo)
    {
        $this->efectivo = $efectivo;
    
        return $this;
    }

    /**
     * Get efectivo
     *
     * @return float 
     */
    public function getEfectivo()
    {
        return $this->efectivo;
    }

    /**
     * Set puntoventa
     *
     * @param integer $puntoventa
     * @return Pago
     */
    public function setPuntoventa($puntoventa)
    {
        $this->puntoventa = $puntoventa;
    
        return $this;
    }

    /**
     * Get puntoventa
     *
     * @return integer 
     */
    public function getPuntoventa()
    {
        return $this->puntoventa;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Pago
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Pago
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    
        return $this;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set bonificacion
     *
     * @param float $bonificacion
     * @return Pago
     */
    public function setBonificacion($bonificacion)
    {
        $this->bonificacion = $bonificacion;
    
        return $this;
    }

    /**
     * Get bonificacion
     *
     * @return float 
     */
    public function getBonificacion()
    {
        return $this->bonificacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Pago
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add pagosAsignacion
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoAsignacion $pagosAsignacion
     * @return Pago
     */
    public function addPagosAsignacion(\JOYAS\JoyasBundle\Entity\PagoAsignacion $pagosAsignacion)
    {
        $this->pagosAsignacion[] = $pagosAsignacion;
    
        return $this;
    }

    /**
     * Remove pagosAsignacion
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoAsignacion $pagosAsignacion
     */
    public function removePagosAsignacion(\JOYAS\JoyasBundle\Entity\PagoAsignacion $pagosAsignacion)
    {
        $this->pagosAsignacion->removeElement($pagosAsignacion);
    }

    /**
     * Get pagosAsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosAsignacion()
    {
        return $this->pagosAsignacion;
    }

    /**
     * Add cheques
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheques
     * @return Pago
     */
    public function addCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheques)
    {
        $this->cheques[] = $cheques;
    
        return $this;
    }

    /**
     * Remove cheques
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheques
     */
    public function removeCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheques)
    {
        $this->cheques->removeElement($cheques);
    }

    /**
     * Get cheques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheques()
    {
        return $this->cheques;
    }

    /**
     * Add ivas
     *
     * @param \JOYAS\JoyasBundle\Entity\Iva $ivas
     * @return Pago
     */
    public function addIva(\JOYAS\JoyasBundle\Entity\Iva $ivas)
    {
        $this->ivas[] = $ivas;
    
        return $this;
    }

    /**
     * Remove ivas
     *
     * @param \JOYAS\JoyasBundle\Entity\Iva $ivas
     */
    public function removeIva(\JOYAS\JoyasBundle\Entity\Iva $ivas)
    {
        $this->ivas->removeElement($ivas);
    }

    /**
     * Get ivas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIvas()
    {
        return $this->ivas;
    }

    /**
     * Add ingresosbrutos
     *
     * @param \JOYAS\JoyasBundle\Entity\IngresosBrutos $ingresosbrutos
     * @return Pago
     */
    public function addIngresosbruto(\JOYAS\JoyasBundle\Entity\IngresosBrutos $ingresosbrutos)
    {
        $this->ingresosbrutos[] = $ingresosbrutos;
    
        return $this;
    }

    /**
     * Remove ingresosbrutos
     *
     * @param \JOYAS\JoyasBundle\Entity\IngresosBrutos $ingresosbrutos
     */
    public function removeIngresosbruto(\JOYAS\JoyasBundle\Entity\IngresosBrutos $ingresosbrutos)
    {
        $this->ingresosbrutos->removeElement($ingresosbrutos);
    }

    /**
     * Get ingresosbrutos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIngresosbrutos()
    {
        return $this->ingresosbrutos;
    }

    /**
     * Add retenciones
     *
     * @param \JOYAS\JoyasBundle\Entity\Retencion $retenciones
     * @return Pago
     */
    public function addRetencione(\JOYAS\JoyasBundle\Entity\Retencion $retenciones)
    {
        $this->retenciones[] = $retenciones;
    
        return $this;
    }

    /**
     * Remove retenciones
     *
     * @param \JOYAS\JoyasBundle\Entity\Retencion $retenciones
     */
    public function removeRetencione(\JOYAS\JoyasBundle\Entity\Retencion $retenciones)
    {
        $this->retenciones->removeElement($retenciones);
    }

    /**
     * Get retenciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRetenciones()
    {
        return $this->retenciones;
    }

    /**
     * Set unidadnegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadnegocio
     * @return Pago
     */
    public function setUnidadnegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadnegocio = null)
    {
        $this->unidadnegocio = $unidadnegocio;

        return $this;
    }

    /**
     * Get unidadnegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio
     */
    public function getUnidadnegocio()
    {
        return $this->unidadnegocio;
    }

    /**
     * Add medios
     *
     * @param \JOYAS\JoyasBundle\Entity\MedioDocumento $medios
     * @return Pago
     */
    public function addMedio(\JOYAS\JoyasBundle\Entity\MedioDocumento $medios)
    {
        $this->medios[] = $medios;
    
        return $this;
    }

    /**
     * Remove medios
     *
     * @param \JOYAS\JoyasBundle\Entity\MedioDocumento $medios
     */
    public function removeMedio(\JOYAS\JoyasBundle\Entity\MedioDocumento $medios)
    {
        $this->medios->removeElement($medios);
    }

    /**
     * Get medios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedios()
    {
        return $this->medios;
    }

    /**
     * Set clienteProveedor
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Pago
     */
    public function setClienteProveedor(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor = null)
    {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \JOYAS\JoyasBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor()
    {
        return $this->clienteProveedor;
    }

    /**
     * Add pagosfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura
     * @return Pago
     */
    public function addPagosfactura(\JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura)
    {
        $this->pagosfactura[] = $pagosfactura;
    
        return $this;
    }

    /**
     * Remove pagosfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura
     */
    public function removePagosfactura(\JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura)
    {
        $this->pagosfactura->removeElement($pagosfactura);
    }

    /**
     * Get pagosfactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosfactura()
    {
        return $this->pagosfactura;
    }
}