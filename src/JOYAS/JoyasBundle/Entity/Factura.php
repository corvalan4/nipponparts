<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\FacturaRepository")
 * @ORM\Table(name="factura")
 */
class Factura {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal", inversedBy="facturas")
     * @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
     */
    protected $sucursal; // Para F

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="facturas")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
     */
    protected $clienteProveedor; // Para FP

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="facturas")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tipo = "F"; // Puede ser F por FACTURA o FP por FACTURAPROVEEDOR

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;

    /**
     * @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="factura", cascade={"persist", "remove"} )
     */
    protected $productosFactura;

    /**
     * @ORM\OneToMany(targetEntity="Iva", mappedBy="factura")
     */
    protected $ivas;

    /**
     * @ORM\OneToMany(targetEntity="CobranzaFactura", mappedBy="factura")
     */
    protected $cobranzasfactura;

    /**
     * @ORM\OneToMany(targetEntity="PagoFactura", mappedBy="factura")
     */
    protected $pagosfactura;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tipofactura; // A o B

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $cae;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechavtocae;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nroremito;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $nrofactura;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $ptovta;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $mesanio;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $localidademision;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $bonificacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $percepcionib;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $percepcioniva;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     *
     * ******************************** */

    public function __construct() {
        $this->productosFactura = new ArrayCollection();
        $this->ivas = new ArrayCollection();
        $this->cobranzasfactura = new ArrayCollection();
        $this->pagosfactura = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        $nro = !empty($this->nrofactura) ? $this->nrofactura : $this->id;
        return "FACTURA" . " " . $this->getTipofactura() . " " . str_pad($nro, 5, "0", STR_PAD_LEFT);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Factura
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Factura
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Factura
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Get importe
     *
     * @return float
     */
    public function getImporteNeto() {
        $importesIva = 0;
        foreach ($this->ivas as $iva) {
            $importesIva += $iva->getValor();
        }
        return $this->importe - $importesIva;
    }

    /**
     * Set tipofactura
     *
     * @param string $tipofactura
     * @return Factura
     */
    public function setTipofactura($tipofactura) {
        $this->tipofactura = $tipofactura;

        return $this;
    }

    /**
     * Get tipofactura
     *
     * @return string
     */
    public function getTipofactura() {
        return $this->tipofactura;
    }

    /**
     * Set cae
     *
     * @param string $cae
     * @return Factura
     */
    public function setCae($cae) {
        $this->cae = $cae;

        return $this;
    }

    /**
     * Get cae
     *
     * @return string
     */
    public function getCae() {
        return $this->cae;
    }

    /**
     * Set fechavtocae
     *
     * @param \DateTime $fechavtocae
     * @return Factura
     */
    public function setFechavtocae($fechavtocae) {
        $this->fechavtocae = $fechavtocae;

        return $this;
    }

    /**
     * Get fechavtocae
     *
     * @return \DateTime
     */
    public function getFechavtocae() {
        return $this->fechavtocae;
    }

    /**
     * Set nroremito
     *
     * @param string $nroremito
     * @return Factura
     */
    public function setNroremito($nroremito) {
        $this->nroremito = $nroremito;

        return $this;
    }

    /**
     * Get nroremito
     *
     * @return string
     */
    public function getNroremito() {
        return $this->nroremito;
    }

    /**
     * Set nrofactura
     *
     * @param integer $nrofactura
     * @return Factura
     */
    public function setNrofactura($nrofactura) {
        $this->nrofactura = $nrofactura;

        return $this;
    }

    /**
     * Get nrofactura
     *
     * @return integer
     */
    public function getNrofactura() {
        $nro = !empty($this->nrofactura) ? str_pad($this->nrofactura, 5, "0", STR_PAD_LEFT) : '';
        return $nro;
    }

    /**
     * Set ptovta
     *
     * @param integer $ptovta
     * @return Factura
     */
    public function setPtovta($ptovta) {
        $this->ptovta = $ptovta;

        return $this;
    }

    /**
     * Get ptovta
     *
     * @return integer
     */
    public function getPtovta() {
        return $this->ptovta;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Factura
     */
    public function setObservacion($observacion) {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion() {
        return $this->observacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Factura
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set sucursal
     *
     * @param \JOYAS\JoyasBundle\Entity\Sucursal $sucursal
     * @return Factura
     */
    public function setSucursal(\JOYAS\JoyasBundle\Entity\Sucursal $sucursal = null) {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \JOYAS\JoyasBundle\Entity\Sucursal
     */
    public function getSucursal() {
        return $this->sucursal;
    }

    /**
     * Set clienteProveedor
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Factura
     */
    public function setClienteProveedor(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \JOYAS\JoyasBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Factura
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Add productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     * @return Factura
     */
    public function addProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura[] = $productosFactura;

        return $this;
    }

    /**
     * Remove productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     */
    public function removeProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura->removeElement($productosFactura);
    }

    /**
     * Get productosFactura
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductosFactura() {
        return $this->productosFactura;
    }

    /**
     * Add ivas
     *
     * @param \JOYAS\JoyasBundle\Entity\Iva $ivas
     * @return Factura
     */
    public function addIva(\JOYAS\JoyasBundle\Entity\Iva $ivas) {
        $this->ivas[] = $ivas;

        return $this;
    }

    /**
     * Remove ivas
     *
     * @param \JOYAS\JoyasBundle\Entity\Iva $ivas
     */
    public function removeIva(\JOYAS\JoyasBundle\Entity\Iva $ivas) {
        $this->ivas->removeElement($ivas);
    }

    /**
     * Get ivas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIvas() {
        return $this->ivas;
    }

    /**
     * Add cobranzasfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura
     * @return Factura
     */
    public function addCobranzasfactura(\JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura) {
        $this->cobranzasfactura[] = $cobranzasfactura;

        return $this;
    }

    /**
     * Remove cobranzasfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura
     */
    public function removeCobranzasfactura(\JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura) {
        $this->cobranzasfactura->removeElement($cobranzasfactura);
    }

    /**
     * Get cobranzasfactura
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCobranzasfactura() {
        return $this->cobranzasfactura;
    }

    /**
     * @return mixed
     */
    public function getMesanio() {
        return $this->mesanio;
    }

    /**
     * @param mixed $mesanio
     */
    public function setMesanio($mesanio) {
        $this->mesanio = $mesanio;
    }

    /**
     * Set localidademision
     *
     * @param string $localidademision
     * @return Factura
     */
    public function setLocalidademision($localidademision) {
        $this->localidademision = $localidademision;

        return $this;
    }

    /**
     * Get localidademision
     *
     * @return string 
     */
    public function getLocalidademision() {
        return $this->localidademision;
    }

    /**
     * Set bonificacion
     *
     * @param float $bonificacion
     * @return Factura
     */
    public function setBonificacion($bonificacion) {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    /**
     * Get bonificacion
     *
     * @return float 
     */
    public function getBonificacion() {
        return $this->bonificacion;
    }

    /**
     * Set percepcionib
     *
     * @param float $percepcionib
     * @return Factura
     */
    public function setPercepcionib($percepcionib) {
        $this->percepcionib = $percepcionib;

        return $this;
    }

    /**
     * Get percepcionib
     *
     * @return float 
     */
    public function getPercepcionib() {
        return $this->percepcionib;
    }

    /**
     * Set percepcioniva
     *
     * @param float $percepcioniva
     * @return Factura
     */
    public function setPercepcioniva($percepcioniva) {
        $this->percepcioniva = $percepcioniva;

        return $this;
    }

    /**
     * Get percepcioniva
     *
     * @return float 
     */
    public function getPercepcioniva() {
        return $this->percepcioniva;
    }

    /**
     * Add pagosfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura
     * @return Factura
     */
    public function addPagosfactura(\JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura) {
        $this->pagosfactura[] = $pagosfactura;

        return $this;
    }

    /**
     * Remove pagosfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura
     */
    public function removePagosfactura(\JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura) {
        $this->pagosfactura->removeElement($pagosfactura);
    }

    /**
     * Get pagosfactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosfactura() {
        return $this->pagosfactura;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento() {
        return $this->descuento;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Factura
     */
    public function setDescuento($descuento) {
        $this->descuento = $descuento;

        return $this;
    }

}
