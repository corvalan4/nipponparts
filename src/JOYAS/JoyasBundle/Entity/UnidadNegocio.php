<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\UnidadNegocioRepository")
 * @ORM\Table(name="unidadnegocio")
 */
class UnidadNegocio{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $responsable;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $direccion;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $celular;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $punto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $iibb;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $mail;

	/**
	* @ORM\OneToMany(targetEntity="Stock", mappedBy="unidadNegocio")
	*/
	protected $stocks;

	/**
	* @ORM\OneToMany(targetEntity="Usuario", mappedBy="unidadNegocio")
	*/
	protected $usuarios;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     *
     **********************************/
	public function __construct()
	{
		$this->stocks = new ArrayCollection();
		$this->usuarios = new ArrayCollection();
	}


	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/
	 public function __toString()
	{
		return $this->getDescripcion();
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return UnidadNegocio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     * @return UnidadNegocio
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return UnidadNegocio
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return UnidadNegocio
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return UnidadNegocio
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    public function setPunto($punto)
    {
        $this->punto = $punto;

        return $this;
    }

    public function getPunto()
    {
        return $this->punto;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return UnidadNegocio
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return UnidadNegocio
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add stocks
     *
     * @param \JOYAS\JoyasBundle\Entity\Stock $stocks
     * @return UnidadNegocio
     */
    public function addStock(\JOYAS\JoyasBundle\Entity\Stock $stocks)
    {
        $this->stocks[] = $stocks;

        return $this;
    }

    /**
     * Remove stocks
     *
     * @param \JOYAS\JoyasBundle\Entity\Stock $stocks
     */
    public function removeStock(\JOYAS\JoyasBundle\Entity\Stock $stocks)
    {
        $this->stocks->removeElement($stocks);
    }

    /**
     * Get stocks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStocks()
    {
        return $this->stocks;
    }

    /**
     * Add usuarios
     *
     * @param \JOYAS\JoyasBundle\Entity\Usuario $usuarios
     * @return UnidadNegocio
     */
    public function addUsuario(\JOYAS\JoyasBundle\Entity\Usuario $usuarios)
    {
        $this->usuarios[] = $usuarios;

        return $this;
    }

    /**
     * Remove usuarios
     *
     * @param \JOYAS\JoyasBundle\Entity\Usuario $usuarios
     */
    public function removeUsuario(\JOYAS\JoyasBundle\Entity\Usuario $usuarios)
    {
        $this->usuarios->removeElement($usuarios);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    public function setCuit($cuit)
    {
        $this->cuit = $cuit;

        return $this;
    }

    public function getCuit()
    {
        return $this->cuit;
    }

    public function setIibb($iibb)
    {
        $this->iibb = $iibb;

        return $this;
    }

    /**
     * Get iibb
     *
     * @return string 
     */
    public function getIibb()
    {
        return $this->iibb;
    }
}