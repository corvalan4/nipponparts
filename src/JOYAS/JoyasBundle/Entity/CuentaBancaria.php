<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JOYAS\JoyasBundle\Entity\TipoCosto;
use JOYAS\JoyasBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\CuentaBancariaRepository")
 * @ORM\Table(name="cuentabancaria")
 */
class CuentaBancaria{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $nrocuenta;

	/**
	* @ORM\ManyToOne(targetEntity="Banco", inversedBy="cuentasbancarias")
	* @ORM\JoinColumn(name="banco_id", referencedColumnName="id", nullable=true)
	*/
    protected $banco;

	/**
	* @ORM\OneToMany(targetEntity="MovimientoBancario", mappedBy="cuentabancaria", cascade={"persist"})
	*/
    private $movimientos;
		
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
		$this->movimientos = new ArrayCollection();
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return $this->getDescripcion();	
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Banco
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set nrocuenta
     *
     * @param string $nrocuenta
     * @return CuentaBancaria
     */
    public function setNrocuenta($nrocuenta)
    {
        $this->nrocuenta = $nrocuenta;
    
        return $this;
    }

    /**
     * Get nrocuenta
     *
     * @return string 
     */
    public function getNrocuenta()
    {
        return $this->nrocuenta;
    }

    /**
     * Set banco
     *
     * @param \JOYAS\JoyasBundle\Entity\banco $banco
     * @return CuentaBancaria
     */
    public function setBanco(\JOYAS\JoyasBundle\Entity\banco $banco = null)
    {
        $this->banco = $banco;
    
        return $this;
    }

    /**
     * Get banco
     *
     * @return \JOYAS\JoyasBundle\Entity\banco 
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * Set movimiento
     *
     * @param \JOYAS\JoyasBundle\Entity\MovimientoBancario $movimiento
     * @return CuentaBancaria
     */
    public function setMovimiento(\JOYAS\JoyasBundle\Entity\MovimientoBancario $movimiento = null)
    {
        $this->movimiento = $movimiento;
    
        return $this;
    }

    /**
     * Get movimiento
     *
     * @return \JOYAS\JoyasBundle\Entity\MovimientoBancario 
     */
    public function getMovimiento()
    {
        return $this->movimiento;
    }

    /**
     * Add movimientos
     *
     * @param \JOYAS\JoyasBundle\Entity\MovimientoBancario $movimientos
     * @return CuentaBancaria
     */
    public function addMovimiento(\JOYAS\JoyasBundle\Entity\MovimientoBancario $movimientos)
    {
        $this->movimientos[] = $movimientos;
    
        return $this;
    }

    /**
     * Remove movimientos
     *
     * @param \JOYAS\JoyasBundle\Entity\MovimientoBancario $movimientos
     */
    public function removeMovimiento(\JOYAS\JoyasBundle\Entity\MovimientoBancario $movimientos)
    {
        $this->movimientos->removeElement($movimientos);
    }

    /**
     * Get movimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientos()
    {
        return $this->movimientos;
    }
}