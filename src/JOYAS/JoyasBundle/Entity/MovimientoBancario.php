<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\MovimientoBancarioRepository")
 * @ORM\Table(name="movimientobancario")
 */
class MovimientoBancario{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Cheque")
     * @ORM\JoinColumn(name="cheque_id", referencedColumnName="id", nullable=true)
     **/
    private $cheque;

	/**
	* @ORM\ManyToOne(targetEntity="CuentaBancaria", inversedBy="movimientoscc")
	* @ORM\JoinColumn(name="cuentabancaria_id", referencedColumnName="id", nullable=true)
	*/
    private $cuentabancaria;
		
	/**
	* @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="movimientoscc")
	* @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
	*/
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $tipoMovimiento;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $valor;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
		$this->fecha = new \DateTime('NOW');
	}
		

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoMovimiento
     *
     * @param string $tipoMovimiento
     * @return MovimientoBancario
     */
    public function setTipoMovimiento($tipoMovimiento)
    {
        $this->tipoMovimiento = $tipoMovimiento;
    
        return $this;
    }

    /**
     * Get tipoMovimiento
     *
     * @return string 
     */
    public function getTipoMovimiento()
    {
        return $this->tipoMovimiento;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return MovimientoBancario
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    
        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return MovimientoBancario
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return MovimientoBancario
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set cheque
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheque
     * @return MovimientoBancario
     */
    public function setCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheque = null)
    {
        $this->cheque = $cheque;
    
        return $this;
    }

    /**
     * Get cheque
     *
     * @return \JOYAS\JoyasBundle\Entity\Cheque 
     */
    public function getCheque()
    {
        return $this->cheque;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return MovimientoBancario
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null)
    {
        $this->unidadNegocio = $unidadNegocio;
    
        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio()
    {
        return $this->unidadNegocio;
    }

    /**
     * Set CuentaBancaria
     *
     * @param \JOYAS\JoyasBundle\Entity\CuentaBancaria $cuentabancaria
     * @return MovimientoBancario
     */
    public function setCuentabancaria(\JOYAS\JoyasBundle\Entity\CuentaBancaria $cuentabancaria = null)
    {
        $this->cuentabancaria = $cuentabancaria;
    
        return $this;
    }

    /**
     * Get cuentabancaria
     *
     * @return \JOYAS\JoyasBundle\Entity\CuentaBancaria 
     */
    public function getCuentabancaria()
    {
        return $this->cuentabancaria;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return MovimientoBancario
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    
        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }
}