<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\MedioDocumentoRepository")
 * @ORM\Table(name="mediodocumento")
 */
class MedioDocumento{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	* @ORM\ManyToOne(targetEntity="Cobranza", inversedBy="medios")
	* @ORM\JoinColumn(name="cobranza_id", referencedColumnName="id")
	*/
    protected $cobranza;

	/**
	* @ORM\ManyToOne(targetEntity="Pago", inversedBy="medios")
	* @ORM\JoinColumn(name="pago_id", referencedColumnName="id")
	*/
    protected $pago;
	/**
	* @ORM\ManyToOne(targetEntity="Gasto", inversedBy="medios")
	* @ORM\JoinColumn(name="gasto_id", referencedColumnName="id")
	*/
    protected $gasto;

	/**
	* @ORM\ManyToOne(targetEntity="Medio", inversedBy="medioDocumentos")
	* @ORM\JoinColumn(name="medio_id", referencedColumnName="id")
	*/
    protected $medio;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $numerolote;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $numerocupon; // Sirve como numero de transacion para debito y nro de tramite para MP ademas de TC.

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $cuotas;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return $this->getDescripcion();	
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return TipoRetencion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set importe
     *
     * @param float $valor
     * @return MedioDocumento
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    
        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set cobranza
     *
     * @param \JOYAS\JoyasBundle\Entity\Cobranza $cobranza
     * @return MedioDocumento
     */
    public function setCobranza(\JOYAS\JoyasBundle\Entity\Cobranza $cobranza = null)
    {
        $this->cobranza = $cobranza;
    
        return $this;
    }

    /**
     * Get cobranza
     *
     * @return \JOYAS\JoyasBundle\Entity\Cobranza 
     */
    public function getCobranza()
    {
        return $this->cobranza;
    }

    /**
     * Set pago
     *
     * @param \JOYAS\JoyasBundle\Entity\Pago $pago
     * @return MedioDocumento
     */
    public function setPago(\JOYAS\JoyasBundle\Entity\Pago $pago = null)
    {
        $this->pago = $pago;
    
        return $this;
    }

    /**
     * Get pago
     *
     * @return \JOYAS\JoyasBundle\Entity\Pago 
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Set medio
     *
     * @param \JOYAS\JoyasBundle\Entity\Medio $medio
     * @return MedioDocumento
     */
    public function setMedio(\JOYAS\JoyasBundle\Entity\Medio $medio = null)
    {
        $this->medio = $medio;
    
        return $this;
    }

    /**
     * Get medio
     *
     * @return \JOYAS\JoyasBundle\Entity\Medio 
     */
    public function getMedio()
    {
        return $this->medio;
    }

    /**
     * Set gasto
     *
     * @param \JOYAS\JoyasBundle\Entity\Gasto $gasto
     * @return MedioDocumento
     */
    public function setGasto(\JOYAS\JoyasBundle\Entity\Gasto $gasto = null)
    {
        $this->gasto = $gasto;
    
        return $this;
    }

    /**
     * Get gasto
     *
     * @return \JOYAS\JoyasBundle\Entity\Gasto 
     */
    public function getGasto()
    {
        return $this->gasto;
    }

    /**
     * Set numerolote
     *
     * @param integer $numerolote
     * @return MedioDocumento
     */
    public function setNumerolote($numerolote)
    {
        $this->numerolote = $numerolote;
    
        return $this;
    }

    /**
     * Get numerolote
     *
     * @return integer 
     */
    public function getNumerolote()
    {
        return $this->numerolote;
    }

    /**
     * Set numerocupon
     *
     * @param integer $numerocupon
     * @return MedioDocumento
     */
    public function setNumerocupon($numerocupon)
    {
        $this->numerocupon = $numerocupon;
    
        return $this;
    }

    /**
     * Get numerocupon
     *
     * @return integer 
     */
    public function getNumerocupon()
    {
        return $this->numerocupon;
    }

    /**
     * Set cuotas
     *
     * @param integer $cuotas
     * @return MedioDocumento
     */
    public function setCuotas($cuotas)
    {
        $this->cuotas = $cuotas;
    
        return $this;
    }

    /**
     * Get cuotas
     *
     * @return integer 
     */
    public function getCuotas()
    {
        return $this->cuotas;
    }
}