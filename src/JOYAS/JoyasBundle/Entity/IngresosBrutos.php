<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\IngresosBrutosRepository")
 * @ORM\Table(name="ingresosbrutos")
 */
class IngresosBrutos{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $valor;

	/**
	* @ORM\ManyToOne(targetEntity="NotaCreditoDebito", inversedBy="ingresosbrutos")
	* @ORM\JoinColumn(name="notacreditodebito_id", referencedColumnName="id", nullable=true)
	*/
    protected $notaCreditoDebito;

	/**
	* @ORM\ManyToOne(targetEntity="TipoIngresosBrutos", inversedBy="ingresosbrutos")
	* @ORM\JoinColumn(name="tipoingresosbrutos_id", referencedColumnName="id", nullable=true)
	*/
    protected $tipoIngresosBrutos;
	/**
	* @ORM\ManyToOne(targetEntity="Gasto", inversedBy="ingresosbrutos")
	* @ORM\JoinColumn(name="gasto_id", referencedColumnName="id", nullable=true)
	*/
    protected $gasto;
	
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return $this->getValor();	
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return IngresosBrutos
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    
        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return IngresosBrutos
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set notaCreditoDebito
     *
     * @param \JOYAS\JoyasBundle\Entity\NotaCreditoDebito $notaCreditoDebito
     * @return IngresosBrutos
     */
    public function setNotaCreditoDebito(\JOYAS\JoyasBundle\Entity\NotaCreditoDebito $notaCreditoDebito = null)
    {
        $this->notaCreditoDebito = $notaCreditoDebito;
    
        return $this;
    }

    /**
     * Get notaCreditoDebito
     *
     * @return \JOYAS\JoyasBundle\Entity\NotaCreditoDebito 
     */
    public function getNotaCreditoDebito()
    {
        return $this->notaCreditoDebito;
    }

    /**
     * Set tipoIngresosBrutos
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoIngresosBrutos $tipoIngresosBrutos
     * @return IngresosBrutos
     */
    public function setTipoIngresosBrutos(\JOYAS\JoyasBundle\Entity\TipoIngresosBrutos $tipoIngresosBrutos = null)
    {
        $this->tipoIngresosBrutos = $tipoIngresosBrutos;
    
        return $this;
    }

    /**
     * Get tipoIngresosBrutos
     *
     * @return \JOYAS\JoyasBundle\Entity\TipoIngresosBrutos 
     */
    public function getTipoIngresosBrutos()
    {
        return $this->tipoIngresosBrutos;
    }

    /**
     * Set gasto
     *
     * @param \JOYAS\JoyasBundle\Entity\Gasto $gasto
     * @return IngresosBrutos
     */
    public function setGasto(\JOYAS\JoyasBundle\Entity\Gasto $gasto = null)
    {
        $this->gasto = $gasto;
    
        return $this;
    }

    /**
     * Get gasto
     *
     * @return \JOYAS\JoyasBundle\Entity\Gasto 
     */
    public function getGasto()
    {
        return $this->gasto;
    }
}