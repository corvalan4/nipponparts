<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\StockRepository")
 * @ORM\Table(name="stock")
 */
class Stock {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="stock")
     */
    protected $productosFactura;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="stocks")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="stocks")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadnegocio;

    /**
     * @ORM\ManyToOne(targetEntity="Cotizacion", inversedBy="stocks")
     * @ORM\JoinColumn(name="cotizacion_id", referencedColumnName="id")
     */
    protected $cotizacion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $stock;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $costo;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosFactura = new ArrayCollection();
        $this->fecha = new \DateTime('NOW');
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getUnidadnegocio() . " - Cantidad: " . $this->getStock();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Stock
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Stock
     */
    public function setStock($stock) {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock() {
        return $this->stock;
    }

    /**
     * Set costo
     *
     * @param float $costo
     * @return Stock
     */
    public function setCosto($costo) {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return float 
     */
    public function getCosto() {
        return $this->costo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Stock
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     * @return Stock
     */
    public function addProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura[] = $productosFactura;

        return $this;
    }

    /**
     * Remove productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     */
    public function removeProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura->removeElement($productosFactura);
    }

    /**
     * Get productosFactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosFactura() {
        return $this->productosFactura;
    }

    /**
     * Set producto
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $producto
     * @return Stock
     */
    public function setProducto(\JOYAS\JoyasBundle\Entity\Producto $producto = null) {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \JOYAS\JoyasBundle\Entity\Producto 
     */
    public function getProducto() {
        return $this->producto;
    }

    /**
     * Set cotizacion
     *
     * @param \JOYAS\JoyasBundle\Entity\Cotizacion $cotizacion
     * @return Stock
     */
    public function setCotizacion(\JOYAS\JoyasBundle\Entity\Cotizacion $cotizacion = null) {
        $this->cotizacion = $cotizacion;

        return $this;
    }

    /**
     * Get cotizacion
     *
     * @return \JOYAS\JoyasBundle\Entity\Cotizacion 
     */
    public function getCotizacion() {
        return $this->cotizacion;
    }

    /**
     * Set UnidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\unidadnegocio $unidadnegocio
     * @return Stock
     */
    public function setUnidadnegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadnegocio = null) {
        $this->unidadnegocio = $unidadnegocio;

        return $this;
    }

    /**
     * Get UnidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadnegocio() {
        return $this->unidadnegocio;
    }

}
