<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\NotaCreditoDebitoRepository")
 * @ORM\Table(name="notacreditodebito")
 */
class NotaCreditoDebito{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
    /**
	* @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="notasCreditoDebito")
	* @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
	*/
	protected $unidadnegocio;
	
    /**
	* @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="notasCreditoDebito")
	* @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id", nullable=true)
	*/
	protected $clienteProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal", inversedBy="facturas")
     * @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
     */
    protected $sucursal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nrocomprobante;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tiponota; // DEBITO O CREDITO

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $codigonota = "B"; // A, B, C

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cae;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $ptovta;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechavtocae;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    protected $descripcion;

	/**
	* @ORM\OneToMany(targetEntity="Iva", mappedBy="notaCreditoDebito", cascade={"persist"})
	*/
	protected $ivas;

	/**
	* @ORM\OneToMany(targetEntity="IngresosBrutos", mappedBy="notaCreditoDebito", cascade={"persist"})
	*/
	protected $ingresosbrutos;

	/**
	* @ORM\OneToMany(targetEntity="CobranzaFactura", mappedBy="notaCreditoDebito", cascade={"persist"})
	*/
	protected $cobranzasfactura;

    /**
     * @ORM\OneToMany(targetEntity="PagoFactura", mappedBy="factura")
     */
    protected $pagosfactura;

    /**
     * @ORM\OneToMany(targetEntity="ItemNota", mappedBy="notadebitocredito", cascade={"persist", "remove"} )
     */
    protected $itemsNota;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
		$this->ivas = new ArrayCollection();
		$this->ingresosbrutos = new ArrayCollection();
		$this->itemsNota = new ArrayCollection();
		$this->pagosasignados = new ArrayCollection();
	}
		

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	    return "NOTA ".$this->tiponota." ".$this->codigonota." - NRO: ".$this->nrocomprobante;
	}		


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return NotaCreditoDebito
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set importe
     *
     * @param string $importe
     * @return NotaCreditoDebito
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    
        return $this;
    }

    /**
     * Get importe
     *
     * @return string 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set nrocomprobante
     *
     * @param string $nrocomprobante
     * @return NotaCreditoDebito
     */
    public function setNrocomprobante($nrocomprobante)
    {
        $this->nrocomprobante = $nrocomprobante;
    
        return $this;
    }

    /**
     * Get nrocomprobante
     *
     * @return string 
     */
    public function getNrocomprobante()
    {
        return $this->nrocomprobante;
    }

    /**
     * Set cae
     *
     * @param string $cae
     * @return NotaCreditoDebito
     */
    public function setCae($cae)
    {
        $this->cae = $cae;
    
        return $this;
    }

    /**
     * Get cae
     *
     * @return string 
     */
    public function getCae()
    {
        return $this->cae;
    }

    /**
     * Set ptovta
     *
     * @param integer $ptovta
     * @return NotaCreditoDebito
     */
    public function setPtovta($ptovta)
    {
        $this->ptovta = $ptovta;
    
        return $this;
    }

    /**
     * Get ptovta
     *
     * @return integer 
     */
    public function getPtovta()
    {
        return $this->ptovta;
    }

    /**
     * Set fechavtocae
     *
     * @param string $fechavtocae
     * @return NotaCreditoDebito
     */
    public function setFechavtocae($fechavtocae)
    {
        $this->fechavtocae = $fechavtocae;
    
        return $this;
    }

    /**
     * Get fechavtocae
     *
     * @return string 
     */
    public function getFechavtocae()
    {
        return $this->fechavtocae;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return NotaCreditoDebito
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return NotaCreditoDebito
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set unidadnegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadnegocio
     * @return NotaCreditoDebito
     */
    public function setUnidadnegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadnegocio = null)
    {
        $this->unidadnegocio = $unidadnegocio;
    
        return $this;
    }

    /**
     * Get unidadnegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadnegocio()
    {
        return $this->unidadnegocio;
    }

    /**
     * Set clienteProveedor
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor
     * @return NotaCreditoDebito
     */
    public function setClienteProveedor(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor = null)
    {
        $this->clienteProveedor = $clienteProveedor;
    
        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \JOYAS\JoyasBundle\Entity\ClienteProveedor 
     */
    public function getClienteProveedor()
    {
        return $this->clienteProveedor;
    }

    /**
     * Add ivas
     *
     * @param \JOYAS\JoyasBundle\Entity\Iva $ivas
     * @return NotaCreditoDebito
     */
    public function addIva(\JOYAS\JoyasBundle\Entity\Iva $ivas)
    {
        $this->ivas[] = $ivas;
    
        return $this;
    }

    /**
     * Remove ivas
     *
     * @param \JOYAS\JoyasBundle\Entity\Iva $ivas
     */
    public function removeIva(\JOYAS\JoyasBundle\Entity\Iva $ivas)
    {
        $this->ivas->removeElement($ivas);
    }

    /**
     * Get ivas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIvas()
    {
        return $this->ivas;
    }

    /**
     * Add ingresosbrutos
     *
     * @param \JOYAS\JoyasBundle\Entity\IngresosBrutos $ingresosbrutos
     * @return NotaCreditoDebito
     */
    public function addIngresosbruto(\JOYAS\JoyasBundle\Entity\IngresosBrutos $ingresosbrutos)
    {
        $this->ingresosbrutos[] = $ingresosbrutos;
    
        return $this;
    }

    /**
     * Remove ingresosbrutos
     *
     * @param \JOYAS\JoyasBundle\Entity\IngresosBrutos $ingresosbrutos
     */
    public function removeIngresosbruto(\JOYAS\JoyasBundle\Entity\IngresosBrutos $ingresosbrutos)
    {
        $this->ingresosbrutos->removeElement($ingresosbrutos);
    }

    /**
     * Get ingresosbrutos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIngresosbrutos()
    {
        return $this->ingresosbrutos;
    }

    /**
     * Add cobranzasfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura
     * @return NotaCreditoDebito
     */
    public function addCobranzasfactura(\JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura)
    {
        $this->cobranzasfactura[] = $cobranzasfactura;
    
        return $this;
    }

    /**
     * Remove cobranzasfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura
     */
    public function removeCobranzasfactura(\JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura)
    {
        $this->cobranzasfactura->removeElement($cobranzasfactura);
    }

    /**
     * Get cobranzasfactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCobranzasfactura()
    {
        return $this->cobranzasfactura;
    }

    /**
     * Set tiponota
     *
     * @param string $tiponota
     * @return NotaCreditoDebito
     */
    public function setTiponota($tiponota)
    {
        $this->tiponota = $tiponota;
    
        return $this;
    }

    /**
     * Get tiponota
     *
     * @return string 
     */
    public function getTiponota()
    {
        return $this->tiponota;
    }

    /**
     * Set codigonota
     *
     * @param string $codigonota
     * @return NotaCreditoDebito
     */
    public function setCodigonota($codigonota)
    {
        $this->codigonota = $codigonota;
    
        return $this;
    }

    /**
     * Get codigonota
     *
     * @return string 
     */
    public function getCodigonota()
    {
        return $this->codigonota;
    }

    /**
     * Set sucursal
     *
     * @param \JOYAS\JoyasBundle\Entity\Sucursal $sucursal
     * @return NotaCreditoDebito
     */
    public function setSucursal(\JOYAS\JoyasBundle\Entity\Sucursal $sucursal = null)
    {
        $this->sucursal = $sucursal;
    
        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \JOYAS\JoyasBundle\Entity\Sucursal 
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }

    /**
     * Add pagosfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura
     * @return NotaCreditoDebito
     */
    public function addPagosfactura(\JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura)
    {
        $this->pagosfactura[] = $pagosfactura;
    
        return $this;
    }

    /**
     * Remove pagosfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura
     */
    public function removePagosfactura(\JOYAS\JoyasBundle\Entity\PagoFactura $pagosfactura)
    {
        $this->pagosfactura->removeElement($pagosfactura);
    }

    /**
     * Get pagosfactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosfactura()
    {
        return $this->pagosfactura;
    }

    /**
     * Add itemsNota
     *
     * @param \JOYAS\JoyasBundle\Entity\ItemNota $itemsNota
     * @return NotaCreditoDebito
     */
    public function addItemsNota(\JOYAS\JoyasBundle\Entity\ItemNota $itemsNota)
    {
        $this->itemsNota[] = $itemsNota;
    
        return $this;
    }

    /**
     * Remove itemsNota
     *
     * @param \JOYAS\JoyasBundle\Entity\ItemNota $itemsNota
     */
    public function removeItemsNota(\JOYAS\JoyasBundle\Entity\ItemNota $itemsNota)
    {
        $this->itemsNota->removeElement($itemsNota);
    }

    /**
     * Get itemsNota
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItemsNota()
    {
        return $this->itemsNota;
    }
}
