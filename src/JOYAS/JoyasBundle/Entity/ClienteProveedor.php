<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ClienteProveedorRepository")
 * @ORM\Table(name="clienteproveedor")
 */
class ClienteProveedor{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $idanterior;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $razonSocial;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $denominacion;

    /**
     * @ORM\Column(type="string",  nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $dni;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $domiciliocomercial;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $telefono;
	
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $celular;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $saldo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $coeficientePrecio;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $porcentajeiva;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $mail;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $fax;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $clienteProveedor='1';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $provincia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $localidad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codigopostal;

	/**
	* @ORM\OneToMany(targetEntity="Sucursal", mappedBy="clienteproveedor")
	*/
	protected $sucursales;

	/**
	* @ORM\OneToMany(targetEntity="PagoAsignacion", mappedBy="clienteproveedor")
	*/
	protected $pagosAsignacion;

	/**
	* @ORM\OneToMany(targetEntity="Cobranza", mappedBy="clienteproveedor")
	*/
	protected $cobranzas;

	/**
	* @ORM\ManyToOne(targetEntity="CondicionIva", inversedBy="clientesproveedores")
	* @ORM\JoinColumn(name="condicioniva_id", referencedColumnName="id")
	*/
    protected $condicioniva;

	/**
	* @ORM\ManyToOne(targetEntity="Localidad", inversedBy="clientesproveedores")
	* @ORM\JoinColumn(name="localidad_id", referencedColumnName="id")
    protected $localidad;
	*/

	/**
	* @ORM\ManyToOne(targetEntity="TipoProveedor", inversedBy="clientesproveedores")
	* @ORM\JoinColumn(name="tipoproveedor_id", referencedColumnName="id")
	*/
    protected $tipoProveedor;
	
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
		$this->cobranzas = new ArrayCollection();
		$this->sucursales = new ArrayCollection();
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return $this->getRazonSocial()."";
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     * @return ClienteProveedor
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;
    
        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string 
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set denominacion
     *
     * @param string $denominacion
     * @return ClienteProveedor
     */
    public function setDenominacion($denominacion)
    {
        $this->denominacion = $denominacion;
    
        return $this;
    }

    /**
     * Get denominacion
     *
     * @return string 
     */
    public function getDenominacion()
    {
        return $this->denominacion;
    }

    /**
     * Set cuit
     *
     * @param integer $cuit
     * @return ClienteProveedor
     */
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;
    
        return $this;
    }

    /**
     * Get cuit
     *
     * @return integer
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return ClienteProveedor
     */
    public function setDni($dni)
    {
        $this->dni = $dni;
    
        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set domiciliocomercial
     *
     * @param string $domiciliocomercial
     * @return ClienteProveedor
     */
    public function setDomiciliocomercial($domiciliocomercial)
    {
        $this->domiciliocomercial = $domiciliocomercial;
    
        return $this;
    }

    /**
     * Get domiciliocomercial
     *
     * @return string 
     */
    public function getDomiciliocomercial()
    {
        return $this->domiciliocomercial;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return ClienteProveedor
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return ClienteProveedor
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    
        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set saldo
     *
     * @param float $saldo
     * @return ClienteProveedor
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    
        return $this;
    }

    /**
     * Get saldo
     *
     * @return float 
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * Set coeficientePrecio
     *
     * @param float $coeficientePrecio
     * @return ClienteProveedor
     */
    public function setCoeficientePrecio($coeficientePrecio)
    {
        $this->coeficientePrecio = $coeficientePrecio;
    
        return $this;
    }

    /**
     * Get coeficientePrecio
     *
     * @return float 
     */
    public function getCoeficientePrecio()
    {
        return $this->coeficientePrecio;
    }

    /**
     * Set porcentajeiva
     *
     * @param float $porcentajeiva
     * @return ClienteProveedor
     */
    public function setPorcentajeiva($porcentajeiva)
    {
        $this->porcentajeiva = $porcentajeiva;
    
        return $this;
    }

    /**
     * Get porcentajeiva
     *
     * @return float 
     */
    public function getPorcentajeiva()
    {
        return $this->porcentajeiva;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return ClienteProveedor
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    
        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return ClienteProveedor
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    
        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set clienteProveedor
     *
     * @param string $clienteProveedor
     * @return ClienteProveedor
     */
    public function setClienteProveedor($clienteProveedor)
    {
        $this->clienteProveedor = $clienteProveedor;
    
        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return string 
     */
    public function getClienteProveedor()
    {
        return $this->clienteProveedor;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ClienteProveedor
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add sucursales
     *
     * @param \JOYAS\JoyasBundle\Entity\Sucursal $sucursales
     * @return ClienteProveedor
     */
    public function addSucursale(\JOYAS\JoyasBundle\Entity\Sucursal $sucursales)
    {
        $this->sucursales[] = $sucursales;
    
        return $this;
    }

    /**
     * Remove sucursales
     *
     * @param \JOYAS\JoyasBundle\Entity\Sucursal $sucursales
     */
    public function removeSucursale(\JOYAS\JoyasBundle\Entity\Sucursal $sucursales)
    {
        $this->sucursales->removeElement($sucursales);
    }

    /**
     * Get sucursales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSucursales()
    {
        return $this->sucursales;
    }

    /**
     * Add cobranzas
     *
     * @param \JOYAS\JoyasBundle\Entity\Cobranza $cobranzas
     * @return ClienteProveedor
     */
    public function addCobranza(\JOYAS\JoyasBundle\Entity\Cobranza $cobranzas)
    {
        $this->cobranzas[] = $cobranzas;
    
        return $this;
    }

    /**
     * Remove cobranzas
     *
     * @param \JOYAS\JoyasBundle\Entity\Cobranza $cobranzas
     */
    public function removeCobranza(\JOYAS\JoyasBundle\Entity\Cobranza $cobranzas)
    {
        $this->cobranzas->removeElement($cobranzas);
    }

    /**
     * Get cobranzas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCobranzas()
    {
        return $this->cobranzas;
    }

    /**
     * Set condicioniva
     *
     * @param \JOYAS\JoyasBundle\Entity\CondicionIva $condicioniva
     * @return ClienteProveedor
     */
    public function setCondicioniva(\JOYAS\JoyasBundle\Entity\CondicionIva $condicioniva = null)
    {
        $this->condicioniva = $condicioniva;
    
        return $this;
    }

    /**
     * Get condicioniva
     *
     * @return \JOYAS\JoyasBundle\Entity\CondicionIva 
     */
    public function getCondicioniva()
    {
        return $this->condicioniva;
    }

    /**
     * Set localidad
     *
     * @param \JOYAS\JoyasBundle\Entity\Localidad $localidad
     * @return ClienteProveedor
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;
    
        return $this;
    }

    /**
     * Get localidad
     *
     * @return \JOYAS\JoyasBundle\Entity\Localidad 
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set tipoProveedor
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoProveedor $tipoProveedor
     * @return ClienteProveedor
     */
    public function setTipoProveedor(\JOYAS\JoyasBundle\Entity\TipoProveedor $tipoProveedor = null)
    {
        $this->tipoProveedor = $tipoProveedor;
    
        return $this;
    }

    /**
     * Get tipoProveedor
     *
     * @return \JOYAS\JoyasBundle\Entity\TipoProveedor 
     */
    public function getTipoProveedor()
    {
        return $this->tipoProveedor;
    }

    /**
     * Add pagosAsignacion
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoAsignacion $pagosAsignacion
     * @return ClienteProveedor
     */
    public function addPagosAsignacion(\JOYAS\JoyasBundle\Entity\PagoAsignacion $pagosAsignacion)
    {
        $this->pagosAsignacion[] = $pagosAsignacion;
    
        return $this;
    }

    /**
     * Remove pagosAsignacion
     *
     * @param \JOYAS\JoyasBundle\Entity\PagoAsignacion $pagosAsignacion
     */
    public function removePagosAsignacion(\JOYAS\JoyasBundle\Entity\PagoAsignacion $pagosAsignacion)
    {
        $this->pagosAsignacion->removeElement($pagosAsignacion);
    }

    /**
     * Get pagosAsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosAsignacion()
    {
        return $this->pagosAsignacion;
    }

    /**
     * @return mixed
     */
    public function getIdanterior()
    {
        return $this->idanterior;
    }

    /**
     * @param mixed $idanterior
     */
    public function setIdanterior($idanterior)
    {
        $this->idanterior = $idanterior;
    }


    /**
     * Set provincia
     *
     * @param string $provincia
     * @return ClienteProveedor
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;
    
        return $this;
    }

    /**
     * Get provincia
     *
     * @return string 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set codigopostal
     *
     * @param string $codigopostal
     * @return ClienteProveedor
     */
    public function setCodigopostal($codigopostal)
    {
        $this->codigopostal = $codigopostal;
    
        return $this;
    }

    /**
     * Get codigopostal
     *
     * @return string 
     */
    public function getCodigopostal()
    {
        return $this->codigopostal;
    }
}