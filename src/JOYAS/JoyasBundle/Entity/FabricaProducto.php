<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\FabricaProductoRepository")
 * @ORM\Table(name="fabricaproducto")
 */
class FabricaProducto{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	* @ORM\ManyToOne(targetEntity="Producto", inversedBy="fabricaProductos")
	* @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
	*/
    protected $producto;

	/**
	* @ORM\ManyToOne(targetEntity="Fabrica", inversedBy="fabricaProductos")
	* @ORM\JoinColumn(name="fabrica_id", referencedColumnName="id")
	*/
    protected $fabrica;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return FabricaProducto
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set producto
     *
     * @param \JOYAS\JoyasBundle\Entity\Producto $producto
     * @return FabricaProducto
     */
    public function setProducto(\JOYAS\JoyasBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;
    
        return $this;
    }

    /**
     * Get producto
     *
     * @return \JOYAS\JoyasBundle\Entity\Producto 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set fabrica
     *
     * @param \JOYAS\JoyasBundle\Entity\Fabrica $fabrica
     * @return FabricaProducto
     */
    public function setFabrica(\JOYAS\JoyasBundle\Entity\Fabrica $fabrica = null)
    {
        $this->fabrica = $fabrica;
    
        return $this;
    }

    /**
     * Get fabrica
     *
     * @return \JOYAS\JoyasBundle\Entity\Fabrica 
     */
    public function getFabrica()
    {
        return $this->fabrica;
    }
}