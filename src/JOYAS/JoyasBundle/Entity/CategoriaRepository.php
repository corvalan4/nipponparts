<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CategoriaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoriaRepository extends EntityRepository
{
	public function prodcategorias($id){
		$query = $this->getEntityManager()->createQuery('SELECT e FROM JOYAS\JoyasBundle\Entity\Producto e, JOYAS\JoyasBundle\Entity\Precio pr, JOYAS\JoyasBundle\Entity\ lp WHERE e.id = pr.producto AND pr. = lp.id AND lp.descripcion = :desclista AND e.categoriasubcategoria = :idcatsub AND e.visible = 1 AND e.estado = :activo')->setParameter(':activo','A')->setParameter(':idcatsub',$idcatsub)->setParameter(':desclista','Lista Web');
		return $query->getResult();
	}
}
