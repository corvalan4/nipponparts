<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\PagoAsignacionRepository")
 * @ORM\Table(name="pagoasignacion")
 */
class PagoAsignacion{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

	/**
	* @ORM\ManyToOne(targetEntity="Pago", inversedBy="pagosAsignacion")
	* @ORM\JoinColumn(name="pago_id", referencedColumnName="id", nullable=true)
	*/
    protected $pago;

	/**
	* @ORM\ManyToOne(targetEntity="Gasto", inversedBy="pagosAsignacion")
	* @ORM\JoinColumn(name="gasto_id", referencedColumnName="id", nullable=true)
	*/
    protected $gasto;

	/**
	* @ORM\ManyToOne(targetEntity="Cobranza", inversedBy="pagosAsignacion")
	* @ORM\JoinColumn(name="cobranza_id", referencedColumnName="id", nullable=true)
	*/
    protected $cobranza;

	/**
	* @ORM\ManyToOne(targetEntity="Factura", inversedBy="pagosAsignacion")
	* @ORM\JoinColumn(name="factura_id", referencedColumnName="id", nullable=true)
	*/
    protected $factura;

	/**
	* @ORM\ManyToOne(targetEntity="NotaCreditoDebito", inversedBy="pagosAsignacion")
	* @ORM\JoinColumn(name="notacreditodebito_id", referencedColumnName="id", nullable=true)
	*/
    protected $notaCreditoDebito;

	/**
	* @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="pagosAsignacion")
	* @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id", nullable=true)
	*/
    protected $clienteProveedor;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     *
     **********************************/
	public function __construct()
	{
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/
	 public function __toString()
	{
		return $this->gasto.$this->pago.$this->factura.$this->cobranza.$this->notaCreditoDebito;
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getDocumento()
    {
        if($this->factura){
            $positivo = ($this->factura->getTipo()=="F") ? false : true;
            $tipo = ($this->factura->getTipo()=="FP") ? "facturaproveedor" : "factura";
            return array("entity"=>$this->factura, "tipo"=>$tipo, "positivo"=>$positivo);
        }
        if($this->cobranza){
            return array("entity"=>$this->cobranza, "tipo"=>"cobranza", "positivo"=>true);
        }
        if($this->pago){
            return array("entity"=>$this->pago, "tipo"=>"pago", "positivo"=>false);
        }
        if($this->gasto){
            return array("entity"=>$this->gasto, "tipo"=>"gasto", "positivo"=>false);
        }
        if($this->notaCreditoDebito){
            $positivo = $this->notaCreditoDebito->getTiponota() == 'credito' ? true : false;
            return array("entity"=>$this->notaCreditoDebito, "tipo"=>"notaCreditoDebito", "positivo"=>$positivo);
        }
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return PagoAsignacion
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return PagoAsignacion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set pago
     *
     * @param \JOYAS\JoyasBundle\Entity\Pago $pago
     * @return PagoAsignacion
     */
    public function setPago(\JOYAS\JoyasBundle\Entity\Pago $pago = null)
    {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return \JOYAS\JoyasBundle\Entity\Pago
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Set gasto
     *
     * @param \JOYAS\JoyasBundle\Entity\Gasto $gasto
     * @return PagoAsignacion
     */
    public function setGasto(\JOYAS\JoyasBundle\Entity\Gasto $gasto = null)
    {
        $this->gasto = $gasto;

        return $this;
    }

    /**
     * Get gasto
     *
     * @return \JOYAS\JoyasBundle\Entity\Gasto
     */
    public function getGasto()
    {
        return $this->gasto;
    }

    /**
     * Set factura
     *
     * @param \JOYAS\JoyasBundle\Entity\Factura $factura
     * @return PagoAsignacion
     */
    public function setFactura(\JOYAS\JoyasBundle\Entity\Factura $factura = null)
    {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \JOYAS\JoyasBundle\Entity\Factura
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * Set notaCreditoDebito
     *
     * @param \JOYAS\JoyasBundle\Entity\NotaCreditoDebito $notaCreditoDebito
     * @return PagoAsignacion
     */
    public function setNotaCreditoDebito(\JOYAS\JoyasBundle\Entity\NotaCreditoDebito $notaCreditoDebito = null)
    {
        $this->notaCreditoDebito = $notaCreditoDebito;

        return $this;
    }

    /**
     * Get notaCreditoDebito
     *
     * @return \JOYAS\JoyasBundle\Entity\NotaCreditoDebito
     */
    public function getNotaCreditoDebito()
    {
        return $this->notaCreditoDebito;
    }

    /**
     * Set cobranza
     *
     * @param \JOYAS\JoyasBundle\Entity\Cobranza $cobranza
     * @return PagoAsignacion
     */
    public function setCobranza(\JOYAS\JoyasBundle\Entity\Cobranza $cobranza = null)
    {
        $this->cobranza = $cobranza;
    
        return $this;
    }

    /**
     * Get cobranza
     *
     * @return \JOYAS\JoyasBundle\Entity\Cobranza 
     */
    public function getCobranza()
    {
        return $this->cobranza;
    }

    /**
     * Set clienteProveedor
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Pago
     */
    public function setClienteProveedor(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor = null)
    {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \JOYAS\JoyasBundle\Entity\ClienteProveedor
     */
    public function getClienteProveedor()
    {
        return $this->clienteProveedor;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pago
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
