<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="producto")
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ProductoRepository")
 */
class Producto {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(
     *       message = "Debe agregar una nombre."
     *              )
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codigo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $procedencia;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $precio;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $coeficientePrecio;

    /**
     * @ORM\Column(type="float", nullable=true)
     */    protected $tipoiva = 21;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $modelo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $caracteristica;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codigoOriginal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $nroFabrica;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $nroFabricacion;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $anio;

    /**
     * @ORM\OneToMany(targetEntity="ImagenProducto", mappedBy="producto", cascade={"remove"})
     */
    protected $imagenesproducto;

    /**
     * @ORM\OneToMany(targetEntity="Stock", mappedBy="producto")
     */
    protected $stocks;

    /**
     * @ORM\ManyToOne(targetEntity="Marca", inversedBy="productos")
     * @ORM\JoinColumn(name="marca_id", referencedColumnName="id")
     */
    protected $marca;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $marcarepuesto;

    /**
     * @ORM\ManyToOne(targetEntity="Rubro", inversedBy="productos")
     * @ORM\JoinColumn(name="rubro_id", referencedColumnName="id")
     */
    protected $rubro;

    /**
     * @ORM\ManyToOne(targetEntity="Subcategoria", inversedBy="productos")
     * @ORM\JoinColumn(name="subcategoria_id", referencedColumnName="id")
     */
    protected $subcategoria;

    /**
     * @ORM\ManyToOne(targetEntity="Origen", inversedBy="productos")
     * @ORM\JoinColumn(name="origen_id", referencedColumnName="id")
     */
    protected $origen;

    /**
     * @ORM\ManyToMany(targetEntity="Fabrica", inversedBy="productos")
     * @ORM\JoinTable(name="productos_fabricas")
     */
    protected $fabricas;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="productos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $usuario;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     *
     * ******************************** */

    public function __construct() {
        $this->fabricas = new ArrayCollection();
        $this->imagenesproducto = new ArrayCollection();
        $this->stocks = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion() . ' - ' . $this->getCodigo();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Producto
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Producto
     */
    public function setCodigo($codigo) {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * Set procedencia
     *
     * @param string $procedencia
     * @return Producto
     */
    public function setProcedencia($procedencia) {
        $this->procedencia = $procedencia;

        return $this;
    }

    /**
     * Get procedencia
     *
     * @return string
     */
    public function getProcedencia() {
        return $this->procedencia;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return Producto
     */
    public function setPrecio($precio) {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio() {
        return $this->precio;
    }

    /**
     * Set coeficientePrecio
     *
     * @param float $coeficientePrecio
     * @return Producto
     */
    public function setCoeficientePrecio($coeficientePrecio) {
        $this->coeficientePrecio = $coeficientePrecio;

        return $this;
    }

    /**
     * Get coeficientePrecio
     *
     * @return float
     */
    public function getCoeficientePrecio() {
        return $this->coeficientePrecio;
    }

    /**
     * Set tipoiva
     *
     * @param float $tipoiva
     * @return Producto
     */
    public function setTipoiva($tipoiva) {
        $this->tipoiva = $tipoiva;

        return $this;
    }

    /**
     * Get tipoiva
     *
     * @return float
     */
    public function getTipoiva() {
        return $this->tipoiva;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Producto
     */
    public function setModelo($modelo) {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string
     */
    public function getModelo() {
        return $this->modelo;
    }

    /**
     * Set caracteristica
     *
     * @param string $caracteristica
     * @return Producto
     */
    public function setCaracteristica($caracteristica) {
        $this->caracteristica = $caracteristica;

        return $this;
    }

    /**
     * Get caracteristica
     *
     * @return string
     */
    public function getCaracteristica() {
        return $this->caracteristica;
    }

    /**
     * Set codigoOriginal
     *
     * @param string $codigoOriginal
     * @return Producto
     */
    public function setCodigoOriginal($codigoOriginal) {
        $this->codigoOriginal = $codigoOriginal;

        return $this;
    }

    /**
     * Get codigoOriginal
     *
     * @return string
     */
    public function getCodigoOriginal() {
        return $this->codigoOriginal;
    }

    /**
     * Set nroFabrica
     *
     * @param string $nroFabrica
     * @return Producto
     */
    public function setNroFabrica($nroFabrica) {
        $this->nroFabrica = $nroFabrica;

        return $this;
    }

    /**
     * Get nroFabrica
     *
     * @return string
     */
    public function getNroFabrica() {
        return $this->nroFabrica;
    }

    /**
     * Set nroFabricacion
     *
     * @param string $nroFabricacion
     * @return Producto
     */
    public function setNroFabricacion($nroFabricacion) {
        $this->nroFabricacion = $nroFabricacion;

        return $this;
    }

    /**
     * Get nroFabricacion
     *
     * @return string
     */
    public function getNroFabricacion() {
        return $this->nroFabricacion;
    }

    /**
     * Set anio
     *
     * @param integer $anio
     * @return Producto
     */
    public function setAnio($anio) {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return integer
     */
    public function getAnio() {
        return $this->anio;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Producto
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add imagenesproducto
     *
     * @param \JOYAS\JoyasBundle\Entity\ImagenProducto $imagenesproducto
     * @return Producto
     */
    public function addImagenesproducto(\JOYAS\JoyasBundle\Entity\ImagenProducto $imagenesproducto) {
        $this->imagenesproducto[] = $imagenesproducto;

        return $this;
    }

    /**
     * Remove imagenesproducto
     *
     * @param \JOYAS\JoyasBundle\Entity\ImagenProducto $imagenesproducto
     */
    public function removeImagenesproducto(\JOYAS\JoyasBundle\Entity\ImagenProducto $imagenesproducto) {
        $this->imagenesproducto->removeElement($imagenesproducto);
    }

    /**
     * Get imagenesproducto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagenesproducto() {
        return $this->imagenesproducto;
    }

    /**
     * Add stocks
     *
     * @param \JOYAS\JoyasBundle\Entity\Stock $stocks
     * @return Producto
     */
    public function addStock(\JOYAS\JoyasBundle\Entity\Stock $stocks) {
        $this->stocks[] = $stocks;

        return $this;
    }

    /**
     * Remove stocks
     *
     * @param \JOYAS\JoyasBundle\Entity\Stock $stocks
     */
    public function removeStock(\JOYAS\JoyasBundle\Entity\Stock $stocks) {
        $this->stocks->removeElement($stocks);
    }

    /**
     * Get stocks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStocks() {
        return $this->stocks;
    }

    public function getStock() {
        $stocktotal = 0;
        foreach ($this->stocks as $stock) {
            $stocktotal += $stock->getStock();
        }
        return $stocktotal;
    }

    /**
     * Set marca
     *
     * @param \JOYAS\JoyasBundle\Entity\Marca $marca
     * @return Producto
     */
    public function setMarca(\JOYAS\JoyasBundle\Entity\Marca $marca = null) {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \JOYAS\JoyasBundle\Entity\Marca
     */
    public function getMarca() {
        return $this->marca;
    }

    /**
     * Set rubro
     *
     * @param \JOYAS\JoyasBundle\Entity\Rubro $rubro
     * @return Producto
     */
    public function setRubro(\JOYAS\JoyasBundle\Entity\Rubro $rubro = null) {
        $this->rubro = $rubro;

        return $this;
    }

    /**
     * Get rubro
     *
     * @return \JOYAS\JoyasBundle\Entity\Rubro
     */
    public function getRubro() {
        return $this->rubro;
    }

    /**
     * Set origen
     *
     * @param \JOYAS\JoyasBundle\Entity\Origen $origen
     * @return Producto
     */
    public function setOrigen(\JOYAS\JoyasBundle\Entity\Origen $origen = null) {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen
     *
     * @return \JOYAS\JoyasBundle\Entity\Origen
     */
    public function getOrigen() {
        return $this->origen;
    }

    /**
     * Add fabricas
     *
     * @param \JOYAS\JoyasBundle\Entity\Fabrica $fabricas
     * @return Producto
     */
    public function addFabrica(\JOYAS\JoyasBundle\Entity\Fabrica $fabricas) {
        $this->fabricas[] = $fabricas;

        return $this;
    }

    /**
     * Remove fabricas
     *
     * @param \JOYAS\JoyasBundle\Entity\Fabrica $fabricas
     */
    public function removeFabrica(\JOYAS\JoyasBundle\Entity\Fabrica $fabricas) {
        $this->fabricas->removeElement($fabricas);
    }

    /**
     * Get fabricas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFabricas() {
        return $this->fabricas;
    }

    /**
     * Set usuario
     *
     * @param \JOYAS\JoyasBundle\Entity\Usuario $usuario
     * @return Producto
     */
    public function setUsuario(\JOYAS\JoyasBundle\Entity\Usuario $usuario = null) {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \JOYAS\JoyasBundle\Entity\Usuario
     */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * Set marcarepuesto
     *
     * @param string $marcarepuesto
     * @return Producto
     */
    public function setMarcarepuesto($marcarepuesto) {
        $this->marcarepuesto = $marcarepuesto;

        return $this;
    }

    /**
     * Get marcarepuesto
     *
     * @return string
     */
    public function getMarcarepuesto() {
        return $this->marcarepuesto;
    }

    /**
     * Set subcategoria
     *
     * @param \JOYAS\JoyasBundle\Entity\Subcategoria $subcategoria
     * @return Producto
     */
    public function setSubcategoria(\JOYAS\JoyasBundle\Entity\Subcategoria $subcategoria = null) {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    /**
     * Get subcategoria
     *
     * @return \JOYAS\JoyasBundle\Entity\Subcategoria 
     */
    public function getSubcategoria() {
        return $this->subcategoria;
    }

    /**
     * Get subcategoria
     *
     * @return \JOYAS\JoyasBundle\Entity\Subcategoria 
     */
    public function getCodigoDescripcion() {
         return $this->getCodigo() . ' - ' . $this->getDescripcion();
    }
}
