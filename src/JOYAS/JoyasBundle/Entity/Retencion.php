<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\RetencionRepository")
 * @ORM\Table(name="retencion")
 */
class Retencion{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	* @ORM\ManyToOne(targetEntity="TipoRetencion", inversedBy="rentenciones")
	* @ORM\JoinColumn(name="tiporetencion_id", referencedColumnName="id")
	*/
    protected $tiporetencion;

	/**
	* @ORM\ManyToOne(targetEntity="Factura", inversedBy="rentenciones")
	* @ORM\JoinColumn(name="factura_id", referencedColumnName="id")
	*/
    protected $factura;

	/**
	* @ORM\ManyToOne(targetEntity="Gasto", inversedBy="rentenciones")
	* @ORM\JoinColumn(name="gasto_id", referencedColumnName="id")
	*/
    protected $gasto;
	/**
	* @ORM\ManyToOne(targetEntity="Cobranza", inversedBy="rentenciones")
	* @ORM\JoinColumn(name="cobranza_id", referencedColumnName="id")
	*/
    protected $cobranza;
	/**
	* @ORM\ManyToOne(targetEntity="Pago", inversedBy="rentenciones")
	* @ORM\JoinColumn(name="pago_id", referencedColumnName="id")
	*/
    protected $pago;

	/**
	* @ORM\ManyToOne(targetEntity="FacturaImportacion", inversedBy="rentenciones")
	* @ORM\JoinColumn(name="facturaimportacion_id", referencedColumnName="id")
	*/
    protected $facturaImportacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}		

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Retencion
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    
        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Retencion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    
        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Retencion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tiporetencion
     *
     * @param \JOYAS\JoyasBundle\Entity\TipoRetencion $tiporetencion
     * @return Retencion
     */
    public function setTiporetencion(\JOYAS\JoyasBundle\Entity\TipoRetencion $tiporetencion = null)
    {
        $this->tiporetencion = $tiporetencion;
    
        return $this;
    }

    /**
     * Get tiporetencion
     *
     * @return \JOYAS\JoyasBundle\Entity\TipoRetencion 
     */
    public function getTiporetencion()
    {
        return $this->tiporetencion;
    }

    /**
     * Set factura
     *
     * @param \JOYAS\JoyasBundle\Entity\Factura $factura
     * @return Retencion
     */
    public function setFactura(\JOYAS\JoyasBundle\Entity\Factura $factura = null)
    {
        $this->factura = $factura;
    
        return $this;
    }

    /**
     * Get factura
     *
     * @return \JOYAS\JoyasBundle\Entity\Factura 
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * Set gasto
     *
     * @param \JOYAS\JoyasBundle\Entity\Gasto $gasto
     * @return Retencion
     */
    public function setGasto(\JOYAS\JoyasBundle\Entity\Gasto $gasto = null)
    {
        $this->gasto = $gasto;
    
        return $this;
    }

    /**
     * Get gasto
     *
     * @return \JOYAS\JoyasBundle\Entity\Gasto 
     */
    public function getGasto()
    {
        return $this->gasto;
    }

    /**
     * Set facturaImportacion
     *
     * @param \JOYAS\JoyasBundle\Entity\FacturaImportacion $facturaImportacion
     * @return Retencion
     */
    public function setFacturaImportacion(\JOYAS\JoyasBundle\Entity\FacturaImportacion $facturaImportacion = null)
    {
        $this->facturaImportacion = $facturaImportacion;
    
        return $this;
    }

    /**
     * Get facturaImportacion
     *
     * @return \JOYAS\JoyasBundle\Entity\FacturaImportacion 
     */
    public function getFacturaImportacion()
    {
        return $this->facturaImportacion;
    }

    /**
     * Set cobranza
     *
     * @param \JOYAS\JoyasBundle\Entity\Cobranza $cobranza
     * @return Retencion
     */
    public function setCobranza(\JOYAS\JoyasBundle\Entity\Cobranza $cobranza = null)
    {
        $this->cobranza = $cobranza;
    
        return $this;
    }

    /**
     * Get cobranza
     *
     * @return \JOYAS\JoyasBundle\Entity\Cobranza 
     */
    public function getCobranza()
    {
        return $this->cobranza;
    }

    /**
     * Set pago
     *
     * @param \JOYAS\JoyasBundle\Entity\Pago $pago
     * @return Retencion
     */
    public function setPago(\JOYAS\JoyasBundle\Entity\Pago $pago = null)
    {
        $this->pago = $pago;
    
        return $this;
    }

    /**
     * Get pago
     *
     * @return \JOYAS\JoyasBundle\Entity\Pago 
     */
    public function getPago()
    {
        return $this->pago;
    }
}