<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\PagoFacturaRepository")
 * @ORM\Table(name="pagofactura")
 */
class PagoFactura{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

	/**
	* @ORM\ManyToOne(targetEntity="Pago", inversedBy="pagosfactura")
	* @ORM\JoinColumn(name="pago_id", referencedColumnName="id", nullable=true)
	*/
    protected $pago;

	/**
	* @ORM\ManyToOne(targetEntity="Factura", inversedBy="cobranzasfactura")
	* @ORM\JoinColumn(name="factura_id", referencedColumnName="id", nullable=true)
	*/
    protected $factura;

	/**
	* @ORM\ManyToOne(targetEntity="NotaCreditoDebito", inversedBy="cobranzasfactura")
	* @ORM\JoinColumn(name="notacreditodebito_id", referencedColumnName="id", nullable=true)
	*/
    protected $notaCreditoDebito;
		
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		
	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return "$ ".$this->getImporte();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return CobranzaFactura
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    
        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return CobranzaFactura
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set factura
     *
     * @param \JOYAS\JoyasBundle\Entity\Factura $factura
     * @return CobranzaFactura
     */
    public function setFactura(\JOYAS\JoyasBundle\Entity\Factura $factura = null)
    {
        $this->factura = $factura;
    
        return $this;
    }

    /**
     * Get factura
     *
     * @return \JOYAS\JoyasBundle\Entity\Factura 
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * Set notaCreditoDebito
     *
     * @param \JOYAS\JoyasBundle\Entity\NotaCreditoDebito $notaCreditoDebito
     * @return CobranzaFactura
     */
    public function setNotaCreditoDebito(\JOYAS\JoyasBundle\Entity\NotaCreditoDebito $notaCreditoDebito = null)
    {
        $this->notaCreditoDebito = $notaCreditoDebito;
    
        return $this;
    }

    /**
     * Get notaCreditoDebito
     *
     * @return \JOYAS\JoyasBundle\Entity\NotaCreditoDebito 
     */
    public function getNotaCreditoDebito()
    {
        return $this->notaCreditoDebito;
    }

    /**
     * Set pago
     *
     * @param \JOYAS\JoyasBundle\Entity\Pago $pago
     * @return PagoFactura
     */
    public function setPago(\JOYAS\JoyasBundle\Entity\Pago $pago = null)
    {
        $this->pago = $pago;
    
        return $this;
    }

    /**
     * Get pago
     *
     * @return \JOYAS\JoyasBundle\Entity\Pago 
     */
    public function getPago()
    {
        return $this->pago;
    }
}