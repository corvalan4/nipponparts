<?php
namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\FacturaImportacionRepository")
 * @ORM\Table(name="facturaimportacion")
 */
class FacturaImportacion{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
		
	/**
	* @ORM\ManyToOne(targetEntity="Sucursal", inversedBy="facturas")
	* @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
	*/
    protected $sucursal; // Para F
		
	/**
	* @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="facturas")
	* @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
	*/
    protected $clienteProveedor; // Para FP
		
	/**
	* @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="facturas")
	* @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
	*/
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tipo = "F"; // Puede ser F por FACTURA o FP por FACTURAPROVEEDOR

    /**
	 * @ORM\Column(type="datetime")
     */
    protected $fecha;

	/**
	* @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="factura", cascade={"persist", "remove"} )
	*/
	protected $productosFactura;

	/**
	* @ORM\OneToMany(targetEntity="Iva", mappedBy="factura")
	*/
	protected $ivas;

	/**
	* @ORM\OneToMany(targetEntity="CobranzaFactura", mappedBy="factura")
	*/
	protected $cobranzasfactura;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tipofactura;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $cae;

    /**
	 * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechavtocae;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nroremito;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $nrofactura;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $ptovta;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/ 
	public function __construct()
	{
		$this->productosFactura = new ArrayCollection();
		$this->ivas = new ArrayCollection();
		$this->cobranzasfactura = new ArrayCollection();
	}
		

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return FacturaImportacion
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return FacturaImportacion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return FacturaImportacion
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    
        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set tipofactura
     *
     * @param string $tipofactura
     * @return FacturaImportacion
     */
    public function setTipofactura($tipofactura)
    {
        $this->tipofactura = $tipofactura;
    
        return $this;
    }

    /**
     * Get tipofactura
     *
     * @return string 
     */
    public function getTipofactura()
    {
        return $this->tipofactura;
    }

    /**
     * Set cae
     *
     * @param string $cae
     * @return FacturaImportacion
     */
    public function setCae($cae)
    {
        $this->cae = $cae;
    
        return $this;
    }

    /**
     * Get cae
     *
     * @return string 
     */
    public function getCae()
    {
        return $this->cae;
    }

    /**
     * Set fechavtocae
     *
     * @param \DateTime $fechavtocae
     * @return FacturaImportacion
     */
    public function setFechavtocae($fechavtocae)
    {
        $this->fechavtocae = $fechavtocae;
    
        return $this;
    }

    /**
     * Get fechavtocae
     *
     * @return \DateTime 
     */
    public function getFechavtocae()
    {
        return $this->fechavtocae;
    }

    /**
     * Set nroremito
     *
     * @param string $nroremito
     * @return FacturaImportacion
     */
    public function setNroremito($nroremito)
    {
        $this->nroremito = $nroremito;
    
        return $this;
    }

    /**
     * Get nroremito
     *
     * @return string 
     */
    public function getNroremito()
    {
        return $this->nroremito;
    }

    /**
     * Set nrofactura
     *
     * @param integer $nrofactura
     * @return FacturaImportacion
     */
    public function setNrofactura($nrofactura)
    {
        $this->nrofactura = $nrofactura;
    
        return $this;
    }

    /**
     * Get nrofactura
     *
     * @return integer 
     */
    public function getNrofactura()
    {
        return $this->nrofactura;
    }

    /**
     * Set ptovta
     *
     * @param integer $ptovta
     * @return FacturaImportacion
     */
    public function setPtovta($ptovta)
    {
        $this->ptovta = $ptovta;
    
        return $this;
    }

    /**
     * Get ptovta
     *
     * @return integer 
     */
    public function getPtovta()
    {
        return $this->ptovta;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return FacturaImportacion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    
        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return FacturaImportacion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set sucursal
     *
     * @param \JOYAS\JoyasBundle\Entity\Sucursal $sucursal
     * @return FacturaImportacion
     */
    public function setSucursal(\JOYAS\JoyasBundle\Entity\Sucursal $sucursal = null)
    {
        $this->sucursal = $sucursal;
    
        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \JOYAS\JoyasBundle\Entity\Sucursal 
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }

    /**
     * Set clienteProveedor
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor
     * @return FacturaImportacion
     */
    public function setClienteProveedor(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor = null)
    {
        $this->clienteProveedor = $clienteProveedor;
    
        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \JOYAS\JoyasBundle\Entity\ClienteProveedor 
     */
    public function getClienteProveedor()
    {
        return $this->clienteProveedor;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return FacturaImportacion
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null)
    {
        $this->unidadNegocio = $unidadNegocio;
    
        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio()
    {
        return $this->unidadNegocio;
    }

    /**
     * Add productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     * @return FacturaImportacion
     */
    public function addProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura)
    {
        $this->productosFactura[] = $productosFactura;
    
        return $this;
    }

    /**
     * Remove productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     */
    public function removeProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura)
    {
        $this->productosFactura->removeElement($productosFactura);
    }

    /**
     * Get productosFactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosFactura()
    {
        return $this->productosFactura;
    }

    /**
     * Add ivas
     *
     * @param \JOYAS\JoyasBundle\Entity\Iva $ivas
     * @return FacturaImportacion
     */
    public function addIva(\JOYAS\JoyasBundle\Entity\Iva $ivas)
    {
        $this->ivas[] = $ivas;
    
        return $this;
    }

    /**
     * Remove ivas
     *
     * @param \JOYAS\JoyasBundle\Entity\Iva $ivas
     */
    public function removeIva(\JOYAS\JoyasBundle\Entity\Iva $ivas)
    {
        $this->ivas->removeElement($ivas);
    }

    /**
     * Get ivas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIvas()
    {
        return $this->ivas;
    }

    /**
     * Add cobranzasfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura
     * @return FacturaImportacion
     */
    public function addCobranzasfactura(\JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura)
    {
        $this->cobranzasfactura[] = $cobranzasfactura;
    
        return $this;
    }

    /**
     * Remove cobranzasfactura
     *
     * @param \JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura
     */
    public function removeCobranzasfactura(\JOYAS\JoyasBundle\Entity\CobranzaFactura $cobranzasfactura)
    {
        $this->cobranzasfactura->removeElement($cobranzasfactura);
    }

    /**
     * Get cobranzasfactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCobranzasfactura()
    {
        return $this->cobranzasfactura;
    }
}